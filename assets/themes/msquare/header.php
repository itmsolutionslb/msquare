<?php
/**
 * Main header section
 */

error_reporting(0);
ini_set("display_errors", "off");
 
 
  $menu_image_data  = get_field("menu_image"); 
{
    $taxonomy     = 'product_cat';
    $orderby      = 'name';  
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no  
    $title        = '';  
    $empty        = 0;

    $args = array(
           'taxonomy'     => $taxonomy,
           'orderby'      => $orderby,
           'show_count'   => $show_count,
           'pad_counts'   => $pad_counts,
           'hierarchical' => $hierarchical,
           'title_li'     => $title,
           'hide_empty'   => $empty
    );
   $all_categories = get_categories( $args );
    
   global $current_user,$woocommerce;
   wp_get_current_user();
   $display_name = $current_user->display_name;
   $name_array = explode(" ", $display_name);
   $full_name = $name_array[0];
   
   // count of wish list 
    
   $count = $woocommerce->cart->cart_contents_count;
   $items = $woocommerce->cart->get_cart();
   $user_id       = get_current_user_id();
   $product_ids = get_user_meta($user_id , "wishlist_product_ids");
   $product_ids = $product_ids[0]; 
   $siteurl = get_site_url();
}


?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <!--<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>-->
    <script>
    theme_directory = "<?php echo get_template_directory_uri() ?>";
    home_url = "<?php echo home_url() ?>";
    </script>
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    </script>
    <meta property="og:url" content="" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php bloginfo('name'); ?> | <?php _e(''); ?>" />
    <meta property="og:description" content="<?php _e(''); ?>" />
    <meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" />
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="user-scalable=no,width=device-width,initial-scale=1.0,maximum-scale=1.0" />
    <title><?php bloginfo('name') . is_front_page() ? '' : _e(' | ') . wp_title(''); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo site_url(); ?>/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="manifest.json">
    <meta name="msapplication-TileColor" content="#2e2e46">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#2e2e46">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,600,700,800" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
    <link href="https://fonts.googleapis.com/css2?family=Tangerine:wght@400;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<?php 
 
?>
<body <?php body_class(); ?>>
    <header <?= !is_page_template( 'pages/page-homepage.php' ) ? 'class="otherHeader"' : '';?>>
        <div class="leftSection">
            <a href="<?= esc_url(home_url('/')); ?>">
                <img class="firstLogo" src="<?= get_template_directory_uri(); ?>/assets/images/logo.svg"
                    alt="Msquare Gallery logo" title="Msquare Gallery" />
            </a>
        </div>
        <div class="middleSection">
            <ul>
                <li>
                    <a href="<?= esc_url(home_url('/shop')); ?>">Shop</a>
                    <div class="fullSubMenu">
                        <div class="container">
                            <div class="row">
                                <div class="leftSection col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <ul>
                                        <li><a href="<?= esc_url(home_url('/shop')); ?>">All Items</a></li>
                                        <?php 
                                            foreach ($all_categories as $cat) { 
                                                $category_slug = $cat->slug;
                                                if( $category_slug == "uncategorized" || $category_slug == "cash-gift" )
                                                    continue;
                                                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                                                    $image = wp_get_attachment_url( $thumbnail_id );
                                                    $siteurl = get_option("siteurl");
                                                
                                            ?>
                                            <li><a href="<?= esc_url(home_url('/shop?cat_name=')) . $category_slug; ?>"><?php echo $cat->name; ?></a></li>
                                        <?php 
                                            }
                                        ?> 
                                    </ul>
                                </div>
                                <div class="rightSection col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <img src="<?= $menu_image_data["url"]; ?>"
                                        alt="Msquare Gallery" title="Msquare Gallery" />
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="<?= esc_url(home_url('/artists')); ?>">Artists</a></li>
                <li><a href="<?= esc_url(home_url('/our-story')); ?>">Our Story</a></li>
                <li><a href="<?= esc_url(home_url('/whats-on')); ?>">What's on</a></li>
                <li><a href="<?= esc_url(home_url('/contact-us')); ?>">Contact</a></li>
                <li><a href="<?= esc_url(home_url('/wedding-list')); ?>">Wedding List</a></li>
            </ul>
        </div>
        <div class="rightSection">
            <ul>
                <li class="hasSubMenu myAccountBtn" style="<?php echo (is_user_logged_in() ? "" : "display:none" ); ?>">
                    <a href="<?= esc_url(home_url('/my-orders')); ?>">My Account</a>
                    <div class="subMenu">
                        <img class="whiteArrow" src="<?= get_template_directory_uri(); ?>/assets/images/white-arrow.svg"
                            title="Arrow" alt="Arrow" />
                        <div class="subMenuHead">
                            <h4>Hi <?php echo $full_name; ?></h4>
                        </div>
                        <div class="subMenuBody">
                            <ul>
                                <li>
                                    <a href="<?= esc_url(home_url('/my-orders')); ?>">
                                        My Orders
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= esc_url(home_url('/addresses')); ?>">
                                        Addresses
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= esc_url(home_url('/account-details')); ?>">
                                        Account Details
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= esc_url(home_url('/wedding-list-info')); ?>">
                                        <i class="fal fa-gift"></i> Wedding List
                                    </a>
                                </li>
                                <li class="logoutBtn">
                                    <a href="<?= esc_url(home_url('/log-out')); ?>">
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li style="<?php echo (!is_user_logged_in() ? "" : "display:none" ); ?>"><a href="<?= esc_url(home_url('/login')); ?>">Login</a></li>
                
                <li class="wishlistBtn"><a href="<?= esc_url(home_url('/wishlist')); ?>"><span
                            class="counter"><?php echo (count($product_ids) > 0 ? count($product_ids) : "") ?></span><i class="fal fa-heart"></i></a></li>
                <li class="hasSubMenu cartBtn">
                    <a href="#"><i class="fal fa-shopping-bag"></i><span
                            class="cartcounter"><?= count($items) ?></span></a>
                    <div class="subMenu">
                        <img class="whiteArrow" src="<?= get_template_directory_uri(); ?>/assets/images/white-arrow.svg"
                            title="Arrow" alt="Arrow" />
                        <div class="subMenuHead">
                            <h4>MY CART (<?= count($items); ?>)</h4>
                            <a href="<?= esc_url(home_url('/my-bag')); ?>">VIEW ALL</a>
                        </div>
                        <div class="subMenuBody">
                            <ul class="BasketItems">
                                <?php 
                                foreach ($items as $key => $item_info) { 
                                    $_product =  wc_get_product( $item_info['data']->get_id());  
                                    $product_id = $item_info['data']->get_id();
                                    $thumb_image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'thumbnail' );
                                    $thumb_image_url = ( strlen($thumb_image[0]) > 0 ) ?  $thumb_image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";   
                                ?>
                                <li id="ITEM_<?= $product_id ?>">
                                    <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>">
                                        <div class="imageHolder"><img
                                                src="<?= $thumb_image_url; ?>"
                                                alt="Msquare Gallery" title="Msquare Gallery" /></div>
                                        <div class="descHolder" style="position: relative">
                                            <h3><?= $_product->get_title(); ?></h3>
                                            <h4><?= get_woocommerce_currency_symbol() ?> <?= $_product->get_price(); ?> </h4>
                                            <span class="ItemQty">Qty:<span class="counter"><?= $item_info['quantity'] ?></span></span>
                                        </div>
                                    </a>
                                </li> 
                                <?php } ?>
                               
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="hamburgerMenu"><a href="#"></a></li>
            </ul>
        </div>
    </header>
    <div class="fullMobileMenu">
        <ul class="menuList">
            <li><a href="<?= esc_url(home_url('/my-orders')); ?>">My Account</a></li>
            <li class="hasSubMenu">
                <a href="<?= esc_url(home_url('/shop')); ?>">Shop <i class="fal fa-angle-down"></i></a>
                <ul class="subMenu">
                    <li><a href="<?= esc_url(home_url('/shop')); ?>">All Items</a></li>
                    <?php 
                        foreach ($all_categories as $cat) {  
                            $category_slug = $cat->slug;
                              if( $category_slug == "uncategorized" || $category_slug == "cash-gift" )
                                 continue;
                    ?>
                    <li><a href="<?= esc_url(home_url('/shop?cat_name=')) . $category_slug; ?>"><?php echo $cat->name ?></a></li> 
                    <?php } ?>
                </ul>
            </li>
            <li><a href="<?= esc_url(home_url('/artists')); ?>">Artists</a></li>
            <li><a href="<?= esc_url(home_url('/our-story')); ?>">Our Story</a></li>
            <li><a href="<?= esc_url(home_url('/whats-on')); ?>">What's on</a></li>
            <li><a href="<?= esc_url(home_url('/contact-us')); ?>">Contact</a></li>
            <li><a href="<?= esc_url(home_url('/wedding-list')); ?>">Wedding List</a></li>
        </ul>
        <div class="socialMediaWrapper">
            <h3>FOLLOW US</h3>
            <ul>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
            </ul>
        </div>
    </div>