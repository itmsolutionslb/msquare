<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



{

    $user_id       = get_current_user_id();
    
    $product_ids = get_user_meta($user_id , "wishlist_product_ids");
    $product_ids = $product_ids[0];
    
    
    $siteurl = get_site_url() . "/shop"; 
} 
foreach ($lst_products->posts as $key => $post_info) {
    
    $product_id = $post_info->ID;
    $product = wc_get_product( $product_id );
    $regular_price  = $product->get_regular_price();
    $sales_price    = $product->get_sale_price();
    $product_price  = $product->get_price_html();
    $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids')); 
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_info->ID ), 'single-post-thumbnail' );
    $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
     
    $background_style = "";
    $icon_class = "fal fa-heart";
    if(in_array($product_id, $product_ids))
    { 
        $background_style  = "color:#FEC512";
        $icon_class = "fas fa-heart";
    } 
    ?>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 productWrap">
     <span data-product_id="<?= $product_id ?>"  class="addToWishlist"><i  style="<?= $background_style ?>" class="<?= $icon_class ?>"></i></span>
    <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>"
    class="<?= (strlen($sales_price) > 0 ) ? "sale" : "" ?>">
    <div class="imageHolder"> 
        <img src="<?= $product_image_url; ?>"
            alt="Msquare Gallery Product <?= $post_info->post_title ?>" title="Msquare Gallery Product <?= $post_info->post_title ?>" />
    </div>
    <div class="descHolder">
        <h3><?= ucfirst( strtolower($post_info->post_title)) ?></h3>
        <h4><?= $product_price ?></h4>
        <?php if(strlen($sales_price) > 0 ){ ?>
        <h4 class="sale-price"><?= $sales_price ?></h4>
        <?php } ?>
    </div>
</a>
</div>
    <?php 
}

?>