<?php
/**
 * Custom post types registration
 *
 * @package  TriTheme
 * @company  Triangle Mena <http://trianglemena.com>
 * @developer  Maroun Melhem <http://maroun.me>
 * @developer  _REPLACE_WITH_DEV_NAME_
 */
function create_custom_post_types()
{
    $postTypes = array(
        'Services categories' => 'Services category',
        'Products' => 'Product',
    );
    foreach ($postTypes as $postType => $post) {
        $postSlug = str_replace(' ', '-', strtolower($postType));
        $postName = str_replace(' ', '_', strtolower($postType));
        $labels = array(
            'name' => $postType,
            'singular_name' => $post,
            'add_new' => 'Add New',
            'add_new_item' => 'Add New ' . $post,
            'edit_item' => 'Edit ' . $post,
            'new_item' => 'New ' . $post,
            'all_items' => 'All ' . $postType,
            'view_item' => 'View ' . $post,
            'search_items' => 'Search ' . $postType,
            'not_found' => 'No ' . $postType . ' found',
            'not_found_in_trash' => 'No ' . $postType . ' found in Trash',
            'parent_item_colon' => '',
            'menu_name' => $postType,
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => $postSlug,
                'with_front' => true
            ),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 10,
            'supports' => array(
                'author',
                'comments',
                'editor',
                'excerpt',
                'revisions',
                'title',
                'editor',
                'thumbnail',
                'trackbacks',
                'post-formats'
            ),
        );
        if ($postType == "Services categories") {

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array(
                    'slug' => $postSlug,
                    'with_front' => true
                ),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => 10,
                'supports' => array(
                    'revisions',
                    'title',
                    'thumbnail',
                ),
                'menu_icon' => 'dashicons-editor-ul',
            );
        }
        if ($postType == "Products") {

            $args = array(
                'labels' => $labels,
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array(
                    'slug' => $postSlug,
                    'with_front' => true
                ),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => 10,
                'supports' => array(
                    'revisions',
                    'title',
                    'editor',
                    'thumbnail',
                ),
                'menu_icon' => 'dashicons-cart',
            );
        }
        register_post_type($postName, $args);
        flush_rewrite_rules(false);
    }
}
add_action('init', 'create_custom_post_types');