<?php
/**
 * Custom Taxonomies Registration
 *
 * @developer  Marc bou sleiman <http://marcbousleiman.com>
 */
function project_taxonomy_cars_family() {
	$labels = array(
		'name'                           => 'Cars family',
		'singular_name'                  => 'Car family',
		'search_items'                   => 'Search Cars family',
		'all_items'                      => 'All Cars family',
		'edit_item'                      => 'Edit Car family',
		'update_item'                    => 'Update Car family',
		'add_new_item'                   => 'Add New Car family',
		'new_item_name'                  => 'New Car family',
		'menu_name'                      => 'Cars family',
		'view_item'                      => 'View Car family',
		'popular_items'                  => 'Popular Car family',
		'separate_items_with_commas'     => 'Separate Cars family with commas',
		'add_or_remove_items'            => 'Add or remove Car family',
		'choose_from_most_used'          => 'Choose from the most used Cars family',
		'not_found'                      => 'No Cars family found'
	);

	register_taxonomy(
		'cars-family',
		'post',
		array(
			'label' => __( 'Cars family' ),
			'hierarchical' => true,
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => false,
			'show_tagcloud' => false,
			'show_admin_column' => true,
			'rewrite' => array(
				'slug' => 'cars-family'
			)
		)
	);
}
add_action( 'init', 'project_taxonomy_cars_family' );

function project_taxonomy_cars() {
	$labels = array(
		'name'                           => 'Cars',
		'singular_name'                  => 'Car',
		'search_items'                   => 'Search Cars',
		'all_items'                      => 'All Cars',
		'edit_item'                      => 'Edit Car',
		'update_item'                    => 'Update Car',
		'add_new_item'                   => 'Add New Car',
		'new_item_name'                  => 'New Car',
		'menu_name'                      => 'Cars',
		'view_item'                      => 'View Car',
		'popular_items'                  => 'Popular Car',
		'separate_items_with_commas'     => 'Separate Cars with commas',
		'add_or_remove_items'            => 'Add or remove Car',
		'choose_from_most_used'          => 'Choose from the most used Cars',
		'not_found'                      => 'No Cars found'
	);

	register_taxonomy(
		'cars',
		'post',
		array(
			'label' => __( 'Cars' ),
			'hierarchical' => true,
			'labels' => $labels,
			'public' => true,
			'show_in_nav_menus' => false,
			'show_tagcloud' => false,
			'show_admin_column' => true,
			'rewrite' => array(
				'slug' => 'cars'
			)
		)
	);
}
add_action( 'init', 'project_taxonomy_cars' );