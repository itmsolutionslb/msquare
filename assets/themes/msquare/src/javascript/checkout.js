/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function CalculateShipmentRate()
{
    var user_address = jQuery("input[name=user_address]").val();
    var siteurl = jQuery("input[name=siteurl]").val();
    var str_params = { action : "shipment_rate" , user_address : user_address };
     jQuery.ajax
    ({
        url: siteurl + "/ajax-requests",
        data: str_params,
        method: 'post',
        dataType: "json",
        success: function (response) {
            
        }
    });
    
}

jQuery(function(){
    jQuery(".shippingMethod .addressInfo .circle").on("click",function(){
       jQuery(".shippingMethod .addressInfo .circle").each(function(){
            jQuery(this).removeClass('active');
            jQuery(this).find('input').removeAttr('checked');
        });
        
        jQuery(this).addClass('active');
        jQuery(this).find('input').attr('checked',true);
        let address_val = jQuery(this).find('input').val();
        jQuery("input[name=user_address_value]").val(address_val);
    });
    jQuery(".ListAddresses").on("click",".circle",function(){
         jQuery(".shippingAddress .addressInfo .circle").each(function(){
            jQuery(this).removeClass('active');
            jQuery(this).find('input').removeAttr('checked');
        });
        
        jQuery(this).addClass('active');
        jQuery(this).find('input').attr('checked',true);
        CalculateShipmentRate();
    });
    jQuery(".shippingAddress .addressInfo .circle").on("click",function(){
       jQuery(".shippingAddress .addressInfo .circle").each(function(){
            jQuery(this).removeClass('active');
            jQuery(this).find('input').removeAttr('checked');
        });
        
        jQuery(this).addClass('active');
        jQuery(this).find('input').attr('checked',true);
        CalculateShipmentRate();
    });
    jQuery(".MsgType").on("click",function(){
        jQuery(".RdMsgType").each(function(){
            jQuery(this).removeAttr('checked'); 
        });
        jQuery(this).find(".circle").find(".RdMsgType").attr("checked","checked");
        let msg_type =  jQuery(this).find(".RdMsgType").val();
        
        if(msg_type == '1')
        {
            jQuery(".CoupleName").css("display","none");
            jQuery(".CoupleNotes").css("display","none");
            jQuery(".WeedingNote").css("display","");
        }
        else
        {
            jQuery(".CoupleName").css("display","");
            jQuery(".CoupleNotes").css("display","");
            jQuery(".WeedingNote").css("display","none");
        }
    });
    jQuery(".sendAsGiftHolder .square").on("click",function(e){ 
      if( jQuery(this).hasClass("checked") )
      {
          jQuery(this).removeClass('checked');
          jQuery(this).addClass('unchecked');
          jQuery("input[name=gift_notes]").val("");
          jQuery("input[name=couple_name]").val("");
          jQuery("input[name=couple_notes]").val("");
          
          jQuery("input[name=txtCoupleName]").val("");
          jQuery("textarea[name=txtCoupleNote]").val("");
          jQuery("textarea[name=txtGiftNote]").val("");
          return false;
      }
    });
    
    
    jQuery("#BTN_SAVE_ADDRESS").on("click",function(e){ 
        e.preventDefault();
        var siteurl = jQuery("input[name=siteurl]").val();
        var str_params = jQuery("#FRM_ADDRESS_INFO").serialize(); 
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: str_params,
            method: 'post',
            dataType: "json",
            success: function (response) { 
                if(response.is_error == 0)
                {
                    jQuery("input[name=user_address]").each(function(n){
                        jQuery(this).removeAttr("checked");
                    });
                    jQuery(".addressInfo .circle").each(function(n){
                        jQuery(this).removeClass("active");
                    });
                    let addresses = jQuery(".ListAddresses").html();
                    jQuery(".ListAddresses").html(addresses + response.address_item);
                    jQuery("#FRM_ADDRESS_INFO input[type=name]").each(function(){
                        jQuery(this).val("");
                    }); 
                    CalculateShipmentRate();
                }
            }
        });
    });
    jQuery("input[name=btn_send_gift]").on("click",function(e){
        e.preventDefault();
        let gift_notes = jQuery("textarea[name=txtGiftNote]").val();
        let couple_name = jQuery("input[name=txtCoupleName]").val();
        let couple_notes = jQuery("textarea[name=txtCoupleNote]").val();
        
        jQuery("input[name=gift_notes]").val(gift_notes);
        jQuery("input[name=couple_name]").val(couple_name);
        jQuery("input[name=couple_notes]").val(couple_notes);
        jQuery(".closeBtn").trigger("click");
        
    });
    jQuery(".blockTitle").on("click",function(){
       //innerBlock 
       jQuery(".blockBody").each(function(){
           jQuery(this).parents(".innerBlock").removeClass("active");
           jQuery(this).slideUp();
       });
       
       jQuery(this).parents(".innerBlock").addClass('active');
       jQuery(this).next().slideDown();
    });
    jQuery(".addressInfo .circle").on("click",function(){
    });
    /**<?= $siteurl ?>/check-out-process */
    jQuery(".BtnPlaceOrder").on("click",function(){
        var siteurl = jQuery("input[name=siteurl]").val(); 
        var usr_address = jQuery('input[name=user_address][checked=checked]').val();
        var has_cash = jQuery('input[name=has_cash]').val();
        
        
        if(has_cash == 0)
        {
            if(usr_address == "-1")
            {
                alert('Please Select Address For Shipment');
                return false;
            }
        }
        jQuery("form[name=frm_place_order]").submit();
    }); 
    
    jQuery('.infoMark').on("mouseover",function(){
        jQuery('.insuranceInfoToolTip').fadeIn('slow');
    });
    jQuery('.infoMark').on("mouseout",function(){
        jQuery('.insuranceInfoToolTip').fadeOut('slow');
    });
    jQuery('.shippingInfoToolTip ul li').on("click",function(){
        //jQuery('.insuranceInfoToolTip').fadeOut('slow');
        jQuery('.shippingInfoToolTip ul li').each(function(){
            jQuery(this).removeClass('active');
        });
        jQuery(this).addClass('active');
        jQuery('.ShippingTitle').html(  jQuery(this).find('a').text() );
    });
   jQuery('body:not(.shippingInfo)').on("click",function(){
        jQuery('.shippingInfoToolTip').fadeOut('slow');
    });
})