var shop_module = {
    DisplayListShop : function(){
         var siteurl = jQuery("input[name=siteurl]").val();
         var page_number = jQuery("input[name=page_number]").val();
         if(page_number == 1)
         {
              jQuery(".LstProducts").html("<div class='row' style='width:100%'><div class='col-md-3'></div><div class='col-md-3' align='center'><img style='width:120px' src='" + siteurl + "/assets/themes/msquare/assets/images/msquare-loader.gif' /></div><div class='col-md-3'></div></div>");
         } 
         // get list categories
         var category_ids = [];
         var style_ids = [];
         var artists_ids = [];
         jQuery(".ckCategories:checked").each(function(){
             category_ids.push(jQuery(this).val());
         });
         jQuery(".ckStyle:checked").each(function(){
             style_ids.push(jQuery(this).val());
         });
          jQuery(".ckArtist:checked").each(function(){
             artists_ids.push(jQuery(this).val());
         });
         
         let width_from = jQuery("input[name=width_from]").val();
         let width_to = jQuery("input[name=width_to]").val();
         let height_from = jQuery("input[name=height_from]").val();
         let height_to = jQuery("input[name=height_to]").val();
         let order_by = jQuery("select[name=order_by").val();
         let cat_name = jQuery("input[name=cat_name").val();
         
          jQuery.ajax
            ({
                url: siteurl + "/ajax-requests",
                data: { action : "listshop" , cat_name : cat_name  , order_by : order_by , page_number : page_number , category_ids : category_ids , style_ids : style_ids , artists_ids : artists_ids , width_from : width_from , width_to : width_to , height_from : height_from , height_to : height_to },
                method: 'post',
                dataType: "json",
                success: function (response) { 
                    //var current_content = jQuery(".LstProducts").html();
                    /**if( current_content.length > 0 )
                    { 
                       if(page_number  > 1)
                       {
                           jQuery(".LstProducts").html(current_content + response.display);
                       }
                       else
                       {
                           jQuery(".LstProducts").html(response.display);
                       }
                       
                    }
                    else
                    {
                        jQuery(".LstProducts").html(response.display);
                    }*/
                     jQuery(".LstProducts").html(response.display);
                    jQuery('#ProductsPagination').twbsPagination({
                        totalPages: response.total_pages,
                        visiblePages: 2,
                        onPageClick: function (event, page) {
                             jQuery('input[name=page_number]').val(page);
                             shop_module.DisplayListShop();
                        }
                    });
                    
                }
            });
    }
};
jQuery(function(){
    shop_module.DisplayListShop(); 
    jQuery("select[name=order_by").on("change",function(){
        shop_module.DisplayListShop();
    });
    jQuery(".ProductVariance").on("click",function(){
        jQuery(".ProductVariance").each(function(){
            jQuery(this).removeClass("active");
        });
         var siteurl = jQuery("input[name=siteurl]").val();
        jQuery(this).addClass("active");
        var term_id     = jQuery(this).data('term_id');
        var product_id  = jQuery("input[name=product_id]").val();
        var params      = { action : "get_product_variance"  , term_id : term_id , product_id : product_id };
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: params,
            method: 'post',
            dataType: "json",
            success: function (response) {

            }
        });
    });
    jQuery('.LnkStyleItem,.LnkCategoryItem,.LnkArtistName').on("click",function(){ 
        if( jQuery(this).find('input[type=checkbox]').attr("checked") == undefined )
        {
            jQuery(this).find('input[type=checkbox]').attr("checked","checked")
            jQuery(this).find('input[type=checkbox]').prop("checked", true);
        }
        else
        { 
            jQuery(this).find('input[type=checkbox]').removeAttr("checked");
        }
        jQuery("input[name=page_number]").val(1);
        shop_module.DisplayListShop();
    });
    jQuery("#BTN_SEND_REQUEST").on("click",function(e){
        e.preventDefault();
        jQuery('.msg_result').html("");
         var siteurl            = jQuery("input[name=siteurl]").val();  
         var product_id         = jQuery("input[name=product_id]").val();  
         var rp_client_name     = jQuery("input[name=rp_client_name]").val();  
         var rp_client_email    = jQuery("input[name=rp_client_email]").val();  
         var rp_client_message  = jQuery("input[name=rp_client_message]").val();
         
        var params      = { action : "send_request_product"  , rp_client_message : rp_client_message , rp_client_email : rp_client_email  , rp_client_name : rp_client_name , product_id : product_id };
     
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: params,
            method: 'post',
            dataType: "json",
            success: function (response) {
                if(response.is_error == 0)
                {
                    //jQuery('.msg_result').html(response.error_msg);
                    
                    jQuery('.RequestPriceBtn').each(function(){
                        jQuery(this).fadeOut('fast')
                    });
                    jQuery('.RequestedtBtn').each(function(){
                        jQuery(this).fadeIn('slow')
                    });
                    jQuery("div.SendMessageForm").slideUp('slow');
                    jQuery("div.SuccessSendMsg").slideUp('fast');
                }
                
                 jQuery("input[type=text]").val("");
                jQuery("input[type=email]").val("");
                jQuery("textarea").val(""); 
                setTimeout(function(){
                    jQuery(".closeBtn").trigger("click");
                },3000);
            }
        });
    });
  
    jQuery("#BTN_ADD_TO_CARD").on("click",function(){
        var product_id = jQuery("input[name=product_id]").val();
        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: '',
            quantity: 1
        };
           jQuery.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
             dataType: "json",
            beforeSend: function (response) { 
            },
            complete: function (response) { 
            },
            success: function (response) {

                if (response.error & response.product_url) {
                    window.location = response.product_url;
                    return;
                } else {
                  //  $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
                  
                   
                   
                }
                let basketcontent =response.basket_content;
                if(basketcontent.length > 0)
                {
                     jQuery(".BasketItems").html(response.basket_content);
                    jQuery(".cartBtn .subMenu").css({"visibility" : "visible","opacity" : "1","display" : "block"});
                    setTimeout(function(){
                        jQuery(".cartBtn .subMenu").fadeOut('slow');
                    },3000)
                }
            },
        });
        
        
    });
})