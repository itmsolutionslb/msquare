/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(function(){
    jQuery("#BTN_SAVE_ADDRESS").on("click",function(e){
        e.preventDefault();
        var siteurl = jQuery("input[name=siteurl]").val();
        var str_params = jQuery("#FRM_ADDRESS_INFO").serialize();
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: str_params,
            method: 'post',
            dataType: "json",
            success: function (response) {
                if(response.is_error == 0)
                {
                    window.location.href = siteurl + "/address"
                }
            }
        });
    });
   
});
