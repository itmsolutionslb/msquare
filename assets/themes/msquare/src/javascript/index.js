import 'simplebar'; // or "import SimpleBar from 'simplebar';" if you want to use it manually.
import 'simplebar/dist/simplebar.css';
import Swiper from 'swiper';
import '../libraries/swiper/swiper.min.css';
import AOS from 'aos';
import 'aos/dist/aos.css';
import '../libraries/bootstrap.min.css';
import '../sass/style.scss';

jQuery(window).load(function () {
    jQuery('.mbsAnimate').each(function () {
        var delayAttr = jQuery(this).attr('data-delay');
        var top_of_element = jQuery(this).offset().top + 80;
        var bottom_of_element = jQuery(this).offset().top + jQuery(this).outerHeight();
        var bottom_of_screen = jQuery(window).innerHeight();
        var top_of_screen = 0;

        if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
            jQuery(this).addClass('mbsAnimated');
            if (delayAttr > 0) {
                jQuery(this).css('transition-delay', delayAttr + 'ms');
            }
        }
    });

    if (jQuery('.withPageIdentifier').hasClass('artistsPage') && jQuery(window).width() < 576) {
        // after page load, get the sum of sub elements in scrollable element
        if (jQuery('.artistsPage .artistsSection .categoriesWrapper ul').length > 0) {
            let maxWidth = 0;
            jQuery('.artistsPage .artistsSection .categoriesWrapper ul li').each(function () {
                let liWidth = jQuery(this).outerWidth() + 30;
                if (liWidth > 0) {
                    maxWidth = maxWidth + liWidth;
                }
            });
            jQuery('.artistsPage .artistsSection .categoriesWrapper ul').css('width', maxWidth + 'px');
        }
        jQuery('.artistsPage .artistsSection .categoriesWrapper').animate({scrollLeft: jQuery('.artistsPage .artistsSection .categoriesWrapper ul li a.active').position().left}, 500);
    }

    if (jQuery('.myAccountPages .contentSection .leftSection .pageName').length > 0) {
        jQuery(document).on('click', '.myAccountPages .contentSection .leftSection .pageName', function (e) {
            e.preventDefault();
            if (jQuery(this).closest('.innerLeftSection').hasClass('active')) {
                jQuery(this).closest('.innerLeftSection').removeClass('active');
                jQuery(this).closest('.innerLeftSection').find('ul').slideUp('slow');
            } else {
                jQuery(this).closest('.innerLeftSection').addClass('active');
                jQuery(this).closest('.innerLeftSection').find('ul').slideDown('slow');
            }
        });
    }
});
jQuery(document).ready(function ($) {
    // AOS.init();

    if (jQuery('body').hasClass('home')) {
        new Swiper('.swiper-container.topSwiper', {
            slidesPerView: 1,
            grabCursor: true,
            speed: 700,
            autoplay: {
                delay: 4000,
            },
            pagination: {
                el: '.topSwiperPag',
                type: 'bullets',
                clickable: true
            }
        });

        new Swiper('.swiper-container.curatedWeeklySlider', {
            slidesPerView: 1,
            autoplay: {
                delay: 5000,
            },
            effect: 'fade',
            pagination: {
                el: '.curatedWeeklySliderPag',
                type: 'bullets',
                clickable: true
            }
        });

        new Swiper('.swiper-container.categoriesSwiper', {
            slidesPerView: 2,
            navigation: {
                nextEl: '.btnNext.categoriesSwiperNav',
                prevEl: '.btnPrev.categoriesSwiperNav'
            },
            breakpoints: {
                1100: {
                    slidesPerView: 5
                },
                600: {
                    slidesPerView: 3
                }
            }
        });
    }

    // When user clicks to open Send message popup
    jQuery(document).on('click', '.sendMessageTrigger', function (e) {
        e.preventDefault();
        jQuery('body').css('overflow-y', 'hidden');
        jQuery('.generalPopup').fadeIn('slow').css('display', 'flex').addClass('showMe');
    });

    // When user clicks to open wedding list popup
    jQuery(document).on('click', '.openGeneralPopup', function (e) {
        e.preventDefault();
        let usage = jQuery(this).attr('data-usage');
        jQuery('body').css('overflow-y', 'hidden');
        jQuery('.generalPopup[data-usage="' + usage + '"]').fadeIn('slow').css('display', 'flex').addClass('showMe');
    });

    // When user clicks the X button to close the general popup
    jQuery(document).on('click', '.generalPopup .innerGeneralPopup .closeBtn', function (e) {
        e.preventDefault();
        jQuery('body').css('overflow-y', 'auto');
        jQuery('.generalPopup').fadeOut('slow').removeClass('showMe');
    });

    // When mobile user clicks the shop btn to open submenu
    jQuery(document).on('click', '.fullMobileMenu .menuList li.hasSubMenu > a', function (e) {
        e.preventDefault();
        if (jQuery(this).parent().hasClass('opened')) {
            jQuery(this).parent().removeClass('opened');
            jQuery(this).parent().find('.subMenu').slideToggle('slow');
        } else {
            jQuery(this).parent().addClass('opened');
            jQuery(this).parent().find('.subMenu').slideToggle('slow');
        }
    });

    // When mobile user clicks on the burger menu
    jQuery(document).on('click', 'header .rightSection ul li.hamburgerMenu', function (e) {
        e.preventDefault();
        if (jQuery('body').hasClass('mobileMenuOpened')) {
            jQuery('body').css('overflow-y', 'auto').removeClass('mobileMenuOpened');
            jQuery('.fullMobileMenu').fadeOut('slow');
        } else {
            jQuery('body').css('overflow-y', 'hidden').addClass('mobileMenuOpened');
            jQuery(this).find('.subMenu').slideToggle('slow');
            jQuery('.fullMobileMenu').fadeIn('slow');
        }
    });

    // When user clicks on any filter head
    jQuery(document).on('click', '.filterHolderHead', function (e) {
        let element = jQuery(this);
        if (jQuery(this).closest('.eachFilterHolder').hasClass('opened')) {
            jQuery(this).closest('.eachFilterHolder').removeClass('opened');
            jQuery(this).closest('.eachFilterHolder').find('.filterHolderHead span i').removeClass('fa-minus').addClass('fa-plus');
            element.closest('.eachFilterHolder').removeClass('doneOpening');
        } else {
            jQuery(this).closest('.eachFilterHolder').addClass('opened');
            jQuery(this).closest('.eachFilterHolder').find('.filterHolderHead span i').removeClass('fa-plus').addClass('fa-minus');
            setTimeout(() => {
                element.closest('.eachFilterHolder').addClass('doneOpening');
            }, 1000);
        }
    });

    // when user clicks on any filter
    jQuery(document).on('click', '.shopPage .contentSection .leftSection .eachFilterHolder .filterHolderBody ul li a', function (e) {
        e.preventDefault();
        if (jQuery(this).hasClass('selected')) {
            jQuery(this).removeClass('selected');
        } else {
            jQuery(this).addClass('selected');
        }
    });

    // when user clicks to filter button to open popup
    jQuery(document).on('click', '.shopPage .contentSection .leftSection .closeFilterPopup', function (e) {
        e.preventDefault();
        jQuery('body').css('overflow-y', 'auto');
        jQuery('.shopPage .contentSection .leftSection').fadeOut('slow');
    });

    // when user clicks to x button to close popup
    jQuery(document).on('click', '.openFilterPopup', function (e) {
        e.preventDefault();
        jQuery('body').css('overflow-y', 'hidden')
        jQuery('.shopPage .contentSection .leftSection').fadeIn('slow');
    });

    // Init both swipers in single products
    var galleryThumbs = new Swiper('.singleGalleryThumbs', {
        spaceBetween: 10,
        slidesPerView: 6.5,
        direction: jQuery(window).width() > 992 ? 'vertical' : 'horizontal',
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
    });
    new Swiper('.singleGallery', {
        spaceBetween: 10,
        grabCursor: true,
        thumbs: {
            swiper: galleryThumbs
        }
    });

    if (jQuery('body').hasClass('page-template-page-aboutus')) {
        new Swiper('.swiper-container.aboutUsSlider', {
            slidesPerView: 1,
            grabCursor: true,
            autoplay: {
                delay: 4000,
            },
            pagination: {
                el: '.aboutUsSliderPag',
                type: 'bullets',
                clickable: true
            }
        });
    }

    // When user clicks on the request button
    jQuery(document).on('click', '.openGeneralPopup', function (e) {
        e.preventDefault();
        let usage = jQuery(this).attr('data-usage');
        jQuery('body').css('overflow-y', 'hidden');
        jQuery('.generalPopup[data-usage="' + usage + '"]').fadeIn('slow').css('display', 'flex').addClass('showMe');
    });

    jQuery(document).on('click', '.myOrdersPage .contentSection .rightSection .innerRightSection .eachOrderWrap .topOrderWrap .viewOrderBtn', function (e) {
        e.preventDefault();
        if (jQuery(this).closest('.eachOrderWrap').hasClass('opened')) {
            jQuery(this).closest('.eachOrderWrap').removeClass('opened');
            jQuery(this).closest('.eachOrderWrap').find('.horizontalPoductWrap').slideUp('slow');
        } else {
            jQuery(this).closest('.eachOrderWrap').addClass('opened');
            jQuery(this).closest('.eachOrderWrap').find('.horizontalPoductWrap').slideDown('slow').css('display', 'flex');
        }
    });

    // JS for checkout page start
    // when user clicks to send as a gift
    jQuery(document).on('click', '.checkoutPage .sendAsGiftHolder', function (e) {
        e.preventDefault();
        jQuery('body').css('overflow-y', 'auto');
        jQuery('.checkoutPage .generalPopup').fadeIn('slow').css('display', 'flex').addClass('showMe');
        jQuery(this).find('.square').removeClass('unchecked').addClass('checked');
    });

    // when user clicks to x button to close popup
    jQuery(document).on('click', '.checkoutPage .generalPopup .closeBtn', function (e) {
        e.preventDefault();
        jQuery('body').css('overflow-y', 'hidden');
        jQuery('.generalPopup').fadeOut('slow').removeClass('showMe');
    });

    jQuery(document).on('click', '.checkoutPage .generalPopup .innerGeneralPopup form .fieldWrapper.radioBtnsHolder .radioBtns', function (e) {
        e.preventDefault();
        if (!jQuery(this).find('.circle').hasClass('checked')) {
            jQuery(this).closest('.radioBtnsHolder').find('.radioBtns .circle').removeClass('checked').addClass('unchecked');
            jQuery(this).find('.circle').removeClass('unchecked').addClass('checked');
        }
    });

//    When user clicks on the arrow to check shipping info
    jQuery(document).on('click', '.checkoutPage.myBagPage .contentSection .rightSection .innerRightSection .separatedInnerContent.shippingInfo', function (e) {
        e.preventDefault();
        if(jQuery(this).hasClass('tooltipOpened')){
            jQuery(this).removeClass('tooltipOpened');
            jQuery(this).find('.shippingInfoToolTip').fadeOut('slow');
        }else{
            jQuery(this).addClass('tooltipOpened');
            jQuery(this).find('.shippingInfoToolTip').fadeIn('slow');
        }
    });
    // JS for checkout page end
});

jQuery(window).scroll(function () {
    let scrollTop = jQuery(this).scrollTop();

    if (scrollTop > 50) {
        jQuery("header").addClass('scrolling');
    } else {
        jQuery("header").removeClass('scrolling');
    }

    if ((jQuery(window).width() > 992) && jQuery('.withPageIdentifier').hasClass('shopPage')) {
        let middleColumnHeight = jQuery('.shopPage .rightSection').outerHeight();
        let leftColumnHeight = (jQuery('.shopPage .rightSection').outerHeight() + jQuery('.shopPage .leftSection').offset().top) - 30;
        let rightColumnHeight = jQuery('.shopPage .leftSection .innerLeftSection').outerHeight();

        if (middleColumnHeight > rightColumnHeight) {
            // right and left section fix function
            if (jQuery('.shopPage .leftSection .innerLeftSection').length > 0) {
                let offsetTop = jQuery('.shopPage .leftSection').offset().top;
                let fixingPoint = (offsetTop - 75 /* 75 Header height */);
                let innerRightColumnWidth = jQuery('.shopPage .leftSection .innerLeftSection').outerWidth();
                let innerRightColumnLeft = jQuery('.shopPage .leftSection .innerLeftSection').offset().left;
                if (scrollTop > fixingPoint) {
                    jQuery('.shopPage .leftSection .innerLeftSection').addClass('fixMe').css({left: innerRightColumnLeft, maxWidth: innerRightColumnWidth});
                    if (scrollTop > ((leftColumnHeight - rightColumnHeight) - 75)) {
                        jQuery('.shopPage .leftSection .innerLeftSection').css({'position': 'absolute', 'right': '30px', 'left': 'auto', bottom: 'auto', 'top': (leftColumnHeight - (rightColumnHeight + offsetTop))});
                    } else {
                        jQuery('.shopPage .leftSection .innerLeftSection').css({'position': 'fixed', bottom: 'auto', 'top': '75px'});
                    }
                } else {
                    jQuery('.shopPage .leftSection .innerLeftSection').removeClass('fixMe');
                    jQuery('.shopPage .leftSection .innerLeftSection').css({'position': 'relative', 'top': '0', left: '0', maxWidth: '100%'});
                }
            }
        }
    }

    jQuery('.mbsAnimate').each(function () {
        var delayAttr = jQuery(this).attr('data-delay');
        var top_of_element = jQuery(this).offset().top + 80;
        var bottom_of_element = jQuery(this).offset().top + jQuery(this).outerHeight();
        var bottom_of_screen = scrollTop + jQuery(window).innerHeight();
        var top_of_screen = scrollTop;

        if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
            jQuery(this).addClass('mbsAnimated');
            if (delayAttr > 0) {
                jQuery(this).css('transition-delay', delayAttr + 'ms');
            }
        }
    });
});