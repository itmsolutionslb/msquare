/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function SignupUsers() { 
        var siteurl = jQuery("input[name=siteurl]").val();
         var str_params = jQuery("#FORM_REGISTER").serialize();
          jQuery(".ImgLoader").css('visibility','visible');
          jQuery("#BTN_REGISTER").prop('disabled', true);
        jQuery.ajax
            ({
                url: siteurl + "/ajax-requests",
                data: str_params,
                method: 'post',
                dataType: "json",
                success: function (response) {
                    jQuery(".ImgLoader").css('visibility','hidden');
                     jQuery("#BTN_REGISTER").prop('disabled', false);
                    if(response.is_error == 0)
                    { 
                        jQuery(".FrmRegistration").slideToggle();
                        jQuery(".MsgRegistration").slideToggle().delay(600);
                        $("input[type=text]").each(function(){
                            $(this).val("");
                        });
                        $("input[type=password]").each(function(){
                            $(this).val("");
                        });
                        $("input[type=email]").each(function(){
                            $(this).val("");
                        });
                        
                       window.location.href = siteurl + "/shop";
                    }
                    else
                    {
                        jQuery(".ErrorMsg").html(response.error_msg);
                    }
                }
            });
}

function removeProductFromBag()
{
    var siteurl = jQuery("input[name=siteurl]").val();
    let product_id = jQuery(this).data("product_id"); 
    var str_params = { product_id : product_id , action : "remove_product_bag" };
    var $this = jQuery(this);
    jQuery.ajax
    ({
        url: siteurl + "/ajax-requests",
        data: str_params,
        method: 'post',
        dataType: "json",
        success: function (response) {
            if(response.is_error == '0')
            {
                $this.parents(".horizontalPoductWrap").fadeOut("fast",function(){
                    jQuery(this).remove();
                });
                jQuery("li#ITEM_" + response.product_id).remove();
            
                if(response.count == '0'){
                    jQuery(".MyBagPage").slideToggle();
                    jQuery(".EmptyBagPage").slideToggle();
                }
                
            }
        }
    }); 
    
}

function ForgotPassword() { 
        var siteurl = jQuery("input[name=siteurl]").val();
         var str_params = jQuery("#FRM_FORGOT_PASSWORD").serialize();
          jQuery(".ImgLoader").css('visibility','visible');
          jQuery("#BTN_FORGOT_PASSWORD").prop('disabled', true);
        jQuery.ajax
            ({
                url: siteurl + "/ajax-requests",
                data: str_params,
                method: 'post',
                dataType: "json",
                success: function (response) {
                    jQuery(".ImgLoader").css('visibility','hidden');
                     jQuery("#BTN_FORGOT_PASSWORD").prop('disabled', false);
                    if(response.is_error == 0)
                    { 
                        jQuery(".ForgotForm").hide();
                        jQuery(".ForgotMessage").show();
 
                    }
                    else
                    {
                        jQuery(".ErrorMsg").html(response.error_msg);
                    }
                }
            });
}


jQuery(function(){
    jQuery("#BTN_ADD_CASH_GIFT").on("click",function(e){
        e.preventDefault();
        var siteurl = jQuery("input[name=siteurl]").val();
        var str_params = jQuery("#FRM_CASH").serialize();
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: str_params,
            method: 'post',
            dataType: "json",
            success: function (response) {
                if(response.is_error == 0)
                {
                    window.location.href = siteurl + "/my-bag/";
                    jQuery(".CashPopup .closeBtn").trigger("click");
                }
            }
        });
    });
    jQuery("#BTN_FORGOT_PASSWORD").on("click",function(e){
        e.preventDefault();
        ForgotPassword();
    });
    jQuery(".removeProduct").on("click",removeProductFromBag);
    jQuery("#BTN_CHANGE_PASSWORD").on("click",function(e){
        e.preventDefault();
        jQuery(".ErrorMsg").html("");
        var siteurl = jQuery("input[name=siteurl]").val();
        var user_password           = jQuery("input[name=user_password]").val();
        var user_retype_password    = jQuery("input[name=user_retype_password]").val();
        if(user_password.length == 0 || user_retype_password.length == 0)
        {
            jQuery(".ErrorMsg").html("Password is required");
            return false;
        }
        
         var str_params = jQuery("#FRM_CHANGE_PASSWORD").serialize();
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: str_params,
            method: 'post',
            dataType: "json",
            success: function (response) {
                if(response.is_error == 0)
                {
                    window.location.href = siteurl + "/login";
                }
                else
                {
                    jQuery(".ErrorMsg").html(response.error_msg);
                }
            }
        });
        
    });
    jQuery("#BTN_ORDER_CHECKOUT").on("click",function(e){
        e.preventDefault();
        var siteurl = jQuery("input[name=siteurl]").val();
        var str_params = "action=checkout";
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: str_params,
            method: 'post',
            dataType: "json",
            success: function (response) {
                if(response.is_error == 0)
                {
                    jQuery(".CashPopup .closeBtn").trigger("click");
                }
            }
        });
    });
    jQuery("#BTN_REGISTER").on("click",function(e){
        e.preventDefault();
        SignupUsers();
    });
    jQuery("#BTN_LOGIN").on("click",function(e){
          e.preventDefault();
           var msq_login = jQuery("input[name=msq_login]").val();
         var msq_password = jQuery("input[name=msq_password]").val();
          if(msq_login.length == 0 )
          {
              jQuery(".ErrorMsg").html("Login Email is Required !!" );
              return false;
          }
          
          if(msq_password.length == 0 )
          {
              jQuery(".ErrorMsg").html("Password is Required !!" );
              return false;
          }
   
        var siteurl = jQuery("input[name=siteurl]").val();
         var str_params = jQuery("#FORM_LOGIN").serialize();
          jQuery.ajax
            ({
                url: siteurl + "/ajax-requests",
                data: str_params,
                method: 'post',
                dataType: "json",
                success: function (response) {
                    if(response.is_error == 0)
                    {
                        
                        var link_url = jQuery('input[name=link_url]').val();
                        if(link_url == '')
                            window.location.href = siteurl + "/shop";
                        else
                            window.location.href = decodeURIComponent( link_url );
                    
                    }
                    else
                    {
                        jQuery(".ErrorMsg").html(response.error_msg);
                    }
                }
            });
    });
    jQuery("#BTN_SUBMIT_WEDDING_LIST").on("click",function(e){
          e.preventDefault();
          
         var wl_couple_name = jQuery("input[name=wl_couple_name]").val();
         var wl_phone = jQuery("input[name=wl_phone]").val();
         var wl_email = jQuery("input[name=wl_email]").val();
          
          
          if(wl_couple_name.length == 0 )
          {
              alert("Couple Name is Required !!" );
              return false;
          }
          
          if(wl_email.length == 0 )
          {
              alert("Couple Email is Required !!" );
              return false;
          }
          
          if(wl_phone.length == 0 )
          {
              alert("Couple Phone is Required !!" );
              return false;
          }
          
        var siteurl = jQuery("input[name=siteurl]").val();
         var str_params = jQuery("#FRM_CREATE_WEDDINGLIST").serialize();
          jQuery.ajax
            ({
                url: siteurl + "/ajax-requests",
                data: str_params,
                method: 'post',
                dataType: "json",
                success: function (response) {
                    if(response.is_error == 0)
                    {
                        jQuery("input[name=wl_couple_name]").val("");
                        jQuery("input[name=wl_phone]").val("");
                        jQuery("input[name=wl_email]").val("");
                        jQuery("input[name=wl_password]").val("");
                       //jQuery(".closeBtn").trigger("click");
                       jQuery(".WeddingListForm").slideUp('fast');
                       jQuery(".WeddingListConfirmationMsg").slideDown('fast');
                    }
                    else
                    {
                        alert(response.error_msg);
                    }
                }
            });
    });
});
