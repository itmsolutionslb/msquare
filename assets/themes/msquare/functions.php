<?php

/*
 *
 *  enqueues for styles and scripts
 */

/* Main start */
require_once(TEMPLATEPATH . '/includes/option-pages.php');
/* Main end */

require_once(TEMPLATEPATH . '/includes/enqueues.php');
/*
 * 
 *  setup
 */
if (!function_exists('marcbousleiman_setup')) :

    function marcbousleiman_setup() {
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
//        add_image_size('image_size_450_285', 450, 285, true);
    }

endif;

add_action('after_setup_theme', 'marcbousleiman_setup');

// removing admin bar from all users
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (is_user_logged_in()) {
        show_admin_bar(false);
    }
}

//removing tags from post page
add_action('init', 'remove_tags');

function remove_tags() {
    register_taxonomy('post_tag', array());
}

//function for my custom excerpt
function marc_custom_length($content, $count) {
    if (strlen($content) > $count) {
        $content = substr($content, 0, $count) . '...';
        echo $content;
    } else {
        echo $content;
    }
}

function custom_length_return($content, $count) {
    if (strlen($content) > $count) {
        $count = $count - 22;
//        $content = substr($content, 0, $count) . '...';
        $content = mb_substr($content, 0, $count, 'utf-8') . '...';
        return html_entity_decode($content);
    } else {
        return html_entity_decode($content);
    }
}

    
function woocommerceCategorySlug( $id )
{
    $term = get_the_terms( $id, 'product_cat'); 
    $category_slug = isset( $term[0]->slug ) ? $term[0]->slug : null;
    return $category_slug;
}

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
        
function woocommerce_ajax_add_to_cart() {
global $woocommerce;
    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
    $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
    $variation_id = isset($_POST['variation_id'])  ? absint($_POST['variation_id']) : 0;
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
    $product_status = get_post_status($product_id);

    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

        do_action('woocommerce_ajax_added_to_cart', $product_id);

        if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
            wc_add_to_cart_message(array($product_id => $quantity), true);
        }
        $items = $woocommerce->cart->get_cart();
        ob_start();
        
         foreach ($items as $key => $item_info) { 
            $_product =  wc_get_product( $item_info['data']->get_id());  
            $product_id = $item_info['data']->get_id();
            $thumb_image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'thumbnail' );
            $thumb_image_url = ( strlen($thumb_image[0]) > 0 ) ?  $thumb_image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";   
        ?>
        <li id="ITEM_<?= $product_id ?>">
            <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>">
                <div class="imageHolder"><img
                        src="<?= $thumb_image_url; ?>"
                        alt="Msquare Gallery" title="Msquare Gallery" /></div>
                <div class="descHolder" style="position: relative">
                    <h3><?= $_product->get_title(); ?></h3>
                    <h4><?= get_woocommerce_currency_symbol() ?> <?= $_product->get_price(); ?> </h4>
                    <span class="ItemQty">Qty:<span class="counter"><?= $item_info['quantity'] ?></span></span>
                </div>
            </a>
        </li> 
        <?php }
        
        
        $basket_content = ob_get_contents();
        ob_end_clean();
        
        $data = array(
            "basket_content" => $basket_content
        );
        
        echo json_encode($data);
        //WC_AJAX :: get_refreshed_fragments();
    } else {

        $data = array(
            'error' => true,
            'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

        echo wp_send_json($data);
    }

    wp_die();
 }

/*
 * 
 *  function tochange post page name
 */

//function revcon_change_post_object() {
//    global $wp_post_types;
//    $labels = &$wp_post_types['post']->labels;
//    $labels->name = 'Shops';
//    $labels->singular_name = 'Shop';
//    $labels->add_new = 'Add Shop';
//    $labels->add_new_item = 'Add Shop';
//    $labels->edit_item = 'Edit Shops';
//    $labels->new_item = 'Shops';
//    $labels->view_item = 'View Shops';
//    $labels->search_items = 'Search Shops';
//    $labels->not_found = 'No Shop found';
//    $labels->not_found_in_trash = 'No Shop found in Trash';
//    $labels->all_items = 'All Shops';
//    $labels->menu_name = 'Shops';
//    $labels->name_admin_bar = 'Shops';
//}
//
//add_action('init', 'revcon_change_post_object');

function extra_profile_fields( $user ) { ?>
   
    <h3><?php _e('Extra User Details'); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="gmail">Wedding List Approve</label></th>
            <td>
                <select name="wedding_list_approval">
                    <option value="1" <?php echo esc_attr( get_the_author_meta( 'wedding_list_approval', $user->ID ) ) == "1" ? "selected" : ""; ?> >Approve</option>
                    <option value="2" <?php echo esc_attr( get_the_author_meta( 'wedding_list_approval', $user->ID ) ) == "2" ? "selected" : ""; ?>  >Deny</option>
                </select> 
               
            <span class="description">Approve/Deny the Wedding List feature for the User.</span>
            </td>
        </tr> 
    </table>
<?php

}

// Then we hook the function to "show_user_profile" and "edit_user_profile"
add_action( 'show_user_profile', 'extra_profile_fields', 10 );
add_action( 'edit_user_profile', 'extra_profile_fields', 10 );


function save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;
    
    
    $wedding_list_approval = isset($_POST['wedding_list_approval']) ? $_POST['wedding_list_approval'] : 0;
    
    /* Edit the following lines according to your set fields */
    update_usermeta( $user_id, 'wedding_list_approval', $wedding_list_approval );
    
    // check if approval true send the info to the user
    if($wedding_list_approval == "1")
    {
        $user_info = get_userdata($user_id);
        
        $user_email = $user_info->user_email;
        $wl_couple_name = get_user_meta($user_id , "couple_name");
    // Send Email to the couple with new password
       $to      = $user_email;
        $email_content = "";
       ob_start();
       require 'inc/emails/wedding-registration-approved.php';
       $email_content = ob_get_contents();
       ob_end_clean();
       $subject = 'MSQUARE GALLERY - WEDDING LIST REGISTRATION APPROVAL';
       $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
       $image_url = get_template_directory_uri() . "/assets/images/emails/T3.png";
       $fbicon_url = get_template_directory_uri() . "/assets/images/emails/fbicon.png";
       $instaicon_url = get_template_directory_uri() . "/assets/images/emails/instaicon.png";
       $youtubeicon_url = get_template_directory_uri() . "/assets/images/emails/youtubeicon.png";
       $contactus_url = get_site_url() . "/contact-us";

       $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content); 
       $email_content = str_replace("%COUPLE_NAME%", $wl_couple_name, $email_content); 
       $email_content = str_replace("%FACEBOOK_ICON%", $fbicon_url, $email_content);  
       $email_content = str_replace("%FACEBOOK_URL%", "https://www.facebook.com/M-Square-Gallery-1212257732163727", $email_content);  
       $email_content = str_replace("%INSTAGRAM_ICON%", $instaicon_url, $email_content); 
       $email_content = str_replace("%INSTAGRAM_URL%", "https://www.instagram.com/msquare_gallery/", $email_content); 
       $email_content = str_replace("%YOUTUBE_ICON%", $youtubeicon_url, $email_content); 
       $email_content = str_replace("%YOUTUBE_URL%", "#", $email_content); 
       $email_content = str_replace("%CONTACTUS_URL%", $contactus_url, $email_content); 


       $mail = new PHPMailer(true); 
       $mail->Host = 'smtp.gmail.com';
       $mail->SMTPAuth = true;                           // Enable SMTP authentication
       $mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
       $mail->Password = 'UserPass456';                           // SMTP password
       $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
       $mail->Port = 587;                                    // TCP port to connect to
       $mail->IsHTML(true);
       $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
       $mail->addAddress($to, 'Msquare Gallery');
       $mail->Subject  = $subject;
       $mail->Body     = $email_content;
       $mail->send(); 
        
        
    }
 else {
     
     $user_info = get_userdata($user_id);
        
        $user_email = $user_info->user_email;
        $wl_couple_name = get_user_meta($user_id , "couple_name");
    // Send Email to the couple with new password
       $to      = $user_email;
        $email_content = "";
       ob_start();
       require 'inc/emails/wedding-registration-deny.php';
       $email_content = ob_get_contents();
       ob_end_clean();
       $subject = 'MSQUARE GALLERY - WEDDING LIST REGISTRATION DENIED';
       $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
       $image_url = get_template_directory_uri() . "/assets/images/emails/T3.png";
       $fbicon_url = get_template_directory_uri() . "/assets/images/emails/fbicon.png";
       $instaicon_url = get_template_directory_uri() . "/assets/images/emails/instaicon.png";
       $youtubeicon_url = get_template_directory_uri() . "/assets/images/emails/youtubeicon.png";
       $contactus_url = get_site_url() . "/contact-us";

       $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content); 
       $email_content = str_replace("%COUPLE_NAME%", $wl_couple_name, $email_content); 
       $email_content = str_replace("%FACEBOOK_ICON%", $fbicon_url, $email_content);  
       $email_content = str_replace("%FACEBOOK_URL%", "https://www.facebook.com/M-Square-Gallery-1212257732163727", $email_content);  
       $email_content = str_replace("%INSTAGRAM_ICON%", $instaicon_url, $email_content); 
       $email_content = str_replace("%INSTAGRAM_URL%", "https://www.instagram.com/msquare_gallery/", $email_content); 
       $email_content = str_replace("%YOUTUBE_ICON%", $youtubeicon_url, $email_content); 
       $email_content = str_replace("%YOUTUBE_URL%", "#", $email_content); 
       $email_content = str_replace("%CONTACTUS_URL%", $contactus_url, $email_content); 


       $mail = new PHPMailer(true); 
       $mail->Host = 'smtp.gmail.com';
       $mail->SMTPAuth = true;                           // Enable SMTP authentication
       $mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
       $mail->Password = 'UserPass456';                           // SMTP password
       $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
       $mail->Port = 587;                                    // TCP port to connect to
       $mail->IsHTML(true);
       $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
       $mail->addAddress($to, 'Msquare Gallery');
       $mail->Subject  = $subject;
       $mail->Body     = $email_content;
       $mail->send(); 
 }
    
}

add_action( 'personal_options_update', 'save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_profile_fields' );