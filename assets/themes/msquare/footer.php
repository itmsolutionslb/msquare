<?php 

{
    $taxonomy     = 'product_cat';
    $orderby      = 'name';  
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no  
    $title        = '';  
    $empty        = 0;

    $args = array(
           'taxonomy'     => $taxonomy,
           'orderby'      => $orderby,
           'show_count'   => $show_count,
           'pad_counts'   => $pad_counts,
           'hierarchical' => $hierarchical,
           'title_li'     => $title,
           'hide_empty'   => $empty
    );
   $all_categories = get_categories( $args );

}

?>

<footer>
    <div class="topPart">
        <div class="eachColumn firstCol">
            <h3><span></span>MSQUARE.<br>GALLERY</h3>
            <ul>
                <li><a href="<?= esc_url(home_url('/our-story')); ?>">The Story</a></li>
                <li><a href="<?= esc_url(home_url('/artists')); ?>">Artists</a></li>
                <li><a href="<?= esc_url(home_url('/whats-on')); ?>">What's On</a></li>
                <li><a href="<?= esc_url(home_url('/contact-us')); ?>">Contact</a></li>
            </ul>
        </div>
        <div class="eachColumn secondCol">
            <h3><span></span>SHOP</h3>
            <ul>
              <?php 
                    foreach ($all_categories as $cat) { 
                        $category_slug = $cat->slug;
                        if( $category_slug == "uncategorized" || $category_slug == "cash-gift" || $category_slug == "our-picks"  || $category_slug == "sales" || $category_slug == "corporate-gifts" || $category_slug=="curated-weekly" )
                            continue;
                            $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                            $image = wp_get_attachment_url( $thumbnail_id );
                            $siteurl = get_option("siteurl");

                    ?>
                    <li><a href="<?= esc_url(home_url('/shop?cat_name=')) . $category_slug; ?>"><?php echo $cat->name; ?></a></li>
                <?php 
                    }
                ?> 
            </ul>
        </div>
        <div class="eachColumn thirdCol">
            <h3><span></span>POLICIES</h3>
            <ul>
                <li><a href="<?= esc_url(home_url('/shipping')); ?>">Shipping</a></li>
                <li><a href="<?= esc_url(home_url('/faq')); ?>">FAQ</a></li>
                <li><a href="<?= esc_url(home_url('/terms')); ?>">Terms</a></li>
            </ul>
        </div>
        <div class="eachColumn forthCol">
            <h3>STAY UPDATED</h3>
            <form>
                <input type="text" placeholder="E-MAIL" />
                <input type="submit" value="Join" />
            </form>
        </div>
        <div class="eachColumn fifthCol">
            <h3>FOLLOW US</h3>
            <ul>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
            </ul>
        </div>
        <div class="currencyAndCountry">
            <div class="selectorWrap">
                <label>Currency</label>
                <select>
                    <option value="$ USD">$ USD</option>
                    <option value="EUR">EUR</option>
                </select>
            </div>
            <div class="selectorWrap">
                <label>Country</label>
                <select>
                    <option value="United States">United States</option>
                    <option value="Lebanon">Lebanon</option>
                </select>
            </div>
        </div>
    </div>
    <div class="bottomPart">
        <h1 class="artGallery titleFont">ART GALLERY</h1>
    </div>
</footer>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/main.js" ></script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/600760c5a9a34e36b96e3289/1esed419s';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>

</html>