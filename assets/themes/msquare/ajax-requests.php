<?php
/*
 *
 *  Template name: Ajax Requests
 * 
 */
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

{
   $action = isset($_POST["action"]) ? $_POST["action"] : "";  
}

require(ABSPATH . "/wp-load.php");


$directory_path = get_template_directory();
$plugin_path = ABSPATH . "assets/plugins";
     

require $directory_path .'/PHPMailer/src/Exception.php';
require $directory_path .'/PHPMailer/src/PHPMailer.php';
require $directory_path .'/PHPMailer/src/SMTP.php';
require $plugin_path .'/yith-woocommerce-wishlist/init.php';
//require $plugin_path .'/aramex-shipping-woocommerce/includes/aramexcalculator/class-aramex-woocommerce-aramexcalculator.php';

     


 switch($action)
 {
     case "registration":
     {
          $msq_fullname  = $_POST["fullname"];
         $msq_email     = $_POST["msqemail"];
         $msq_password  = $_POST["msqpassword"];
         
         // check email if exist
         $user_exist = username_exists($msq_email);
         if(is_numeric($user_exist))
         {
               $result_array['is_error'] = 1;
               $result_array['error_msg'] = "An account with this email exists. Please enter your password to log in or click the link below if you forgot it.";
               
                echo json_encode($result_array);
                exit;
         }
         
         $name_array = explode(" ", $msq_fullname);
         
         $user = array(
                'user_login' => $msq_email,
                'user_pass' => $msq_password,
                'user_email' => $msq_email,
                'display_name' => $msq_fullname, 
                'role' => 'customer'
         ); 
        $user_id = wp_insert_user($user);
        add_user_meta($user_id, "phone", ""); 
        add_user_meta($user_id, "user_full_name", $msq_fullname);
        add_user_meta($user_id, "first_name", $name_array[0]);
        add_user_meta($user_id, "last_name", $name_array[1]);
        $result_array = array();
        $result_array["is_error"] = 0;
        $result_array["error_msg"] = "New User Has been Created !!";
    
         $to      = $msq_email ;
        $subject = 'MSQUARE GALLERY - REGISTRATION COMPLETED';

        
        
        $email_content = "";
        ob_start();
        require 'inc/emails/user-registration.php';
        $email_content = ob_get_contents();
        ob_end_clean();
        
        $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
        $image_url = get_template_directory_uri() . "/assets/images/emails/T3.png";
        $fbicon_url = get_template_directory_uri() . "/assets/images/emails/fbicon.png";
        $instaicon_url = get_template_directory_uri() . "/assets/images/emails/instaicon.png";
        $youtubeicon_url = get_template_directory_uri() . "/assets/images/emails/youtubeicon.png";
        $contactus_url = get_site_url() . "/contact-us";

        $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content); 
        $email_content = str_replace("%WELCOME_IMAGE%", $image_url, $email_content); 
        $email_content = str_replace("%FACEBOOK_ICON%", $fbicon_url, $email_content);  
        $email_content = str_replace("%FACEBOOK_URL%", "https://www.facebook.com/M-Square-Gallery-1212257732163727", $email_content);  
        $email_content = str_replace("%INSTAGRAM_ICON%", $instaicon_url, $email_content); 
        $email_content = str_replace("%INSTAGRAM_URL%", "https://www.instagram.com/msquare_gallery/", $email_content); 
        $email_content = str_replace("%YOUTUBE_ICON%", $youtubeicon_url, $email_content); 
        $email_content = str_replace("%YOUTUBE_URL%", "#", $email_content); 
        $email_content = str_replace("%CONTACTUS_URL%", $contactus_url, $email_content); 
        
        $mail = new PHPMailer(true);
        $mail->Host = 'smtp.gmail.com';
    	$mail->SMTPAuth = true;                           // Enable SMTP authentication
    	$mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
   	    $mail->Password = 'UserPass456';                           // SMTP password
    	$mail->SMTPSecure = 'tls'; 
    	$mail->Port = 587;                                    // TCP port to connect to
        $mail->IsHTML(true);
        $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
        $mail->addAddress($to, 'Msquare Gallery');
        $mail->Subject  = $subject;
        $mail->Body     = $email_content;
        if(!$mail->send()) {
            $result_array['is_error'] = 1; 
        }
        else {
            $result_array['is_error'] = 0;
            $result_array['error_msg'] = "Thank You For Your Intreset in msquare gallery , your User has been Created !!";
            $login_data = array();
            $login_data['user_login'] = $msq_email;
            $login_data['user_password'] = $msq_password;
            $login_data['remember'] = false;
 
            $user_verify = wp_signon( $login_data, false ); 
            
        }
        
        echo json_encode($result_array);
        exit;
     }
     break;
     case "send_request_product":
     {
         $rp_client_message     = $_POST["rp_client_message"];
         $rp_client_email       = $_POST["rp_client_email"];
         $rp_client_name        = $_POST["rp_client_name"];
         $product_id            = $_POST["product_id"];
         $product = wc_get_product( $product_id );
          $product_title       = $product->get_title();
        
        $requested_array = get_user_meta($user_id , "requested_products");
        if($requested_array == '')
        {
            $requested_array = array();
            $requested_array[] = $product_id;
        }
        else 
        {
            $requested_array = $requested_array[0];
            $requested_array[] = $product_id;
        }
       
        update_user_meta($user_id, "requested_products", $requested_array);
        
        $to      = "onlinestore@msquaregallery.com" ;
        $subject = 'MSQUARE GALLERY - SEND REQUEST PRODUCT';
        $email_title = 'Product Info Request From ' . $rp_client_name . ',<br/>';
        $message = 'Request For More information sent from ' . $product_title . '<br/>';
        $message .= 'here information received from  ' . $rp_client_name . ':<br/>';
        $message .= ' <b>Email:</b> ' . $rp_client_email . ':<br/>';
        $message .= ' <b>Message Received:</b> ' . $rp_client_message . ':<br/>';
        
        
        $email_content = "";
        ob_start();
        require 'inc/email-template.php';
        $email_content = ob_get_contents();
        ob_end_clean();
        
        $logo_url = get_template_directory_uri() . "/assets/images/logo.svg";
        
        $email_content = str_replace("%EMAIL_TITLE%", $email_title, $email_content);
        $email_content = str_replace("%EMAIL_CONTENT%", $message, $email_content);
        $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content);

        $mail = new PHPMailer(true);
        $mail->Host = 'smtp.gmail.com';
    	$mail->SMTPAuth = true;                           // Enable SMTP authentication
    	$mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
   	$mail->Password = 'UserPass456';                           // SMTP password
    	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    	$mail->Port = 587;                                    // TCP port to connect to
        $mail->IsHTML(true);
        $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
        $mail->addAddress($to, 'Msquare Gallery');
        $mail->Subject  = $subject;
        $mail->Body     = $email_content;
        if(!$mail->send()) {
            $result_array['is_error'] = 1; 
        }
        else {
            $result_array['is_error'] = 0;
            $result_array['error_msg'] = "Your Message Has been Sent !!";
        }
        
        // Send Message to User
        
        $to      = "onlinestore@msquaregallery.com" ;
        $subject = 'MSQUARE GALLERY - SEND REQUEST PRODUCT';
        $email_title = $subject . ',<br/>';
        $message = 'Dear Customer, <br/>';
        $message .= 'Thank you for getting in touch with us!<br/>';
        $message .= 'Your request was well received; we will get back to you shortly.<br/>';
        $message .= 'Best,<br/>';
        $message .= 'M.SQUARE GALLERY team.';
        
        
        $email_content = "";
        ob_start();
        require 'inc/email-template.php';
        $email_content = ob_get_contents();
        ob_end_clean();
        
        $logo_url = get_template_directory_uri() . "/assets/images/logo.svg";
        
        $email_content = str_replace("%EMAIL_TITLE%", $email_title, $email_content);
        $email_content = str_replace("%EMAIL_CONTENT%", $message, $email_content);
        $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content);

        $mail = new PHPMailer(true); 
        $mail->Host = 'smtp.gmail.com';
    	$mail->SMTPAuth = true;                           // Enable SMTP authentication
    	$mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
   	$mail->Password = 'UserPass456';                           // SMTP password
    	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    	$mail->Port = 587;                                    // TCP port to connect to
        $mail->IsHTML(true);
        $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
        $mail->addAddress($to, 'Msquare Gallery');
        $mail->Subject  = $subject;
        $mail->Body     = $email_content;
        if(!$mail->send()) {
            $result_array['is_error'] = 1; 
        }
        else {
            $result_array['is_error'] = 0;
            $result_array['error_msg'] = "Your Message Has been Sent !!";
        }
        
        echo json_encode($result_array);
        exit;
          
     }
     break;
     case "login":
     {
         $msq_email = $_POST["msq_login"];
         $msq_password = $_POST["msq_password"];
         $result_array = array();
	$remember = isset($_POST["msq_remember_me"]) ? true : false;
        
        
        if(strlen($msq_email) == 0)
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Email Address is Required";
            
            echo json_encode($result_array);
            exit;
        }
        
        if(strlen($msq_password) == 0)
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Password is Required";
            
            echo json_encode($result_array);
            exit;
        }
        
    
 
	$login_data = array();
	$login_data['user_login'] = $msq_email;
	$login_data['user_password'] = $msq_password;
	$login_data['remember'] = $remember;
 
	$user_verify = wp_signon( $login_data, false ); 
 
	if ( is_wp_error($user_verify) ) 
	{
	    $result_array['is_error'] = 1;
            $result_array['error_msg'] = "The email/password combination does not match our records.";
            
            echo json_encode($result_array);
            exit;
	 } 
         else 
         {	
	    $result_array['is_error'] = 0;
            
            echo json_encode($result_array);
	   exit();
	 }
        
        echo json_encode($result_array);
        exit;
         
     }
     break;
     case "register_wedding_list":
     {
        global $current_user;
        wp_get_current_user();
        
        
        $wl_couple_name     = $_POST["wl_couple_name"];
        $wl_email           = $_POST["wl_email"];
        $wl_phone           = $_POST["wl_phone"];
        $current_user_data  = $current_user->data;
        
        
        if(!isset($current_user_data))
        {
            $msq_password = rand(100000, 999999);
            
        // create a new user
            $user = array(
               'user_login' => $wl_email,
               'user_pass' => $msq_password,
               'user_email' => $wl_email,
               'display_name' => $wl_couple_name, 
               'role' => 'customer'
            ); 
           $user_id = wp_insert_user($user);
           
           update_user_meta($user_id , "has_couple" , 1);
           update_user_meta($user_id , "couple_name" , $wl_couple_name);
           update_user_meta($user_id , "couple_email" , $wl_email);
           update_user_meta($user_id , "couple_phone" , $wl_phone);         
           update_user_meta($user_id , "wedding_list_approval" , 2);         
           
           // send email with the new password
     
            $to_admin      = 'onlinestore@msquaregallery.com';
            $subject = 'MSQUARE GALLERY - New Wedding Couple';

            $email_title = "Dear Msquare Administrator";
            $message = 'A New Wedding Couple is registered on ' . date("Y-m-d") . ' :<br/>';
            $message .= 'Full Name :' . $wl_couple_name . '<br/>';
            $message .= 'Email :' . $wl_email. '<br/>';
            $message .= 'Phone Number :' . $wl_phone. '<br/>';

            $email_content = "";
            ob_start();
            require 'inc/email-template.php';
            $email_admin_content = ob_get_contents();
            ob_end_clean();

            $logo_url = get_template_directory_uri() . "/assets/images/logo.png";

            $email_admin_content = str_replace("%EMAIL_TITLE%", $email_title, $email_admin_content);
            $email_admin_content = str_replace("%EMAIL_CONTENT%", $message, $email_admin_content);
            $email_admin_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_admin_content);

            $mail_admin = new PHPMailer(true); 
            $mail_admin->Host = 'smtp.gmail.com';
            $mail_admin->SMTPAuth = true;                           // Enable SMTP authentication
            $mail_admin->Username = 'no-reply@msquaregallery.com';                 // SMTP username
            $mail_admin->Password = 'UserPass456';                           // SMTP password
            $mail_admin->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail_admin->Port = 587;                                    // TCP port to connect to
            $mail_admin->IsHTML(true);
            $mail_admin->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
            $mail_admin->addAddress($to_admin, 'Msquare Gallery');
            $mail_admin->Subject  = $subject;
            $mail_admin->Body     = $email_admin_content;
            $mail_admin->send();
            
            
            $to      = $wl_email;
             $email_content = "";
            ob_start();
            require 'inc/emails/wedding-registration.php';
            $email_content = ob_get_contents();
            ob_end_clean();
            $subject = 'MSQUARE GALLERY - WEDDING LIST REGISTRATION';
            $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
            $image_url = get_template_directory_uri() . "/assets/images/emails/T3.png";
            $fbicon_url = get_template_directory_uri() . "/assets/images/emails/fbicon.png";
            $instaicon_url = get_template_directory_uri() . "/assets/images/emails/instaicon.png";
            $youtubeicon_url = get_template_directory_uri() . "/assets/images/emails/youtubeicon.png";
            $contactus_url = get_site_url() . "/contact-us";

            $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content); 
            $email_content = str_replace("%COUPLE_NAME%", $wl_couple_name, $email_content); 
            $email_content = str_replace("%FACEBOOK_ICON%", $fbicon_url, $email_content);  
            $email_content = str_replace("%FACEBOOK_URL%", "https://www.facebook.com/M-Square-Gallery-1212257732163727", $email_content);  
            $email_content = str_replace("%INSTAGRAM_ICON%", $instaicon_url, $email_content); 
            $email_content = str_replace("%INSTAGRAM_URL%", "https://www.instagram.com/msquare_gallery/", $email_content); 
            $email_content = str_replace("%YOUTUBE_ICON%", $youtubeicon_url, $email_content); 
            $email_content = str_replace("%YOUTUBE_URL%", "#", $email_content); 
            $email_content = str_replace("%CONTACTUS_URL%", $contactus_url, $email_content); 


            $mail = new PHPMailer(true); 
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;                           // Enable SMTP authentication
            $mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
            $mail->Password = 'UserPass456';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to
            $mail->IsHTML(true);
            $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
            $mail->addAddress($to, 'Msquare Gallery');
            $mail->Subject  = $subject;
            $mail->Body     = $email_content;
            $mail->send();
        }
        else
        {
            $user_id = $current_user->ID;

            update_user_meta($user_id , "has_couple" , 1);
            update_user_meta($user_id , "couple_name" , $wl_couple_name);
            update_user_meta($user_id , "couple_email" , $wl_email);
            update_user_meta($user_id , "couple_phone" , $wl_phone);      
           update_user_meta($user_id , "wedding_list_approval" , 2);  
            
            
            $to_admin      = 'onlinestore@msquaregallery.com';
            $subject = 'MSQUARE GALLERY - New Wedding Couple';

            $email_title = "Dear Msquare Administrator";
            $message = 'A New Wedding Couple is registered on ' . date("Y-m-d") . ' :<br/>';
            $message .= 'Full Name :' . $wl_couple_name . '<br/>';
            $message .= 'Email :' . $wl_email. '<br/>';
            $message .= 'Phone Number :' . $wl_phone. '<br/>';

            $email_content = "";
            ob_start();
            require 'inc/email-template.php';
            $email_admin_content = ob_get_contents();
            ob_end_clean();

            $logo_url = get_template_directory_uri() . "/assets/images/logo.png";

            $email_admin_content = str_replace("%EMAIL_TITLE%", $email_title, $email_admin_content);
            $email_admin_content = str_replace("%EMAIL_CONTENT%", $message, $email_admin_content);
            $email_admin_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_admin_content);

            $mail_admin = new PHPMailer(true);  
            $mail_admin->Host = 'smtp.gmail.com';
            $mail_admin->SMTPAuth = true;                           // Enable SMTP authentication
            $mail_admin->Username = 'no-reply@msquaregallery.com';                 // SMTP username
            $mail_admin->Password = '70901010';                           // SMTP password
            $mail_admin->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail_admin->Port = 587;                                    // TCP port to connect to
            $mail_admin->IsHTML(true);
            $mail_admin->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
            $mail_admin->addAddress($to_admin, 'Msquare Gallery');
            $mail_admin->Subject  = $subject;
            $mail_admin->Body     = $email_admin_content;
            $mail_admin->send();
            
        }
        

        
        $result_array['is_error'] = 0;
        $result_array['error_msg'] = "Register Wedding List Has Been Completed";
        echo json_encode($result_array);
        exit;
     }
     break;
     case "listshop":
     {
        $result_array   = array();
        $page_number        = isset($_POST["page_number"]) ? $_POST["page_number"] : 1;
        $category_ids       = isset($_POST["category_ids"]) ? $_POST["category_ids"] : array();
        $style_ids          = isset($_POST["style_ids"]) ? $_POST["style_ids"] : array();
        $artists_ids        = isset($_POST["artists_ids"]) ? $_POST["artists_ids"] : array();
        $width_from         = isset($_POST["width_from"]) ? $_POST["width_from"] : "";
        $width_to           = isset($_POST["width_to"]) ? $_POST["width_to"] : "";
        $height_from        = isset($_POST["height_from"]) ? $_POST["height_from"] : "";
        $height_to          = isset($_POST["height_to"]) ? $_POST["height_to"] : "";
        $order_by           = isset($_POST["order_by"]) ? $_POST["order_by"] : "";
        $cat_name           = isset($_POST["cat_name"]) ? $_POST["cat_name"] : "";
         $nbr_rows_per_pages    = 30;
         
          if($page_number > 1)
           $skip = ( $page_number - 1 ) * $nbr_rows_per_pages ;
          else
           $skip = 0;
     
        $result_array["is_error"] = 0;
        
        //get list of products
        
        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 30,
            'paged' => $page_number
        ); 
        if($order_by != "Sort by")
        {
            switch ($order_by)
            {
                case "Newest":
                {
                    $args['orderby'] = "ID";
                    $args['order'] = "DESC";
                }
                break;
                case "Oldest":
                {
                      $args['orderby'] = "ID";
                    $args['order'] = "ASC";
                }
                break;
                case "A-to-Z":
                {
                    $args['orderby'] = "post_title";
                    $args['order'] = "ASC";
                }
                break;
                case "Z-to-A":
                {
                    $args['orderby'] = "post_title";
                    $args['order'] = "DESC";
                }
                break;
            }
        }
        else {

              $args['orderby'] = "menu_order";
              $args['order'] = "ASC";
        }
                
         if(count($category_ids) > 0)
         {
             $args['tax_query'] =  array(
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'term_id', // Or 'name' or 'term_id'
                    'terms'    => $category_ids,
                    'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'
                )
            );
         } 
         
         
         if($cat_name != "" && count($category_ids) == 0)
         {
             $args['tax_query'] =  array(
                array(
                    'taxonomy' => 'product_cat',
                    'field'    => 'name', // Or 'name' or 'term_id'
                    'terms'    => $cat_name
                )
            );
         }
         
         if(count($style_ids) > 0)
         {
             $params['tax_query'] = array(
                'relation'=>'OR', // 'AND' 'OR' ...
                 array(
                   'taxonomy'        => 'pa_style',
                    'field'           => 'term_id',
                    'terms'           =>  $style_ids,
                    'operator'        => 'IN'
                 )
            );
         }
         
         
         
         if(count($artists_ids) > 0)
         {
            $params['tax_query'] = array(
                'relation'=>'AND', // 'AND' 'OR' ...
                 array(
                   'taxonomy'        => 'pa_artist-name',
                    'field'           => 'term_id',
                    'terms'           =>  $artists_ids,
                    'operator'        => 'IN'
                 )
            );
         }
         
         
         $args['tax_query'] =  array(
             'relation'=>'AND', // 'AND' 'OR' ...
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug', // Or 'name' or 'term_id'
                'terms'    => array('cash-gift'),
                'operator' => 'NOT IN', // Excluded
            )
        );
                
         
         /**if( strlen($width_from) > 0 )
         {
              $params['tax_query'] = array(
                 array(
                   'taxonomy'        => 'pa_artist-name',
                    'field'           => 'term_id',
                    'terms'           =>  $artists_ids,
                    'operator'        => 'IN'
                 )
            );
         }*/
             $wishlists = YITH_WCWL_Wishlist_Factory::get_wishlists();
    $wishlist_id = 0;
    foreach ( $wishlists as $wishlist ) {
        $wishlist_id = $wishlist->get_id();
    }
    $wishlist      = yith_wcwl_get_wishlist( $wishlist_id );    
    
    
        $count_args = $args;
        unset($count_args['paged']);
        unset($count_args['posts_per_page']);
        $count_args['posts_per_page'] = -1;
        $all_products =  new WP_Query( $count_args );
                
        
        $count_products = count($all_products->posts);
     
        $total_pages = ceil( $count_products /$nbr_rows_per_pages );
        $total_pages = intval($total_pages);
        
        
        $lst_products = new WP_Query( $args );
        ob_start();
        require_once 'inc/products.php';
        $result_data = ob_get_contents();
        ob_end_clean();
        $result_array["display"] = $result_data;
        $result_array['total_pages'] = $total_pages;
        
        echo json_encode($result_array);
        exit;
     }
     break;
     case "checkout":
     {
     }
     break;
    case "shipment_rate":
    {
        $user_address = isset($_POST['user_address']) ? $_POST['user_address'] : 0;
        
        $customer_id    = get_current_user_id();
        $list_addresses = get_user_meta($customer_id , "shipping_address");
        $list_addresses_array = json_decode($list_addresses[0]);
        $address_info = $list_addresses_array[ $user_address ];
        $current_user = wp_get_current_user();
        $user_email = $current_user->user_email;
        
        $firstname  = $address_info->firstname;
        $lastname   = $address_info->lastname;
        $address    = $address_info->address;
        $country    = $address_info->country;
        $city       = $address_info->city;
        $state      = $address_info->state;
        $zip        = $address_info->zip;
        $phone      = $address_info->phone;
        
        $calculate_rate = new Aramex_Ratecalculator_Method();
        
        $post_data = array();
        $post_data["data"]["destination_country"]   = $address_info->country;
        $post_data["data"]["destination_firstname"] = $address_info->firstname;
        $post_data["data"]["destination_lastname"]  = $address_info->lastname;
        $post_data["data"]["destination_address"]   = $address_info->address;
        $post_data["data"]["destination_city"]      = $address_info->city;
        $post_data["data"]["destination_state"]     = $address_info->state;
        $post_data["data"]["destination_zip"]       = $address_info->zip;
        $post_data["data"]["destination_phone"]     = $address_info->phone;
        
        $response = $calculate_rate->run($post_data);
        
      
    }
    break;
    case "editaddress":
    {
        $firstname   = isset($_POST["firstname"]) ? $_POST["firstname"] : "";
        $lastname    = isset($_POST["lastname"]) ? $_POST["lastname"] : "";
        $address     = isset($_POST["address"]) ? $_POST["address"] : "";
        $country     = isset($_POST["country"]) ? $_POST["country"] : "";
        $city        = isset($_POST["city"]) ? $_POST["city"] : "";
        $state       = isset($_POST["state"]) ? $_POST["state"] : "";
        $zip         = isset($_POST["zip"]) ? $_POST["zip"] : "";
        $phone       = isset($_POST["phone"]) ? $_POST["phone"] : "";
        $address_id  = isset($_POST["address_id"]) ? $_POST["address_id"] : "";
        $sa_id  = isset($_POST["sa_id"]) ? $_POST["sa_id"] : 0;
        
        $user_id     = get_current_user_id();
        
        
        $existing_array = get_user_meta($user_id , "shipping_address");
        
       
        $existing_array = json_decode($existing_array[0]);
                
                
        
        $existing_array[$sa_id]->firstname = $firstname;
        $existing_array[$sa_id]->lastname = $lastname;
        $existing_array[$sa_id]->address = $address;
        $existing_array[$sa_id]->country = $country;
        $existing_array[$sa_id]->city = $city;
        $existing_array[$sa_id]->state = $state;
        $existing_array[$sa_id]->zip = $zip;
        $existing_array[$sa_id]->phone = $phone;
                
        
        //$existing_array[ $sa_id ] = $current_address_info;
        
         update_user_meta($user_id , "shipping_address", json_encode($existing_array));
        
        
       $result_array = array();
       $result_array['is_error'] = 0;
       echo json_encode($result_array);
        exit;
        
    }
    break;
    case "delete_address":
    {
        global $current_user;
        wp_get_current_user();
        $user_id = $current_user->ID;
        $sa_id  = $_POST['sa_id'];
        $existing_array = get_user_meta($user_id , "shipping_address");
        $existing_array = json_decode($existing_array[0]);
        unset($existing_array[$sa_id]);

         update_user_meta($user_id , "shipping_address", json_encode($existing_array));

         $result_array = array();
         $result_array['is_error'] = 0;
         echo json_encode($result_array);
         exit;
            
    }
    break;
    case "set_address_default":
    {
        global $current_user;
        wp_get_current_user();
        $user_id = $current_user->ID;
        $sa_id  = $_POST['sa_id'];
       
        update_user_meta($user_id , "default_shipping_address", $sa_id);

         $result_array = array();
         $result_array['is_error'] = 0;
         $result_array['error_msg'] = "Address Set As a Default";
         echo json_encode($result_array);
         exit;
            
    }
    break;
    case "display_gift_sending":
    {
         $user_id = get_current_user_id();
         
             $args = array(
                 'customer_id' => $user_id,
                   "meta_query" => array(
                       'key' => 'sender_id',
                       'value' => $user_id
                   )  
            );
           
            $orders = wc_get_orders($args); 
            ob_start(); 
            foreach ($orders as $key => $order_info) 
            {
                $order_items = $orders[$key]->get_items();
                $order_id = $orders[$key]->get_id();
                $order_data =$orders[$key]->get_data();
                $couple_name = $orders[$key]->get_meta('couple_name');
                foreach ( $order_items as $key => $item_info ) 
                {
                    
                   $product        = $item_info->get_product();
                    $ID = $product->get_id();
                     $product_title = $product->get_title();
                    $product_description = $product->get_description();
                    $product_width   = $product->get_width();
                    $product_height  = $product->get_height();
                    $product_length  = $product->get_length();
                    $regular_price  = $product->get_regular_price();
                    $sales_price    = $product->get_sale_price();
                            $product_price  = $product->get_price_html();
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
                    $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                    $artist_name = $product->get_attribute( 'pa_artist-name' );
                ?>
                    <div class="horizontalPoductWrap">
                    <div class="imageHolder">
                        <img src="<?= $product_image_url; ?>"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <div class="descHolder">
                        <h3 class="productName"><?= $product_title ?></h3>
                        <h4 class="artistName"><?= $artist_name ?></h4>
                        <h5 class="color moreInfo">Color: blue</h5>
                        <h5 class="dimensions moreInfo">Dimensions: W: <?= $product_width ?> x <?= $product_length ?>CM - H: <?= $product_height ?> x <?= $product_length ?>CM </h5>
                        <h5 class="orderDate moreInfo">Order date: <?=  $order_info->order_date ?></h5>
                        <h5 class="giftInfo moreInfo"><i class="fal fa-gift"></i> Gift sent to:  <?= $couple_name ?></h5>
                    </div>
                    <div class="priceHolder">
                        <h3><?= $product_price ?></h3>
                    </div>
                </div> 
                <?php 
                }
                
            } 
             
            $page_content = ob_get_contents();

            ob_end_clean();   
            
            
            $result_array = array();
       $result_array['is_error'] = 0;
       $result_array['page_content'] = $page_content;
       echo json_encode($result_array);
        exit;
    }
    break;
    case "display_received_gifts":
    {
             $user_id       = get_current_user_id();
             $couple_name   = get_user_meta($user_id , "couple_name");
                $couple_name = $couple_name[0];
            $args = array(
                 'customer_id' => $user_id,
               "meta_query" => array(
                   'key' => 'couple_name',
                   'value' => $couple_name
               )   
            ); 
            $received_gift = wc_get_orders($args); 
                
            ob_start(); 
            foreach ($received_gift as $key => $order_info) 
            {
                $order_items = $received_gift[$key]->get_items();
                $order_id = $received_gift[$key]->get_id();
                $order_data =$received_gift[$key]->get_data();
                $sender_id = $received_gift[$key]->get_meta('sender_id');
                $couple_name = $received_gift[$key]->get_meta('couple_name');
               $sender_info = get_userdata($sender_id);
               $sender_name = $sender_info->display_name;
               
                foreach ( $order_items as $key => $item_info ) 
                {
                    
                   $product        = $item_info->get_product();
                    $ID = $product->get_id();
                     $product_title = $product->get_title();
                    $product_description = $product->get_description();
                    $product_width   = $product->get_width();
                    $product_height  = $product->get_height();
                    $product_length  = $product->get_length();
                    $regular_price  = $product->get_regular_price();
                    $sales_price    = $product->get_sale_price();
                            $product_price  = $product->get_price_html();
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
                    $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                    $artist_name = $product->get_attribute( 'pa_artist-name' );
                ?>
                    <div class="horizontalPoductWrap">
                    <div class="imageHolder">
                        <img src="<?= $product_image_url; ?>"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <div class="descHolder">
                        <h3 class="productName"><?= $product_title ?></h3>
                        <h4 class="artistName"><?= $artist_name ?></h4>
                        <h5 class="color moreInfo">Color: blue</h5>
                        <h5 class="dimensions moreInfo">Dimensions: W: <?= $product_width ?> x <?= $product_length ?>CM - H: <?= $product_height ?> x <?= $product_length ?>CM </h5>
                        <h5 class="orderDate moreInfo">Order date: <?=  $order_info->order_date ?></h5>
                        <h5 class="giftInfo moreInfo"><i class="fal fa-gift"></i> From :  <?= $sender_name ?></h5>
                    </div>
                    <div class="priceHolder">
                        <h3><?= $product_price ?></h3>
                    </div>
                </div> 
                <?php 
                }
                
            } 
             
            $page_content = ob_get_contents();

            ob_end_clean();   
            
            
            $result_array = array();
       $result_array['is_error'] = 0;
       $result_array['page_content'] = $page_content;
       echo json_encode($result_array);
        exit;
            
                
             
    }
    break;
    case "addaddress":
    {
        $firstname  = isset($_POST["firstname"]) ? $_POST["firstname"] : "";
        $lastname   = isset($_POST["lastname"]) ? $_POST["lastname"] : "";
        $address    = isset($_POST["address"]) ? $_POST["address"] : "";
        $country    = isset($_POST["country"]) ? $_POST["country"] : "";
        $city       = isset($_POST["city"]) ? $_POST["city"] : "";
        $state       = isset($_POST["state"]) ? $_POST["state"] : "";
        $zip       = isset($_POST["zip"]) ? $_POST["zip"] : "";
        $phone       = isset($_POST["phone"]) ? $_POST["phone"] : "";
        
        $user_id = get_current_user_id();
        
        $existing_array = get_user_meta($user_id , "shipping_address");

        $index = 0;
        if(count($existing_array) == 0)
        {
                    $address_info[0] = array(
                "firstname" => $firstname,
                "lastname" => $lastname,
                "address" => $address,
                "country" => $country,
                "city" => $city,
                "state" => $state,
                "zip" => $zip,
                "phone" => $phone
            );
           add_user_meta( $user_id, "shipping_address", json_encode($address_info));
        }
        else
        {
            $existing_array = json_decode($existing_array[0]);
            $index = count($existing_array);
            $existing_array[count($existing_array)] = array(
                 "firstname" => $firstname,
                "lastname" => $lastname,
                "address" => $address,
                "country" => $country,
                "city" => $city,
                "state" => $state,
                "zip" => $zip,
                "phone" => $phone
            );
            update_user_meta( $user_id, "shipping_address", json_encode($existing_array));
        }
        
         ob_start();
        ?>
         <div class="eachAddressWrap">
            <div class="addressInfo">
                <span class="circle active"><input type="radio" name="user_address" checked="checked" style="display:none" value="<?= $index ?>" /></span>
                <div class="detailsHolder">
                    <h4><?= $firstname ?> <?= $lastname ?></h4>
                    <h4><?= $address ?></h4>
                    <h4><?= WC()->countries->countries[ $country ]; ?></h4>
                    <h4><?= $phone ?></h4>

                </div>
            </div>
        </div> 

        <?php 
        $address_item = ob_get_contents();
        ob_end_clean();
       
       $result_array = array();
       $result_array['is_error'] = 0;
       $result_array['address_item'] = $address_item;
       echo json_encode($result_array);
        exit;
    }
    break;
     case "get_product_variance":
     {
         $term_id       = $_POST['term_id'];
         $product_id    = $_POST['product_id'];
         $product = wc_get_product( $product_id );
     }
     break;
     case "cash_amount":
     {
        $cash_amount = isset($_POST['cg_amount']) ? $_POST['cg_amount'] : 0;
        $user_id = get_current_user_id();
        $post_array = array(
           'post_author' => $user_id,
           'post_parent' => 0,
           'post_type' =>"product",
           'post_status' => 'Publish',
           'post_date' => date("Y-m-d H:i:s"),
           'post_title' => "Cash Amount",
           'post_content' => "Cash Amount",
        );
        $product_id = wp_insert_post($post_array);
       
        add_post_meta($product_id,'_regular_price',$cash_amount);
        add_post_meta($product_id,'_price',$cash_amount);
        
        $term = get_term_by('name', 'Cash Gift', 'product_cat');

        wp_set_object_terms($product_id, $term->term_id, 'product_cat');
        
        WC()->cart->add_to_cart( $product_id );
          
        
        $result_array = array();
        $result_array['is_error'] = 0;
        
        echo json_encode($result_array);
     }
     break;
    case "send_reset_password":
    {
        // Reset Password 
        $email_address = isset($_POST['user_email']) ? $_POST['user_email'] : "";
        
                
        $user_info =  get_user_by_email($email_address );
       
       $result_array = array();
        
        if(!is_numeric($user_info->data->ID))
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Please Enter A valid Email";
            echo json_encode($result_array);
            exit;
        }
                
        
        $user_id = $user_info->data->ID;
        $full_name = $user_info->data->display_name;  
         
         $email_content = "";
        ob_start();
        require get_template_directory() . '/inc/emails/reset-password.php';
        $email_content = ob_get_contents();
        
        ob_end_clean();
        
        $user_info = base64_encode( "abc" . $user_id . "abc" );
            $siteurl = get_site_url();
        $new_url = $siteurl . "/save-new-password?info=" . $user_info;
        
        $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
        $siteurl = get_site_url();
        
        $subject = "MSQUARE GALLERY - RESET PASSWORD";
         $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
        $image_url = get_template_directory_uri() . "/assets/images/emails/T3.png";
        $fbicon_url = get_template_directory_uri() . "/assets/images/emails/fbicon.png";
        $instaicon_url = get_template_directory_uri() . "/assets/images/emails/instaicon.png";
        $youtubeicon_url = get_template_directory_uri() . "/assets/images/emails/youtubeicon.png";
        $contactus_url = get_site_url() . "/contact-us";

        $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content); 
        $email_content = str_replace("%RESET_PASSWORD_LINK%", $new_url, $email_content); 
        $email_content = str_replace("%USER_FULLNAME%", $full_name, $email_content); 
        $email_content = str_replace("%FACEBOOK_ICON%", $fbicon_url, $email_content);  
        $email_content = str_replace("%FACEBOOK_URL%", "https://www.facebook.com/M-Square-Gallery-1212257732163727", $email_content);  
        $email_content = str_replace("%INSTAGRAM_ICON%", $instaicon_url, $email_content); 
        $email_content = str_replace("%INSTAGRAM_URL%", "https://www.instagram.com/msquare_gallery/", $email_content); 
        $email_content = str_replace("%YOUTUBE_ICON%", $youtubeicon_url, $email_content); 
        $email_content = str_replace("%YOUTUBE_URL%", "#", $email_content); 
        $email_content = str_replace("%CONTACTUS_URL%", $contactus_url, $email_content); 
    
        $mail = new PHPMailer(true);
        
        $mail->Host = 'smtp.gmail.com';
    	$mail->SMTPAuth = true;                           // Enable SMTP authentication
    	$mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
   	$mail->Password = '70901010';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    	$mail->Port = 587;   
        $mail->IsHTML(true);
        $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
        $mail->addAddress($email_address,$full_name);
        $mail->Subject  = $subject;
        $mail->Body     = $email_content;
        $mail->send();
 
	$result_array['is_error'] = 0;
        echo json_encode($result_array);
    }
    break;
    case "change_password":
    {
        $user_id        = isset($_POST["user_id"]) ? $_POST["user_id"] : 0;
        $user_password  = isset($_POST["user_password"]) ? $_POST["user_password"] : "";
        $user_retype_password = isset($_POST["user_retype_password"]) ? $_POST["user_retype_password"] : "";
        $result_array = array();
        
        if($user_password != $user_retype_password)
        {
            $result_array['is_error'] = 1;
            $result_array['error_msg'] = "Passwords not compatible";
            echo json_encode($result_array);
            exit;
        }
        
        
        wp_set_password($user_password, $user_id);
        
        
         $result_array['is_error'] = 0;
        echo json_encode($result_array);
    }
    break;
    case "remove_product":
    {
       $product_id = $_POST['product_id'];
       
       $cartId = WC()->cart->generate_cart_id($product_id);
        $cartItemKey = WC()->cart->find_product_in_cart( $cartId );
        WC()->cart->remove_cart_item( $cartItemKey );
        $result_array = array();
        
        $result_array['is_error'] = 0;
        echo json_encode($result_array);
    }
    break;
    case "add_remove_wishlist":
    {
        $product_id     = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
         $user_id       = get_current_user_id();
         $user_data     = get_userdata($user_id);
         
         
         $result_array = array();
         
         if( $user_data == null )
         {
             $result_array['is_error'] = 1;
             
             echo json_encode($result_array);
             exit;
         }
         
         $wishlist_product_ids = get_user_meta($user_id , "wishlist_product_ids");
          
         $wishlist_product_ids = $wishlist_product_ids[0];
         
         if( $wishlist_product_ids == "")
             $wishlist_product_ids = array();
             
         $index = array_search($product_id, $wishlist_product_ids);
         if( $index !== FALSE )
         {
             $result_array['action'] = "remove";
             unset($wishlist_product_ids[$index]);
         }
         else
         {
             $result_array['action'] = "add";
             $wishlist_product_ids[count($wishlist_product_ids) + 1 ] = $product_id;
         }
         
         update_user_meta($user_id, "wishlist_product_ids", $wishlist_product_ids);
         $result_array['is_error'] = 0;
         $result_array['count'] = count($wishlist_product_ids);
 
         echo json_encode($result_array);
    }
    break;
    case "remove_wishlist":
    {
        $product_id     = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
         $user_id       = get_current_user_id();
         $user_data     = get_userdata($user_id);
         $result_array = array();
         
         if( $user_data == null )
         {
             $result_array['is_error'] = 1;
             
             echo json_encode($result_array);
             exit;
         }
         
         $wishlist_product_ids = get_user_meta($user_id , "wishlist_product_ids");
         $wishlist_product_ids = $wishlist_product_ids[0];
         $index = array_search($product_id, $wishlist_product_ids);
         
         unset($wishlist_product_ids[$index]);
         
          update_user_meta($user_id, "wishlist_product_ids", $wishlist_product_ids);
         $result_array['is_error'] = 0;
         $result_array['count'] = count($wishlist_product_ids);
         echo json_encode($result_array);
    }
    break;
    case "remove_product_bag":
    {
        $product_id     = isset($_POST["product_id"]) ? $_POST["product_id"] : 0;
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            if ( $cart_item['product_id'] == $product_id ) {
                 WC()->cart->remove_cart_item( $cart_item_key );
            }
       }
       $items = $woocommerce->cart->get_cart();
       
         $result_array['is_error'] = 0;
         $result_array['count'] = count($items);
         echo json_encode($result_array);
    }
    break;
 }