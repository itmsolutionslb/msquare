<?php
/*
 *
 *  Template name: Single shop
 * 
 */
{
    global $current_user;
    wp_get_current_user();
    $user_id = get_current_user_id();
    


   $product_id = isset($_GET["ID"]) ? $_GET["ID"] : 1;
   $product = wc_get_product( $product_id );
   $category_ids = wc_get_product_category_list($product_id); 
   $product_title       = $product->get_title();
   $product_description = $product->get_description();
   $product_width       = $product->get_width();
   $product_height      = $product->get_height();
   $product_length      = $product->get_length();
   $product_weight      = $product->get_weight();
 
   $regular_price       = $product->get_regular_price();
   $sales_price         = $product->get_sale_price();
   $product_cart_url    = $product->add_to_cart_url();
   
   $is_variable = $product->is_type( 'variable' ) ? 1 : 0;
   
   $artist_name = $product->get_attribute( 'pa_artist-name' );
   
   $artist_slug = strtolower( str_replace(" ","-", $artist_name)  );
   
   $args = array(
    'name'        => $artist_slug,
    'post_type'   => 'portfolio',
    'post_status' => 'publish',
    'numberposts' => 1
  );
  $artist_info = get_posts($args);
  if(count($artist_info) > 0)
   $artist_id = $artist_info[0]->ID;
  else
   $artist_id = 0;
  
  $url_artist = "#";
  
  if($artist_id > 0)
  {
      $url_artist = home_url() . "/single-artist/?id=" . $artist_id;
  } 
  
  
  
    $attachment_ids = $product->get_gallery_image_ids();
        if ( $is_variable == 1 ) {
            $available_variations = $product->get_available_variations(); //get all child variations
            $variation_variations = $product->get_variation_attributes(); // get all attributes by variations

            $list_color_terms = get_the_terms($product_id,"pa_color");
        }
     // get related products
     $list_categories = wp_get_post_terms($product_id , 'product_cat' , array(
         'fields' => 'ids'
     ));
     $category_id = $list_categories[0];
     
      $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 4,
            'paged' => 1
           // 'product_cat'    => 'hoodies'
        );
        
         $args['meta_query'] =  array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'term_id', // Or 'name' or 'term_id'
                'terms'    => $list_categories,
                'operator' => 'IN', // Excluded
            )
        );
         
          $args['tax_query'] =  array(
             'relation'=>'AND', // 'AND' 'OR' ...
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug', // Or 'name' or 'term_id'
                'terms'    => array('cash-gift'),
                'operator' => 'NOT IN', // Excluded
            )
        );
        

    $lst_products = new WP_Query( $args );


    $thumb_image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'thumbnail' );
    $thumb_image_url = ( strlen($thumb_image[0]) > 0 ) ?  $thumb_image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";

    $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'full' );
    $full_image_url = ( strlen($full_image[0]) > 0 ) ?  $full_image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
    $siteurl = get_site_url();
    $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    
    //add_to_card
    
    $open_card = isset($_GET['add_to_card']) ? true : false;
                                                
}

{
    $product_ids = array();
       $product_ids = get_user_meta($user_id , "wishlist_product_ids");
     if(is_array($product_ids) && count($product_ids) > 0)
        $product_ids = $product_ids[0];
     else
         $product_ids = array();
    
    
    $requested_array = array();
    $requested_array = get_user_meta($user_id , "requested_products");
    
    if(is_array($requested_array) && count($requested_array) > 0)
    {
                                                
            $requested_array = $requested_array[0];
            
        $requested_price = ( array_search($product_id , $requested_array) !== FALSE ) ? 1 : 0;
    }
    else
    {
        $requested_price = 0;
    }
    
    // get current url
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' )
        $url = "https://";
    else
        $url = "http://";
    
    $url .= $_SERVER['HTTP_HOST'];
    $url .= $_SERVER['REQUEST_URI'];
    
    $permalink_url = $url;
    
    $login_url = "";
    
    if($user_id == 0)
    {
        $login_url = esc_url(home_url('/login')) . "?link_url=" . urldecode($permalink_url);
    }
    else
    {
        $login_url = "#";
    }
    
}



get_header(); ?>
<span id="hidden_fields">
    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
    <input type="hidden" name="requested_price" value="<?php echo $requested_price; ?>" />
    <input type="hidden" name="siteurl" value="<?php echo $siteurl ?>" />
    <input type="hidden" name="permalink_url" value="<?php echo $permalink_url ?>" />
</span>
<div class="singleShopPage">
    <div class="innerPage">
        <div class="topSection">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 leftSection">
                        <div class="innerLeftSection">
                            <a class="backBtn" href="<?= esc_url(home_url('/shop')); ?>">
                                <i class="fal fa-long-arrow-left"></i>
                                <span>BACK TO SHOP</span></a>
                            <div class="innerInfoHolder">
                                <?php 
                                    $wishlist_url = "";
                                    $background_style = "";
                                    $icon_class = "fal fa-heart";
                                    if(in_array($product_id, $product_ids))
                                    {
                                        $wishlist_url = esc_url( add_query_arg( 'remove_from_wishlist', $product_id ) );
                                        $background_style  = "color:#FEC512";
                                        $icon_class = "fas fa-heart";
                                    }
                                    else 
                                    {
                                        $wishlist_url =  esc_url( add_query_arg( 'add_to_wishlist', $product_id, $siteurl ) );
                                    }
                                ?>
                                
                                <a class="addToWishList"><span data-product_id="<?= $product_id ?>"  class="addToWishlist"><i  style="<?= $background_style ?>" class="<?= $icon_class ?>"></i></span></a>
                                <h1 class="title"><?php echo ucfirst( strtolower($product_title));  ?></h1>
                                <h4 class="artist"><a href="<?=  $url_artist ?>"><?= $artist_name ?></a></h4>
                                <div class="content"><?= $product_description ?></div>
                                <div class="subInfoHolder">
                                    <h4>DIMENSIONS</h4>
                                    <ul>
                                        <li><?php echo $product_width; ?> x <?php echo $product_height; ?>CM</li>
                                    </ul>
                                    <a href="#" class="shippingTrigger openGeneralPopup" data-usage="dimensionpopup">View Shipping Dimensions</a>
                                </div>
                                <?php if ( $is_variable == "1" ) { ?>
                                <div class="subInfoHolder">
                                    <h4>COLORS</h4>
                                    <div class="colorsHolder">
                                        <?php  
                                         foreach ($list_color_terms as $key => $term_info) {
                                             $term_id           = $term_info->term_id;
                                             $term_data_meta    = get_term_meta($term_id); 
                                            $swatches_id_type   = $term_data_meta["pa_color_swatches_id_type"][0];
                                            switch($swatches_id_type)
                                            {
                                                case "color":
                                                {
                                                     $swatches_color = $term_data_meta["pa_color_swatches_id_color"][0];
                                                    ?>
                                        <div class="color ProductVariance" style="background-color:<?php echo $swatches_color; ?>;" data-term_id="<?php echo $term_id; ?>"></div>
                                                    <?php
                                                }
                                                break;
                                                case "photo":
                                                {
                                                    $file_id           = $term_data_meta["pa_color_swatches_id_photo"][0];
                                                    $file_attached = wp_get_attachment_url($file_id);
                                                    ?>
                                        <div class="color ProductVariance" style="background:url('<?php echo $file_attached; ?>')" data-term_id="<?php echo $term_id; ?>"></div>
                                                    <?php 
                                                }
                                                break;
                                            }
                                         }
                                        
                                        ?>
                                    </div>
                                </div>
                                <?php }?>
                                 <?php if( $regular_price > 0 ){ ?>
                                <button type="button" name="btn_add_to_card" id="BTN_ADD_TO_CARD" class="triggerBtn">ADD TO CARD</button>
                                
                        <!--<a href="<?= $product_cart_url ?>&ID=<?= $product_id ?>" class="triggerBtn">ADD TO CARD</a> -->
                        <?php }else{ ?>
                        <a href="<?= $login_url ?>" class="<?php echo (($user_id == 0) ? "" : "openGeneralPopup"); ?> triggerBtn RequestPriceBtn" data-usage="formpopup">REQUEST PRICE</a>
                        <a href="#" class="triggerBtn RequestedtBtn"  style="display:none">PRICE REQUESTED</a>
                        <?php } ?>
                                <div class="moreInfo">
                                    <ul>
                                        <li>
                                            <i class="truckIcon customIcon"></i>
                                            <span><strong>Shipping:</strong> Standard to anywhere in the world, arrives
                                                in
                                                8-13 days.</span>
                                        </li>
                                        <li>
                                            <i class="arrowIcon customIcon"></i>
                                            <span><strong>Return policy:</strong> Items cannot be returned</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <a class="backBtn" href="<?= esc_url(home_url('/artists')); ?>">
                                <i class="fal fa-long-arrow-left"></i>
                                <span>BACK TO SHOP</span></a>
                        <div class="innerRightSection">
                            <div class="swiper-container singleGallery">
                                <div class="swiper-wrapper">
                                     <div class="swiper-slide">
                                        <img src="<?= $full_image_url; ?>"
                                            alt="Msquare Gallery item <?= $product_title ?>" title="Msquare Gallery <?= $product_title ?>" />
                                    </div>
                                    <?php 
                                        foreach( $attachment_ids as $attachment_id ) {
                                            $image_link = wp_get_attachment_url( $attachment_id );
                                     ?>
                                    <div class="swiper-slide">
                                        <img src="<?= $image_link; ?>"
                                            alt="Msquare Gallery item <?= $product_title ?>" title="Msquare Gallery <?= $product_title ?>" />
                                    </div>
                                    <?php } ?> 
                                </div>
                            </div>
                            <div class="swiper-container singleGalleryThumbs">
                                <div class="swiper-wrapper"> 
                                     <div class="swiper-slide">
                                        <img src="<?= $thumb_image_url; ?>"
                                            alt="Msquare Gallery  <?= $product_title ?>" title="Msquare Gallery <?= $product_title ?>" />
                                    </div> 
                                    <?php 
                                        foreach( $attachment_ids as $attachment_id ) {
                                            $image_link = wp_get_attachment_url( $attachment_id );
                                     ?>
                                    <div class="swiper-slide">
                                        <img src="<?= $image_link; ?>"
                                            alt="Msquare Gallery  <?= $product_title ?>" title="Msquare Gallery <?= $product_title ?>" />
                                    </div> 
                                        <?php } ?>
                                    
                                </div>
                            </div>
                        </div>
                         <?php if( $regular_price > 0 ){ ?>
                         <button type="button" name="btn_add_to_card" id="BTN_ADD_TO_CARD" class="triggerBtn">ADD TO CARD</button>
                        <!--<a href="<?= $product_cart_url ?>&ID=<?= $product_id ?>" class="triggerBtn">ADD TO CARD</a>-->
                        <?php }else{ ?>
                        <a href="<?= $login_url ?>" class="<?php echo (($user_id == 0) ? "" : "openGeneralPopup"); ?> triggerBtn RequestPriceBtn" data-usage="formpopup">REQUEST PRICE</a>
                        <a href="#" class="triggerBtn RequestedtBtn" style="display:none">PRICE REQUESTED</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <section class="mightAlsoLikeSection">
            <div class="container">
                <h2 class="sectionTitle titleFont">You might also like</h2>
                <div class="row">
                    <?php 
                        foreach ($lst_products->posts as $key => $post_info) {

                            $product_id = $post_info->ID;
                            $product = wc_get_product( $product_id );
                            $regular_price  = $product->get_regular_price();
                            $sales_price    = $product->get_sale_price();
                            $product_price  = $product->get_price_html();
                            $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
                            $cat_id = (int)$term_list[0];

                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_info->ID ), 'single-post-thumbnail' );
                            $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                            ?>
                        <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>"
                            class="col-lg-3 col-md-3 col-sm-6 col-xs-6 productWrap <?= (strlen($sales_price) > 0 ) ? "sale" : "" ?>">
                            <div class="imageHolder">
                                <span class="addToWishlist"><i class="fal fa-heart"></i></span>
                                <img src="<?= $product_image_url; ?>"
                                    alt="Msquare Gallery Product <?= $post_info->post_title ?>" title="Msquare Gallery Product <?= $post_info->post_title ?>" />
                            </div>
                            <div class="descHolder">
                                <h3><?= ucfirst( strtolower($post_info->post_title)) ?></h3>
                                <h4><?= $product_price ?></h4>
                                <?php if(strlen($sales_price) > 0 ){ ?>
                                <h4 class="sale-price"><?= $sales_price ?></h4>
                                <?php } ?>
                            </div>
                        </a>
                            <?php 
                        }

                        ?>
                </div>
            </div>
        </section>
    </div>
    <div class="generalPopup formsPopup" data-usage="formpopup">
        <div class="innerGeneralPopup SendMessageForm" data-simplebar data-simplebar-auto-hide="false">
            <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
            <h1 class="popupTitle">Send a message</h1><br/>
            <span class="msg_result"></span>
            <form name="frm_send_request" id="FRM_SEND_REQUEST">
                <input type="hidden" name="action" value="product_request" />
                <div class="fieldWrapper">
                    <label>Name*</label>
                    <input type="text" required="required" name="rp_client_name" value="" />
                </div>
                <div class="fieldWrapper">
                    <label>Message</label>
                    <textarea rows="4"  required="required" name="rp_client_message"></textarea>
                </div>
                <div class="fieldWrapper submitWrapper">
                    <input type="submit" name="btn_send_request" id="BTN_SEND_REQUEST" value="SEND" />
                </div>
            </form>
        </div>
        <div class="innerGeneralPopup SuccessSendMsg" style="display: none" data-simplebar data-simplebar-auto-hide="false">
            <h2>You’re request has been sent!</h2>
            <span style="width:100%;text-align: center">We will be sending you the item’s price shortly. Please check your <br/>email to view details</span>
        </div>
    </div>
    <div class="generalPopup formsPopup" data-usage="dimensionpopup">
        <div class="innerGeneralPopup" data-simplebar data-simplebar-auto-hide="false">
            <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
            <h1 class="popupTitle">Product Dimension</h1><br/>
            <div class="row">
                <div class="col-md-6">
                    <label>Width&nbsp;:</label>
                </div>
                <div class="col-md-6">
                    <?= $product_width ?>&nbsp;CM
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Height&nbsp;:</label>
                </div>
                <div class="col-md-6">
                    <?= $product_height ?>&nbsp;CM
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Length&nbsp;:</label>
                </div>
                <div class="col-md-6">
                    <?= $product_length ?>&nbsp;CM
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Weight&nbsp;:</label>
                </div>
                <div class="col-md-6">
                    <?= $product_weight ?>
                </div>
            </div>
        </div>
    </div>
    
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/shop.js" ></script>
<?php get_footer();