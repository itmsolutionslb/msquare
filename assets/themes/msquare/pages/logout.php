<?php
/*
 *
 *  Template name: Logout
 * 
 */
{
    $url = home_url("/");
    wp_logout();
    header("location:" . $url);
    exit;
}
?>