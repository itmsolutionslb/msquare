<?php
/*
 *
 *  Template name: Checkout
 * 
 */

{
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    $siteurl = get_site_url();
    
    $customer_id = get_current_user_id();
    if($customer_id == 0)
    {
        $permalink = get_permalink();
        
        header("location:".$siteurl . "/login?link_url=" . urldecode($permalink));
        exit;
    }
}

{
   $category_info = get_term_by( 'slug', 'cash-gift', 'product_cat' );
   $cash_category_id = $category_info->term_id;
   

   
   $list_addresses = get_user_meta($customer_id , "shipping_address");
 
    if($list_addresses != "" && count($list_addresses) > 0){
        $list_addresses_array = json_decode($list_addresses[0]);
    }
     else {

         $list_addresses_array = array();
     }
      
   
    $lst_couples = get_users(array('meta_key' => 'has_couple'));
    
    $lst_couple_array = array();
    
    foreach ( $lst_couples as $key => $couple_info ) {
        $user_id = $couple_info->ID; 
        $couple_name = get_user_meta($user_id,"couple_name");
        $lst_couple_array[ $user_id ] = $couple_name[0];
    }
    
    // check if we have one product cash we dont need a shipment and shipment details
    $has_cash = 0;
    /**foreach ($items as $key => $item_info) { 
        $ID = $item_info['data']->get_id();
        $product =  wc_get_product( $ID ); 
        
        $post = get_post($ID);
        
        
        if( has_term( $cash_category_id, 'product_cat' , $post ) ) {
            $has_cash = 1;
        }
    }
   $has_cash = 0;  */
} 

 

get_header();
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<input type="hidden" name="siteurl" value="<?= $siteurl ?>" />
<div class="withPageIdentifier checkoutPage myBagPage">
    <div class="pageIdentifier">
        <div class="subColumn leftSection">
            <a href="<?= esc_url(home_url('/')); ?>">
                <img class="firstLogo" src="<?= get_template_directory_uri(); ?>/assets/images/logo.svg"
                     alt="Msquare Gallery logo" title="Msquare Gallery" />
            </a>
        </div>
        <div class="subColumn middleSection">
            <h1>Checkout</h1>
        </div>
        <div class="subColumn rightSection">
            <h3>Secured payment</h3>
        </div>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <div class="blocksHolder">
                        <div class="innerBlock shippingAddress <?= ($has_cash == 0) ? "active" : "" ?>" style="<?= ($has_cash == 0) ? "" : "display:none" ?>">
                            <h3 class="blockTitle">1. Shipping Address</h3>
                            <div class="blockBody">
                                <h4 class="blockSubTitle">Provide your address to see available shipping methods below.</h4>
                                <div class="formHolder">
                                    <form name="frm_address_info" id="FRM_ADDRESS_INFO">
                                        <input type="hidden" name="action" value="addaddress" />
                                         <div class="fieldWrapper cf halfed first">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">First name*</label>
                                                <div class="fl w-100">
                                                    <input type="text" name="firstname" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="FIRSTNAME" value="" />
                                                </div>
                                            </div>
                                        </div> 
                                         <div class="fieldWrapper cf halfed second">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">Last Name*</label>
                                                <div class="fl w-100">
                                                    <input type="text" name="lastname" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="LASTNAME" value="" />
                                                </div>
                                            </div>
                                        </div>  
                                         <div class="fieldWrapper cf">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">Address *</label>
                                                <div class="fl w-100">
                                                    <input type="text" name="address" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="ADDRESS" value="" />
                                                </div>
                                            </div>
                                        </div>   
                                         <div class="fieldWrapper cf">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">Country *</label>
                                                <div class="fl w-100">
                                                      <select name="country" id="COUNTRY" class="input-reset ba b--black-20 pa2 mb2 db w-100" rel="country">
                                                            <option value="default"><?php esc_html_e( 'Select a country / region&hellip;', 'woocommerce' ); ?></option>
                                                            <?php
                                                            foreach ( WC()->countries->get_shipping_countries() as $key => $value ) {
                                                                    echo '<option value="' . esc_attr( $key ) . '"' . selected( WC()->customer->get_shipping_country(), esc_attr( $key ), false ) . '>' . esc_html( $value ) . '</option>';
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> 
                                         <div class="fieldWrapper cf halfed first">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">City *</label>
                                                <div class="fl w-100">
                                                    <input type="text" name="city" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="CITY" value="" />
                                                </div>
                                            </div>
                                        </div>   
                                         <div class="fieldWrapper cf halfed second">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">State *</label>
                                                <div class="fl w-100">
                                                    <input type="text" name="state" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="STATE" value="" />
                                                </div>
                                            </div>
                                        </div>   
                                         <div class="fieldWrapper cf">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">Zip *</label>
                                                <div class="fl w-100">
                                                    <input type="text" name="zip" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="ZIP" value="" />
                                                </div>
                                            </div>
                                        </div>   
                                         <div class="fieldWrapper cf">
                                            <div class="fl w-100">
                                                <label class="fl w-100 pa2">Phone *</label>
                                                <div class="fl w-100">
                                                    <input type="text" name="phone" class="input-reset ba b--black-20 pa2 mb2 db w-100" id="PHONE" value="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fieldWrapper submitWrapper">
                                            <input type="submit" value="SAVE ADDRESS" name="btn_save_address" id="BTN_SAVE_ADDRESS" class="generalBtn" />
                                        </div>
                                    </form>
                                </div>
                                <div class="addressesHolder">
                                    <div class="ListAddresses">
                                    <?php 
                                        foreach ($list_addresses_array as $key => $addresses_info) {  
                                         
                                      ?>
                                        <div class="eachAddressWrap">
                                            <div class="addressInfo">
                                                <span class="circle"><input type="radio" name="user_address" style="display:none" value="<?= $key ?>" /></span>
                                                <div class="detailsHolder">
                                                    <h4><?= $addresses_info->firstname ?> <?= $addresses_info->lastname ?></h4>
                                                    <h4><?= $addresses_info->address ?></h4>
                                                    <h4><?= WC()->countries->countries[ $addresses_info->country ]; ?></h4>
                                                    <h4><?= $addresses_info->phone ?></h4>
                                                    
                                                </div>
                                            </div>
                                        </div> 
                                    <?php } ?> 
                                        
                                    </div>
                                    <a href="#!" class="eachAddressWrap">
                                        <div class="addressInfo">
                                            <span class="circle active"><input type="radio" name="user_address" style="display:none" checked="checked" value="-1" /></span>
                                            <div class="detailsHolder">
                                                <h4>Add New Address</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="innerBlock shippingMethod" style="<?= ($has_cash == 1) ? "display:none" : "" ?>">
                            <h3 class="blockTitle">2. Shipping Method</h3>
                            <div class="blockBody" style="display:none">
                                <h4 class="blockSubTitle">Provide your address to see available shipping methods below.</h4>
                                <div class="addressesHolder">
                                    <div class="eachAddressWrap">
                                        <div class="addressInfo">
                                            <span class="circle active"><input type="radio" name="shipping_method" checked style="display:none" value="1" /></span>
                                            <div class="detailsHolder">
                                                <h4>$<span class="StandardPrice"></span> Standard, arrives in 8 to 13 days</h4>
                                                <h4>Delivered by a parcel delivery Aramex. Signature is required at delivery.</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="eachAddressWrap">
                                        <div class="addressInfo">
                                            <span class="circle"><input type="radio" name="shipping_method" style="display:none" value="2" /></span>
                                            <div class="detailsHolder">
                                                <h4><span class="ExpressPrice"></span>$ Express, arrives in 4 days</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="generalBtn">CONTINUE</a>
                            </div>
                        </div>
                        <!--<div class="innerBlock paymentMethod <?= ($has_cash == 0) ? "active" : "" ?>">
                            <h3 class="blockTitle">3. Payment Method</h3>
                            <div class="blockBody" style="<?= ($has_cash == 1) ? "display:none" : "" ?>">
                                <div class="addressesHolder">
                                    <div class="eachAddressWrap">
                                        <div class="addressInfo">
                                            <span class="circle active"></span>
                                            <div class="detailsHolder">
                                                <h4>Credit Card</h4>
                                                <img src="<?= get_template_directory_uri(); ?>/assets/images/payment-methods.png" alt="" title="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <form name="frm_place_order" action="<?= $siteurl ?>/check-out-process" method="post">
                        <span id="hidden_fields">
                            <input type="hidden" name="gift_notes" value="" />
                            <input type="hidden" name="has_cash" value="<?= $has_cash ?>" />
                            <input type="hidden" name="couple_name" value="" />
                            <input type="hidden" name="couple_notes" value="" />
                            <input type="hidden" name="user_address_value" value="-1" />
                        </span>
                        <div class="innerRightSection">
                            <h3 class="sectionTitle withBackSquare">ITEMS (<?= count($items); ?>)</h3>
                            <div class="productsHolder">
                                 <?php 
                                    foreach ($items as $key => $item_info) { 
                                        $ID = $item_info['data']->get_id();
                                        $product =  wc_get_product( $item_info['data']->get_id()); 
                                        $product_title = $product->get_title();
                                        $product_description = $product->get_description();
                                        $product_width   = $product->get_width();
                                        $product_height  = $product->get_height();
                                        $product_length  = $product->get_length();
                                        $regular_price  = $product->get_regular_price();
                                        $sales_price    = $product->get_sale_price();
                                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
                                        $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                                        $artist_name = $product->get_attribute( 'pa_artist-name' );
                                        $category_slug = woocommerceCategorySlug($ID);
                                     ?>
                                    <div class="horizontalPoductWrap">
                                        <div class="imageHolder">
                                            <img src="<?= $product_image_url; ?>"
                                                 alt="Msquare Gallery logo" title="Msquare Gallery" />
                                        </div>
                                        <div class="descHolder">
                                            <h3 class="productName"><?= $product_title ?>x<?= $item_info['quantity'] ?></h3>
                                            <?php if($category_slug != "cash-gift"){ ?>
                                            <h5 class="color moreInfo">Color: blue</h5>
                                            <?php if($product_width != "" && $product_length != "" ){ ?>
                                            <h5 class="dimensions moreInfo">Dimensions: W: <?= $product_width ?> x <?= $product_length ?></h5>
                                            <h5 class="dimensions moreInfo">H: <?= $product_height ?> x <?= $product_length ?></h5>
                                            <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="priceHolder">
                                            <h3><?= $regular_price ?><?= get_woocommerce_currency_symbol() ?></h3>
                                        </div>
                                    </div>
                                 <?php } ?>
                            </div>
                            <div class="separatedInnerContent subtotal">
                                <h4>Subtotal</h4>
                                <h4><?= $woocommerce->cart->get_cart_subtotal() ?></h4>
                            </div>
                            <div class="separatedInnerContent shippingInfo">
                                <h4 class="ShippingTitle">STANDARD SHIPPING (<span class="StandardPrice"></span>$)</h4>
                                <h4 class="hasArrow"><i class="fal fa-angle-down"></i></h4>
                                <div class="shippingInfoToolTip">
                                    <ul>
                                        <li class="active"><a href="#!"><i class="far fa-check"></i> Standard Shipping (<span class="StandardPrice"></span>$)</a></li>
                                        <li><a href="#!"><i class="far fa-check"></i> Express Shipping - 3 DAYS (<span class="ExpressPrice"></span>$)</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="separatedInnerContent insurance" style="position: relative">
                                <h4>Insurance <span class="infoMark">i</span></h4>
                                <h4>$3.00</h4>
                                <div class="insuranceInfoToolTip">lurame epsome lurame epsome lurame epsome <br/>lurame epsome lurame epsome lurame epsome</div>
                            </div>
                            <div class="separatedInnerContent total bolded">
                                <h4>Total</h4>
                                <h4><?= $woocommerce->cart->get_cart_total() ?></h4>
                            </div>
                            <div class="separatedInnerContent sendAsGiftHolder">
                                <h4><i class="fal fa-gift"></i> SEND AS A GIFT</h4>
                                <h4><span class="square unchecked"></span></h4>
                            </div>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/poweredbyareeba.png" style="width:150px;margin-bottom:15px" /><br/>
                            <button type="button" class="generalBtn BtnPlaceOrder" >PLACE ORDER</button>
                            <h4 class="generalNotice">*no cancellation & no returns</h4>
                        </div>
                        
                    </form> 
                </div>
            </div>
        </div>
    </div>
    <div class="generalPopup formsPopup">
        <div class="innerGeneralPopup" data-simplebar data-simplebar-auto-hide="false">
            <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
            <h1 class="popupTitle">Send as a gift</h1>
            <form name="frm_send_gift" id="FRM_SEND_GIFT">
                                        <input type="hidden" name="action" value="setasgift" />
                <div class="fieldWrapper radioBtnsHolder">
                    <div class="radioBtns MsgType">
                        <span class="circle unchecked"><input type="radio" class="RdMsgType" name="msg_type" value="1" style="display:none" /></span>
                        <h4>Gift</h4>
                    </div>
                    <div class="radioBtns MsgType">
                        <span class="circle checked"><input type="radio" class="RdMsgType" name="msg_type" value="2" style="display:none" /></span>
                        <h4>Wedding gift</h4>
                    </div>
                </div>
                <div class="fieldWrapper CoupleName ui-widget">
                    <label for="tags">Couples name*</label>
                    <input type="text" id="tags" name="txtCoupleName" required value="" />
                </div>
                <div class="fieldWrapper CoupleNotes">
                    <label>Add a note for the couple</label>
                    <textarea rows="4" name="txtCoupleNote" placeholder="Type here..." val=""></textarea>
                </div>
                <div class="fieldWrapper WeedingNote" style="display:none">
                    <label>Gift Note</label>
                    <textarea rows="4" name="txtGiftNote" placeholder="Type here..." val=""></textarea>
                </div>
                <div class="fieldWrapper submitWrapper">
                    <input type="submit" value="DONE" name="btn_send_gift" />
                </div>
            </form>
        </div>
    </div>
</div>
<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" 
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-shim.min.js"></script>    
<script src="<?php echo get_template_directory_uri(); ?>/src/javascript/formvalidation/dist/js/FormValidation.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/src/javascript/formvalidation/dist/js/plugins/Tachyons.min.js"></script>
<script>
 // Generate a simple captcha
    function CreateNewAddressHandleForm(){
        const form = document.getElementById('FRM_ADDRESS_INFO');
    FormValidation.formValidation(form, {
        fields: {
            firstname: {
                validators: {
                    notEmpty: {
                        message: 'The First name is required'
                    }
                }
            },
            lastname: {
                validators: {
                    notEmpty: {
                        message: 'Last Name is required'
                    }
                }
            },
            address : {
                validators: {
                    notEmpty: {
                        message: 'Address is required'
                    }
                }
            },
            country : {
                validators: {
                    notEmpty: {
                        message: 'Country is required'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'city is required'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            tachyons: new FormValidation.plugins.Tachyons(),
            submitButton: new FormValidation.plugins.SubmitButton(),
            icon: new FormValidation.plugins.Icon({
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }),
        }
    }); 
    }
    
    
    
    $(document).ready(function() {
        CreateNewAddressHandleForm(); 
    });
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/sweetalert2/src/sweetalert2.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/checkout.js" ></script>
<script type="text/javascript">
    jQuery( function() {
    var availableTags = [
      <?php  foreach ($lst_couple_array as $user_id => $couple_name) {  ?>
      "<?php echo $couple_name; ?>",
        <?php } ?>
    ]; 
    $( "input[name=txtCoupleName]" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
<?php
get_footer();
