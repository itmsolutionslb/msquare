<?php
/*
 *
 *  Template name: My Wishlist
 * 
 */

{
    global $current_user,$woocommerce;
    wp_get_current_user();
    $display_name = $current_user->display_name;
    $name_array = explode(" ", $display_name);
    $full_name = $name_array[0];
    
    $user_id       = get_current_user_id();
    
        $product_ids = get_user_meta($user_id , "wishlist_product_ids");
     if(is_array($product_ids) && count($product_ids) > 0)
        $product_ids = $product_ids[0];
     else
         $product_ids = array();
    $siteurl = get_site_url();
}

get_header(); ?>
<input type="hidden" name="siteurl" value="<?= $siteurl ?>" />
<div class="myWishList myAccountPages withPageIdentifier">
    <div class="pageIdentifier">
        <h1>My Account</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <h2 class="userName">Hello, <?php echo $full_name; ?></h2>
                    <div class="innerLeftSection">
                        <h3 class="pageName">WISHLIST <i class="far fa-plus"></i></h3>
                        <ul>
                            <li><a href="<?= esc_url(home_url('/my-orders')); ?>">MY ORDERS</a></li>
                            <li><a href="<?= esc_url(home_url('/account-details')); ?>">ACCOUNT DETAILS</a></li>
                            <li><a href="<?= esc_url(home_url('/addresses')); ?>">ADDRESSES</a></li>
                            <li class="active"><a href="#">WISHLIST (<?= count($product_ids) ?>) <i class="far fa-check"></i></a></li>
                            <li><a href="<?= esc_url(home_url('/wedding-list-info')); ?>"><i class="fal fa-gift"></i>
                                    WEDDING
                                    LIST</a></li>
                        </ul>
                    </div>
                </div> 
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <div class="innerRightSection">
                        <h2 class="sectionTitle">Wishlist</h2>
                        <section class="ourPicksSection productListSection">
                            <div class="container">
                                <div class="row"> 
                                    	<?php
                                        if( count($product_ids) > 0 )
                                        {
                                          foreach ($product_ids as $key => $product_id) 
                                         {
                                             $product = wc_get_product( $product_id );
                                             $availability = $product->get_availability();
                                            $stock_status = isset( $availability['class'] ) ? $availability['class'] : false;

                                            $product_id = $product->get_id();
                                           $regular_price       = $product->get_regular_price();
                                            $product_title  = $product->get_name();
                                            $sales_price    = $product->get_sale_price();
                                            $product_price  = $product->get_price_html();
                                            $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
                                            $cat_id = (int)$term_list[0];
                                                $product_cart_url    = $product->add_to_cart_url();
                                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );
                                            $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                                               $product_cart_id = WC()->cart->generate_cart_id( $product_id );
                                                $in_cart = WC()->cart->find_product_in_cart( $product_cart_id ); 
                                                
                                              ?>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 productWrap">
                                                    <div class="imageHolder">
                                                        <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>">
                                                            <img src="<?= $product_image_url; ?>"
                                                                alt="Msquare Gallery <?= $product_title ?>" title="Msquare Gallery" />
                                                        </a>
                                                    </div>
                                                    <div class="descHolder">
                                                        <h3><?= ucfirst( strtolower( $product->get_name() )) ?></h3>
                                                        <h4><?= $regular_price ?></h4>
                                                        <?php if(strlen($sales_price) > 0 ){ ?>
                                                        <h4 class="sale-price"><?= $sales_price ?></h4>
                                                        <?php } ?>
                                                        <?php if( $regular_price > 0 ){ ?>
                                                        <a href="<?= $product_cart_url ?>&ID=<?= $product_id ?>" class="generalBtn">ADD TO CART</a>
                                                        <?php } ?>
                                                        <a href="#" data-product_id="<?= $product_id ?>" class="removeBtn removeWishlist removeProduct">remove</a>
                                                    </div>
                                                </div>
                                                <?php 
                                                
                                         }  
                                        }
                                        else
                                        {
                                            ?>
                                                <div class="col-md-12" align="center">
                                                        <i style="font-size:24px;" class="fal fa-heart"></i><br/>
                                                        <h4>No items are added yet!</h4><br/>
                                                         <a href="<?= $siteurl ?>/shop" class="generalBtn">START SHOPPING</a>
                                                    </div>
                                             <?php
                                        }
                                    
                                    ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();