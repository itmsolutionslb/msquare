<?php
/*
 *
 *  Template name: My Orders
 * 
 */

{
    
    $customer_id = get_current_user_id();
    $siteurl = get_site_url();
    if($customer_id == 0)
    {
        $permalink = get_permalink();
        
        header("location:".$siteurl . "/login?link_url=" . urlencode($permalink));
        exit;
    }
    
    global $current_user,$woocommerce;
    wp_get_current_user();
 
    $display_name = $current_user->display_name;
    $name_array = explode(" ", $display_name);
    $full_name = $name_array[0];
    $count_wishlist = yith_wcwl_count_all_products(); 
    
    
    $user_id = $current_user->ID;
    
    $args = array(
        'customer_id' => $user_id
    );
    $orders = wc_get_orders($args);
    

    
}

get_header(); ?>
<div class="myOrdersPage myAccountPages withPageIdentifier">
    <div class="pageIdentifier">
        <h1>My Account</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <h2 class="userName">Hello, Maya</h2>
                    <div class="innerLeftSection">
                        <h3 class="pageName">MY ORDERS <i class="far fa-plus"></i></h3>
                        <ul>
                            <li class="active"><a href="#">MY ORDERS <i class="far fa-check"></i></a></li>
                            <li><a href="<?= esc_url(home_url('/account-details')); ?>">ACCOUNT DETAILS</a></li>
                            <li><a href="<?= esc_url(home_url('/addresses')); ?>">ADDRESSES</i></a></li>
                            <li><a href="<?= esc_url(home_url('/wishlist')); ?>">WISHLIST (<?= $count_wishlist ?>)</a></li>
                            <li><a href="<?= esc_url(home_url('/wedding-list-info')); ?>"><i class="fal fa-gift"></i>
                                    WEDDING LIST</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <div class="innerRightSection">
                        <h2 class="sectionTitle">Orders History</h2>
                        <?php foreach ($orders as $key => $order_info) { ?>
                             <div class="eachOrderWrap">
                                <div class="topOrderWrap">
                                    <h3>Order Sent</h3>
                                    <h4>Order NO.: <?= $orders[$key]->get_id() ?></h4>
                                    <h4>Order date: <?=  $orders[$key]->get_date_created() ?></h4>
                                    <h4>Estimated delivery:</h4>
                                    <a href="#" class="viewOrderBtn">View Order +</a>
                                    <a href="#" class="trackOrderBtn">TRACK ORDER</a>
                                </div>
                                <div class="horizontalPoductWrap">
                                    <?php 
                                        $order_items = $orders[$key]->get_items();
                                    ?>
                                    <?php  foreach ($order_items as $items_key => $items_value) {   
                                        
                                        $product        = $items_value->get_product();
                                        $ID = $product->get_id();;
                                         $product_title = $product->get_title();
                                        $product_description = $product->get_description();
                                        $product_width   = $product->get_width();
                                        $product_height  = $product->get_height();
                                        $product_length  = $product->get_length();
                                        $regular_price  = $product->get_regular_price();
                                        $sales_price    = $product->get_sale_price();
                                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
                                        $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                                        $artist_name = $product->get_attribute( 'pa_artist-name' );
                                        ?>
                                        <div class="imageHolder">
                                           <img src="<?= $product_image_url; ?>"
                                               alt="Msquare Gallery <?= $product_title ?>" title="Msquare Gallery" />
                                       </div>
                                       <div class="descHolder">
                                           <h3 class="productName"><?= $product_title ?></h3>
                                           <h4 class="artistName"><?= $artist_name ?></h4>
                                           <h5 class="color moreInfo"></h5>
                                           <h5 class="dimensions moreInfo">Dimensions: W: <?= $product_width ?> x <?= $product_length ?>CM - H: <?= $product_height ?> x <?= $product_length ?>CM </h5>
                                       </div>
                                       <div class="priceHolder">
                                           <h3><?= $regular_price ?>$</h3>
                                       </div>
                                    <?php } ?>
                                   
                                </div>
                            </div>
                       <?php } ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();