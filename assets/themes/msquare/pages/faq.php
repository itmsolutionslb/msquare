<?php
/*
 *
 *  Template name: FAQ
 * 
 */
get_header(); ?>
<div class="faqPage withPageIdentifier">
    <div class="pageIdentifier">
        <h1>Faq's</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="eachFaqWrap">
                <h2><span>1.</span><?php echo get_field("first_question"); ?></h2>
                <div class="content"><?php echo get_field("first_answer"); ?></div>
            </div>
            <div class="eachFaqWrap">
                <h2><span>2.</span><?php echo get_field("second_question"); ?></h2>
                <div class="content"><?php echo get_field("second_answer"); ?></div>
            </div>
            <div class="eachFaqWrap">
                <h2><span>3.</span><?php echo get_field("third_question"); ?></h2>
                <div class="content"><?php echo get_field("third_answer"); ?></div>
            </div>
            <div class="eachFaqWrap">
                <h2><span>4.</span><?php echo get_field("fourth_question"); ?></h2>
                <div class="content"><?php echo get_field("fourth_answer"); ?></div>
            </div>
            <div class="eachFaqWrap">
                <h2><span>5.</span><?php echo get_field("fifth_question"); ?></h2>
                <div class="content"><?php echo get_field("fifth_answer"); ?></div>
            </div>
            <div class="eachFaqWrap">
                <h2><span>6.</span><?php echo get_field("sixth_question"); ?></h2>
                <div class="content"><?php echo get_field("sixth_answer"); ?></div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();