<?php
/*
 *
 *  Template name: Save new pass
 * 
 */

{
    $siteurl = get_site_url();
}


{
    $info = isset($_GET["info"]) ? $_GET["info"] : "";
    
    $user_data = base64_decode($info);
    
    $user_id = str_replace("abc", "", $user_data);
    
}

get_header(); ?>
<input type="hidden" name="siteurl" value="<?= $siteurl ?>" />
<div class="forgotPassPage formsPage paddingTop">
    <div class="innerPage">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <div class="innerLeftSection ForgotForm">
                        <h1 class="titleFont">Change your password?</h1>
                        <form name="frm_change_password" id="FRM_CHANGE_PASSWORD" method="post">
                            <input type="hidden" name="action" value="change_password" />
                            <input type="hidden" name="user_id" value="<?= $user_id ?>" />
                            <span class="ErrorMsg"></span>
                            <div class="fieldWrapper">
                                <label>Password</label>
                                <input type="password" name="user_password" required="required" value="" id="USER_PASSWORD" />
                            </div>
                            <br/>
                            <div class="fieldWrapper">
                                <label>Retype Password</label>
                                <input type="password" name="user_retype_password"  required="required" value="" id="USER_RETYPE_PASSWORD" />
                            </div>
                            <br/>
                            <div class="fieldWrapper submitWrapper">
                                <input type="submit" value="CHANGE PASSWORD" class="triggerBtn" name="btn_change_password" id="BTN_CHANGE_PASSWORD" />&nbsp;&nbsp;<img src="<?= get_template_directory_uri(); ?>/assets/images/msquare-loader.gif" style="width:63px;visibility: hidden" class="ImgLoader" />
                            </div>
                        </form>
                    </div> 
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 rightSection">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img19.png"
                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/users.js" ></script>
<?php get_footer();