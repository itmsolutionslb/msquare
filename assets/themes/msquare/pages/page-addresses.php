<?php
/*
 *
 *  Template name: Addresses
 * 
 */

{
    
    
    $customer_id = get_current_user_id();
    $siteurl = get_site_url();
    if($customer_id == 0)
    {
        $permalink = get_permalink();
        
        header("location:".$siteurl . "/login?link_url=" . urlencode($permalink));
        exit;
    }
    
    global $current_user,$woocommerce;
    wp_get_current_user();
    $display_name = $current_user->display_name;
    $name_array = explode(" ", $display_name);
    $full_name = $name_array[0];
    $count_wishlist = yith_wcwl_count_all_products();
    $customer_id = get_current_user_id();
   $siteurl = get_site_url();
    
}

{
   $list_addresses = get_user_meta($customer_id , "shipping_address");
   $default_shipping_address = get_user_meta($customer_id , "default_shipping_address");
   
   
   if(count($default_shipping_address) > 0)
    $default_shipping_address = $default_shipping_address[0];
else 
    $default_shipping_address = 0;
    
   if(count($list_addresses) > 0)
        $list_addresses_array = json_decode($list_addresses[0]);
   else
        $list_addresses_array = array();
} 
get_header(); ?> 
<div class="addressesPage myAccountPages withPageIdentifier">
    <input type="hidden" name="siteurl" value="<?php echo $siteurl ?>" />
    <div class="pageIdentifier">
        <h1>My Account</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 leftSection"> 
                    <h2 class="userName">Hello, <?= $full_name ?></h2>
                    <div class="innerLeftSection">
                        <h3 class="pageName">ADDRESSES <i class="far fa-plus"></i></h3>
                        <ul>
                            <li><a href="<?= esc_url(home_url('/my-orders')); ?>">MY ORDERS</a></li>
                            <li><a href="<?= esc_url(home_url('/account-details')); ?>">ACCOUNT DETAILS</a></li>
                            <li class="active"><a href="#">ADDRESSES <i class="far fa-check"></i></a></li>
                            <li><a href="<?= esc_url(home_url('/wishlist')); ?>">WISHLIST (<?= $count_wishlist ?>)</a></li>
                            <li><a href="<?= esc_url(home_url('/wedding-list-info')); ?>"><i class="fal fa-gift"></i>
                                    WEDDING
                                    LIST</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <div class="innerRightSection">
                        <h2 class="sectionTitle">Addresses</h2>
                        <div class="addressesHolder">
                            <?php 
                            
                             foreach ($list_addresses_array as $i => $addresses_info ){  
                                ?>
                                <div class="eachAddressWrap">
                                    <div class="addressInfo">
                                        <span class="circle <?php echo ($default_shipping_address == $i) ? "active" : ""; ?>"><input type="radio" name="rb_address" value="<?= $i ?>"  <?php echo ($default_shipping_address == $i) ? "checked='checked'" : ""; ?> style="display: none" /></span>
                                        <div class="detailsHolder">
                                            <h4><?php echo $addresses_info->firstname . " " . $addresses_info->lastname ?></h4>
                                            <h4><?= $addresses_info->address ?></h4>
                                            <h4><?= WC()->countries->countries[ $addresses_info->country ]; ?></h4>
                                            <h4><?= $addresses_info->phone ?></h4>
                                        </div>
                                    </div>
                                    <div class="actionsHolder">
                                        <!--<a href="<?= $siteurl ?>/edit-address/?id=<?= $i ?>"><i class="far fa-edit"></i> EDIT</a>
                                        <a href="#" class="DeleteAddress" data-sa_id="<?= $i ?>"><i class="fas fa-trash-alt"></i> DELETE</a>--> 
                                         <a href="#" class="removeBtn DeleteAddress" data-sa_id="<?= $i ?>" >remove</a>
                                    </div>
                                </div>
                            <?php } ?> 
                        </div>
                        <a href="<?= $siteurl ?>/new-address" class="generalBtn">ADD NEW ADDRESS</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(function(){
    jQuery(".circle").on("click",function(){
       jQuery(".circle").each(function(){
            jQuery(this).removeClass('active');
            jQuery(this).find('input').removeAttr('checked');
        });
        
        jQuery(this).addClass('active');
        jQuery(this).find('input').attr('checked',true);
    });
    
    jQuery(".addressesHolder").on("click",".addressInfo",function(){
        var sa_id = jQuery(this).find('input').val();
        var siteurl = jQuery("input[name=siteurl]").val();
        var str_params = { sa_id : sa_id , action : "set_address_default" };
        var $this = jQuery(this); 
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: str_params,
            method: 'post',
            dataType: "json",
            success: function (response) { 
                if(response.is_error == 0)
                {
                   alert(response.error_msg);
                }
            }
        });
        
    });
    jQuery(".addressesHolder").on("click",".DeleteAddress",function(){
        var sa_id = jQuery(this).data("sa_id");
        var siteurl = jQuery("input[name=siteurl]").val();
        var str_params = { sa_id : sa_id , action : "delete_address" };
        var $this = jQuery(this);
        jQuery.ajax
        ({
            url: siteurl + "/ajax-requests",
            data: str_params,
            method: 'post',
            dataType: "json",
            success: function (response) { 
                if(response.is_error == 0)
                {
                    $this.parents('.eachAddressWrap').fadeOut('slow',function(){
                        $this.remove();
                    })
                }
            }
        });
        
    })
})
</script>
<?php get_footer();