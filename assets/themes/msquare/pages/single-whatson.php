<?php
/*
 *
 *  Template name: Single whatson page
 * 
 */

{// identification block
    $post_id = isset($_GET['post_id']) ? $_GET['post_id'] : 0;
    $post_info = get_post($post_id);
    $youtube_link    = get_field("event_youtube_link",$post_id);
   $event_date      = get_field("event_date",$post_id);
   $event_date = strtotime($event_date);
   $event_date = date("d M Y",$event_date);
   $post_image = get_the_post_thumbnail($post_id);
    

  /**  $query_images_args = array(
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'post_status'    => 'inherit',
        'posts_per_page' => - 1,
        'post_parent' => $post_id
    );

    $query_images = new WP_Query( $query_images_args );*/
   
   $query_images = get_post_gallery_images($post_id);
}

get_header(); ?>
<div class="singleArtistPage singleWhatsonPage">
    <div class="columnsWrap">
        <div class="leftSection">
            <div class="innerLeftSection">
                <a href="<?= esc_url(home_url('/whats-on')); ?>">
                <i class="fal fa-long-arrow-left"></i>
                <span>BACK TO EVENTS</span></a>
                <div class="infoWrapper">
                    <h1 class="mbsAnimate fadeRight"><?php echo $post_info->post_title ?></h1>
                    <h3 class="mbsAnimate fadeRight" data-delay="200"><?php echo $event_date; ?></h3>
                    <div class="content mbsAnimate fadeRight" data-delay="300"><?php echo strip_shortcodes($post_info->post_content); ?></div>
                    <a href="<?php echo $youtube_link; ?>" target="_blank" class="playBtn mbsAnimate fadeRight" data-delay="400"></a>
                </div>
            </div>
        </div>
        <div class="rightSection">
            <div class="topWhiteBg"></div>
            <div class="innerRightSection"> 
                <?php
                 foreach ($query_images as $key => $value) {
                     ?>
                 <div class="eachArtWrap">
                    <img src="<?= $value ?>" 
                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                </div>
                
                     <?php
                 }
                
                
                ?>
               
            </div>
        </div>
    </div>
</div>
<?php get_footer();