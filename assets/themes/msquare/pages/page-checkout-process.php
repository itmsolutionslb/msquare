<?php
/*
 *
 *  Template name: Checkout Process Page
 * 
 */
{
    
    error_reporting(0);
    global $woocommerce,$current_user;
    $items = $woocommerce->cart->get_cart();
    $xml_request = "";
    
    $gift_notes = isset($_POST["gift_notes"]) ? $_POST["gift_notes"] : "";
    $couple_name = isset($_POST["couple_name"]) ? $_POST["couple_name"] : "";
    $couple_notes = isset($_POST["couple_notes"]) ? $_POST["couple_notes"] : "";
    $msg_type = isset($_POST["msg_type"]) ? $_POST["msg_type"] : 1;
    
  
    $user_address   = isset($_POST['user_address_value']) ? $_POST['user_address_value'] : -1;
    $siteurl        = get_site_url();
    $customer_id    = get_current_user_id();
    $woocommerce_areeba_settings    = get_option("woocommerce_areeba_settings");
    $link_create_checkout_session   = $woocommerce_areeba_settings["link_create_checkout_session"];
    $merchant_id                    = $woocommerce_areeba_settings["merchant_id"];
    
    $link_create_checkout_session   = str_replace("%YOUR_MERCHANT_ID%", $merchant_id, $link_create_checkout_session);
    $list_addresses                 = get_user_meta($customer_id , "shipping_address");
    
     
   
    $list_addresses_array = json_decode($list_addresses[0]);
    
    $address_info = $list_addresses_array[ $user_address ];
     $email         =  $current_user->data->user_login;
     $user_id = get_current_user_id();
     
    
    $firstname  = $address_info->firstname;
    $lastname   = $address_info->lastname;
    $address    = $address_info->address;
    $country    = $address_info->country;
    $city       = $address_info->city;
    $state      = $address_info->state;
    $zip        = $address_info->zip;
    $phone      = $address_info->phone;
   
    update_user_meta( $user_id, 'billing_first_name', $first_name );
    update_user_meta( $user_id, 'billing_last_name', $last_name );
    update_user_meta( $user_id, 'billing_company', "" );
    update_user_meta( $user_id, 'billing_email', $email );
    update_user_meta( $user_id, 'billing_phone', $phone );
    update_user_meta( $user_id, 'billing_address_1', $address_1 );
    update_user_meta( $user_id, 'billing_address_2', $address_2 );
    update_user_meta( $user_id, 'billing_city', $city );
    update_user_meta( $user_id, 'billing_state', $state );
    update_user_meta( $user_id, 'billing_zip', $zip );
    update_user_meta( $user_id, 'billing_postcode', $postcode );
    update_user_meta( $user_id, 'billing_country', $country );
    
    // create order 
      $address = array(
      'first_name' => $firstname,
      'last_name'  => $lastname,
      'company'    => '',
      'email'      => $email,
      'phone'      => $phone,
      'address_1'  => $address,
      'address_2'  => '',
      'city'       => $city,
      'state'      => $state,
      'postcode'   => $zip,
      'country'    => $country
  );

  // Now we create the order
  $order = wc_create_order(array('customer_id'=>$customer_id));
        $order->set_address( $address, 'billing' );
        $order->set_address( $address, 'shipping' );
  // add products to order
       
        
  foreach ( $items as $key => $item_info ) {
        $order->add_product( wc_get_product( $item_info['product_id'] ), $item_info['quantity'] );
        
        $product = wc_get_product( $item_info['product_id'] );
        
        $sku            = $product->get_sku();
        $regular_price  = $product->get_regular_price();
        
        $xml_request .= "<Sales> 
	<SystemOnlineId>" . $item_info['product_id'] . "</SystemOnlineId> 
	<CustomerName>" .   $current_user->display_name . "</CustomerName> 
	<CustomerPass>PP</CustomerPass> 
	<OpDate>" . date("d/m/Y") . " " . date("H:i:s ") . "</OpDate> 
	<ItemCode>" . $sku . "</ItemCode> 
	<Q>" . $item_info['quantity']  . "</Q> 
	<Amount>" . $regular_price . "</Amount> 
	<PaymentMode>Credit Card</PaymentMode> <Op>0</Op> <Doc>0</Doc> </Sales>";
        
  }
  
  
      $order->update_meta_data('sender_id', $sender_id);  
        $order->update_meta_data('msg_type', $msg_type); 
        
        $sender_id = get_current_user_id(); 
        $couple_id = 0;
        $couple_email = "";
        if(strlen($couple_name) > 0)
        {
            $args = array(
                'meta_query'=>
                 array(
                    array(
                        'relation' => 'AND',
                    array(
                        'key'     => 'couple_name',
                            'value'   => $couple_name, 
                        'compare' => "="
                    ),
                  )
               )
            );

              $couple_info = get_users( $args );

              $couple_id = $couple_info[0]->ID; 
              $couple_email = $couple_info[0]->user_login; 
        }
        
        
        if($couple_id > 0)
        {
           $order->update_meta_data('couple_id', $couple_id); 
            $order->update_meta_data('couple_name', $couple_name);
            $order->update_meta_data('is_couple', 1);
            $order->update_meta_data('couple_notes', $couple_notes);
        }
       
        $order->update_meta_data('gift_notes', $gift_notes);
        $order->calculate_totals(); 
 

  $order_id = $order->id; 
    $total_order = $order->get_total();
    $currency_code = $order->get_currency();
    $currency_symbol = get_woocommerce_currency_symbol( $currency_code );
    $data = array(
        "apiOperation" => "CREATE_CHECKOUT_SESSION",
        "interaction" => array(
            "operation" => "PURCHASE"
        ),
        "order" => array (
            "currency" => $currency_code,
            "id" => $order_id ,
            "amount" => $total_order
        )
    );
     
 
    // save order to curl info
    
    $order_url  = "http://h-aapi.reconva.com:523/WsMara/api/Mara";
    $api_key    = "qpalzm@753lkjhgfdsa..";
    
    $xml_request = "<?xml version='1.0' encoding='UTF-8'?><string xmlns='http://tempuri.org/'><DsSales xmlns='http://tempuri.org/DsSales.xsd'>" . $xml_request; 
    
    
    $dataArray = array(
        "salesXML" => $xml_request,
        "api_Key" => $api_key
    );
    $ch = curl_init();
    $data = http_build_query($dataArray);
    $getUrl = $api_key;
    
 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, $getUrl);
    curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        
    $response = curl_exec($ch); 
    curl_close($ch);
  // The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
    
     
    // curl to payment
    $curl = curl_init(); 
    curl_setopt($curl, CURLOPT_URL, $link_create_checkout_session);
    // Optional Authentication:  
    $password = "c6230092f7fd36fa450de7256f512f54";
    $access = "merchant.".$merchant_id . ":" . $password;  
    curl_setopt($curl, CURLOPT_USERPWD, $access);
    curl_setopt( $curl, CURLOPT_POSTFIELDS,$data );
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
   $error_data =  curl_error ( $curl ); 
    $result_array = json_decode($result);
  
    $result_status = $result_array->result;
    if($result_status == "ERROR")
    {
         $order->update_status( 'cancelled' );
    }
    else 
    {
        $session = $result_array->session->id; 
    } 
    curl_close($curl);
    
    $data = array(
        "gift_notes" => $gift_notes,
        "couple_name" => $couple_name,
        "couple_notes" => $couple_notes
    );
    
    // from couple name get couple user id
    // get sender id
    // check if the order is sending as wedding List 
    
    
    if(strlen($couple_name) > 0)
    {
       /** $couple_info = get_users( array(
            'meta_query' => array(
                array(
                    'key'     => 'couple_name',
                    'value'   => gen_sql_like( false, $couple_name ), 
                )
            ),
        ) );*/
        

        
        
        $sender_info = get_userdata($sender_id); 
        $sender_email = $sender_info->data->user_login;
        $display_name = $sender_info->data->display_name;
        $order_content = "";
        foreach ( $items as $key => $item_info ) {
                $ID =$item_info['product_id'];
                $product = wc_get_product( $item_info['product_id'] );
                $product_title = $product->get_title();
                $product_description = $product->get_description();
                $product_width   = $product->get_width();
                $product_height  = $product->get_height();
                $product_length  = $product->get_length();
                $regular_price  = $product->get_regular_price();
                $sales_price    = $product->get_sale_price();

                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
                $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";

                $artist_name = $product->get_attribute( 'pa_artist-name' );

                $category_slug = woocommerceCategorySlug( $ID ); 
                $is_variable = $product->is_type( 'variable' ) ? 1 : 0;
            
            $order_content .= " 
                            <tr>
                                <td>
                                     <img src='" . $product_image_url . "' alt='Msquare " . $product_title . "' title='Msquare " . $product_title . "' />
                                </td>
                                <td>
                                  <h3 class='productName'>" .$product_title . "</h3> 
                                    <h4 class='artistName'>" . $artist_name . "</h4>
                                    <h5 class='color moreInfo'>Color: blue</h5>
                                    <h5 class='dimensions moreInfo'>Dimensions: W: " . $product_width ."x " . $product_length . "CM -  H: " . $product_height . "x" . $product_length . "CM </h5>
                                    <?php } ?>
                                </td>
                                <td>" . $regular_price ."</td>
                            </tr>
            ";
        }
        
        
        // send email to couple that he received a new gift
        $email_content = "";
        ob_start();
        require get_template_directory() . '/inc/emails/received-gift.php';
        $email_content = ob_get_contents();
        
        ob_end_clean();

        
        $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
        $siteurl = get_site_url();
        
        $subject = "MSQUARE GALLERY - WEDDING GIFT";
         $logo_url = get_template_directory_uri() . "/assets/images/logo.png";
        $image_url = get_template_directory_uri() . "/assets/images/emails/T3.png";
        $fbicon_url = get_template_directory_uri() . "/assets/images/emails/fbicon.png";
        $instaicon_url = get_template_directory_uri() . "/assets/images/emails/instaicon.png";
        $youtubeicon_url = get_template_directory_uri() . "/assets/images/emails/youtubeicon.png";
        $contactus_url = get_site_url() . "/contact-us";

        $email_content = str_replace("%EMAIL_LOGO%", $logo_url, $email_content); 
        $email_content = str_replace("%ORDER_DETAILS%", $order_content, $email_content); 
        $email_content = str_replace("%COUPLE_NAME%", $couple_name, $email_content); 
        $email_content = str_replace("%SENDER_NAME%", $display_name, $email_content); 
        $email_content = str_replace("%FACEBOOK_ICON%", $fbicon_url, $email_content);  
        $email_content = str_replace("%FACEBOOK_URL%", "https://www.facebook.com/M-Square-Gallery-1212257732163727", $email_content);  
        $email_content = str_replace("%INSTAGRAM_ICON%", $instaicon_url, $email_content); 
        $email_content = str_replace("%INSTAGRAM_URL%", "https://www.instagram.com/msquare_gallery/", $email_content); 
        $email_content = str_replace("%YOUTUBE_ICON%", $youtubeicon_url, $email_content); 
        $email_content = str_replace("%YOUTUBE_URL%", "#", $email_content); 
        $email_content = str_replace("%CONTACTUS_URL%", $contactus_url, $email_content);
        
         $mail = new PHPMailer(true);
        
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;                           // Enable SMTP authentication
            $mail->Username = 'no-reply@msquaregallery.com';                 // SMTP username
            $mail->Password = '70901010';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;   
            $mail->IsHTML(true);
            $mail->setFrom('no-reply@msquaregallery.com', 'Msquare Gallery');
            $mail->addAddress($couple_email,$full_name);
            $mail->Subject  = $subject;
            $mail->Body     = $email_content;
            $mail->send();
        
        
    }
    
    
   
    
     
}
?>
<html>
    <head>
        <script src="https://epayment.areeba.com/checkout/version/55/checkout.js"
                data-error="errorCallback"
                data-cancel="cancelCallback"
        data-complete="<?= $siteurl ?>/receipt-page?order_id=<?= $order_id ?>">
            </script>

        <script type="text/javascript">
            function errorCallback(error) {
                  console.log(JSON.stringify(error));
            }
            function cancelCallback() {
                  console.log('Payment cancelled');
            }
            
            Checkout.configure({
                merchant:'<?= $merchant_id ?>',
                order: {
                    amount: <?= $total_order ?>,
                    currency: '<?= $currency_code ?>',
                    description: 'Order #<?= $order_id ?>',
                   id: '<?= $order_id ?>'
                    },
                    session: {
                      id: '<?= $session ?>'
                    },
                interaction: {
                  operation: 'PURCHASE',
                    merchant: {
                        name: 'MSQUARE GALLERY',
                        address: {
                            line1: 'Rome street-Sanayeh',
                            line2: 'Beirut, Lebanon'     
                        }    
                    }
                }
            });
            Checkout.showPaymentPage();
        </script>
    </head>
    <body>
        <div style="width:100%" align="center" align="center">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/msquare-loader.gif" />
        </div>
    </body>
</html>