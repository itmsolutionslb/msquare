<?php
/*
 *
 *  Template name: Register
 * 
 */

{
    $register_image_profile = get_field("register_image_profile");
    $siteurl = get_site_url();
}

get_header(); 
?> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/src/javascript/formvalidation/dist/css/formValidation.min.css">
<div class="registerPage formsPage paddingTop">
<input type="hidden" name="siteurl" value="<?php echo $siteurl ?>" />
    <div class="innerPage">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <div style="display: none" class="innerLeftSection MsgRegistration">
                        <h2>Thank you for Joining<br/>Msquare Gallery</h2><br/>
                        <span>Enjoy our artworks and start shopping</span>
                    </div>
                    <div class="innerLeftSection FrmRegistration">
                        <h1 class="titleFont">Create an account</h1><br/>
                        <span class="ErrorMsg"></span>
                        <form name="form_register" id="FORM_REGISTER" method="post">
                            <input type="hidden" name="action" value="registration" />
                             <div class="fieldWrapper cf">
                                <div class="fl w-100">
                                    <label class="fl w-100 pa2">Full Name</label>
                                    <div class="fl w-100">
                                        <input type="text" name="fullname" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="MSQ_FULLNAME" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="fieldWrapper cf">
                                <div class="fl w-100">
                                    <label class="fl w-100 pa2">Email</label>
                                    <div class="fl w-100">
                                        <input type="email" name="msqemail" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="MSQ_EMAIL" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="fieldWrapper cf">
                                <div class="fl w-100">
                                    <label class="fl w-100 pa2">Password</label>
                                    <div class="fl w-100">
                                        <input type="password" name="msqpassword" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="MSQ_PASSWORD" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="fieldWrapper cf">
                                <div class="fl w-100">
                                    <label class="fl w-100 pa2">Confirm Password</label>
                                    <div class="fl w-100">
                                        <input type="password" name="msqconfpassword" class="input-reset ba b--black-20 pa2 mb2 db w-100" required id="MSQ_CONF_PASSWORD" value="" />
                                    </div>
                                </div>
                            </div>
                             <div class="rememberForgotWrap">
                                <div class="rememberWrapper">
                                    <input type="checkbox" class="square" name="agree" value="1"  />
                                    <label>I agree to <a href="<?= esc_url(home_url( '/terms' )); ?>">terms & conditions</a></label>
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/msquare-loader.gif" style="width:63px;visibility: hidden" class="ImgLoader" />
                                </div>
                            </div>
                            <div class="fieldWrapper submitWrapper">
                                <button  type="submit" value="REGISTER" name="btn_register" id="BTN_REGISTER" class="triggerBtn">REGISTER</button>
                                <a href="<?= esc_url(home_url( '/login' )); ?>" class="triggerBtn">LOG IN</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 rightSection">
                    <img src="<?= $register_image_profile["url"]; ?>"
                        alt="Msquare Gallery Registration" title="Msquare Gallery Registration" />
                </div>
            </div> 
        </div>
    </div>
</div>
<script
  src="https://code.jquery.com/jquery-3.5.1.js"
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-shim.min.js"></script>    
<script src="<?php echo get_template_directory_uri(); ?>/src/javascript/formvalidation/dist/js/FormValidation.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/src/javascript/formvalidation/dist/js/plugins/Tachyons.min.js"></script>
<script>
 // Generate a simple captcha
    function _handleSignUpForm(){
        const form = document.getElementById('FORM_REGISTER');
    FormValidation.formValidation(form, {
        fields: {
            fullname: {
                validators: {
                    notEmpty: {
                        message: 'The full name is required'
                    }
                }
            },
            msqemail: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            msqpassword: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    },
                    different: {
                        message: 'The password cannot be the same as Email',
                        compare: function() {
                            return form.querySelector('[name=msqemail]').value;
                        }
                    }
                }
            },
            agree: {
                validators: {
                    notEmpty: {
                        message: 'You must agree with the terms and conditions'
                    }
                }
            }
        },
        plugins: {
            trigger: new FormValidation.plugins.Trigger(),
            tachyons: new FormValidation.plugins.Tachyons(),
            submitButton: new FormValidation.plugins.SubmitButton(),
            icon: new FormValidation.plugins.Icon({
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }),
        }
    }); 
    }

    $(document).ready(function() {
        _handleSignUpForm();
    });
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/sweetalert2/src/sweetalert2.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/users.js" ></script>
<?php get_footer();