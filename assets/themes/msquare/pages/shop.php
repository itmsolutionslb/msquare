<?php
/*
 *
 *  Template name: Shop
 * 
 */
{
    $taxonomy     = 'product_cat';
    $orderby      = 'name';  
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no  
    $title        = '';  
    $empty        = 0;
    $cat_name        = isset($_GET['cat_name']) ? $_GET['cat_name'] : '';

    $args = array(
           'taxonomy'     => $taxonomy,
           'orderby'      => $orderby,
           'show_count'   => $show_count,
           'pad_counts'   => $pad_counts,
           'hierarchical' => $hierarchical,
           'title_li'     => $title,
           'hide_empty'   => $empty
    );
   $all_categories = get_categories( $args );
   
   
    $taxonomy     = 'portfolio_category';
    $orderby      = 'name';  
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no  
    $title        = '';  
    $empty        = 0;

    $args = array(
           'taxonomy'     => $taxonomy,
           'orderby'      => $orderby,
           'show_count'   => $show_count,
           'pad_counts'   => $pad_counts,
           'hierarchical' => $hierarchical,
           'title_li'     => $title,
           'hide_empty'   => $empty
    );
   $artist_categories = get_categories( $args );
    $siteurl = get_site_url();
    
    
    $lst_style = get_terms(array(
        'taxonomy' => 'pa_style',
        'hide_empty' => false
    ));
    
     $lst_artist_name = get_terms(array(
        'taxonomy' => 'pa_artist-name',
        'hide_empty' => false
    )); 
    
}

get_header(); ?>
<div class="shopPage withPageIdentifier">
    <span id="hidden_fields">
        <input type="hidden" name="page_number" value="1" />
        <input type="hidden" name="category_id" value="-1" />
        <input type="hidden" name="cat_name" value="<?= $cat_name ?>" />
        <input type="hidden" name="siteurl" value="<?= $siteurl ?>" />
    </span>
    <div class="pageIdentifier">
        <h1>The gallery</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 leftSection">
                    <div class="innerLeftSection">
                        <h3 class="sectionTitle">SHOP BY</h3>
                        <a href="#" class="closeFilterPopup"><i class="fal fa-times"></i></a>
                        <div class="eachFilterHolder">
                            <div class="filterHolderHead">
                                <h4>Category</h4>
                                <span><i class="fal fa-plus"></i></span>
                            </div>
                            <div class="filterHolderBody" data-simplebar data-simplebar-auto-hide="false">
                                <ul>
                                   
                                    
                                    <li>
                                        <a id="all_category" href="#">
                                            <span class="circle"></span>
                                            <span class="label">All Items</span>
                                        </a>
                                    </li>
                                     <?php foreach ($all_categories as $key => $category_info) {  ?>
                                        <?php 
                                            if( $category_info->slug == "uncategorized" || $category_info->slug == "cash-gift" )
                                                continue;
                                        ?>
                                         <li>
                                                <a  class="LnkCategoryItem" id="CLINK_<?php echo $category_info->term_id ?>" data-id="<?php echo $category_info->term_id ?>" href="#">
                                                    <input style="display:none" type="checkbox" class="ckCategories" id="CK_CATEGORIES_<?php echo $category_info->term_id ?>"  name="ck_category[]" value="<?php echo $category_info->term_id ?>" />
                                                    <span class="circle"></span>
                                                    <span class="label"><?php echo $category_info->cat_name ?></span>
                                                </a>
                                            </li> 
                                    <?php } ?> 
                                    <li>
                                        <a href="#">
                                            <span class="square saleChecker"><i class="far fa-check"></i></span>
                                            <span class="label">Sale</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="eachFilterHolder">
                            <div class="filterHolderHead">
                                <h4>Style</h4>
                                <span><i class="fal fa-plus"></i></span>
                            </div>
                            <div class="filterHolderBody" data-simplebar data-simplebar-auto-hide="false">
                                <ul>
                                    <?php foreach ($lst_style as $key => $style_info) { ?>
                                         <li>
                                             <a class="LnkStyleItem" data-id="<?php echo $style_info->term_id; ?>" href="#">
                                            <input style="display:none" type="checkbox" class="ckStyle" id="CK_STYLE_<?php echo $style_info->term_id ?>"  name="ck_style[]" value="<?php echo $style_info->term_id ?>" />
                                            <span class="circle"></span>
                                            <span class="label"><?= $style_info->name ?></span>
                                        </a>
                                    </li> 
                                    <?php } ?>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="eachFilterHolder">
                            <div class="filterHolderHead">
                                <h4>Artists</h4>
                                <span><i class="fal fa-plus"></i></span>
                            </div>
                            <div class="filterHolderBody" data-simplebar data-simplebar-auto-hide="false">
                                <ul>
                                    <?php foreach ($lst_artist_name as $key => $artist_info) {  ?>
                                    <li>
                                        <a class="LnkArtistName" data-id="<?php echo $artist_info->term_id; ?>" href="#">
                                            <input style="display:none" type="checkbox" class="ckArtist" id="CK_ARTIST_<?php echo $style_info->term_id ?>"  name="ck_artist[]" value="<?php echo $style_info->term_id ?>" />
                                            <span class="circle"></span>
                                            <span class="label"><?php echo $artist_info->name; ?></span>
                                        </a>
                                    </li>     
                                    <?php  } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="eachFilterHolder">
                            <div class="filterHolderHead">
                                <h4>Price</h4>
                                <span><i class="fal fa-plus"></i></span>
                            </div>
                            <div class="filterHolderBody" data-simplebar data-simplebar-auto-hide="false">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <span class="circle"></span>
                                            <span class="label">Any Price</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="LnkPrice">
                                            <span class="circle"></span>
                                            <input style="display:none" type="checkbox" class="ckPrice" name="ck_price[]" value="1900" />
                                            <span class="label">Under <?= wc_price(1900,array('decimals' => 0)); ?></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="LnkPrice">
                                            <span class="circle"></span>
                                            <input style="display:none" type="checkbox" class="ckPrice" name="ck_price[]" value="1900_4000" />
                                            <span class="label"><?= wc_price(1900,array('decimals' => 0)); ?> - <?= wc_price(4000,array('decimals' => 0)); ?></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="LnkPrice">
                                            <span class="circle"></span>
                                            <input style="display:none" type="checkbox" class="ckPrice" name="ck_price[]" value="4000_up" />
                                            <span class="label"><?= wc_price(4000,array('decimals' => 0)); ?> And Up</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="LnkPrice">
                                            <span class="square"></span>
                                            <span class="label">On Sale</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="eachFilterHolder">
                            <div class="filterHolderHead">
                                <h4>Dimensions</h4>
                                <span><i class="fal fa-plus"></i></span>
                            </div>
                            <div class="filterHolderBody" data-simplebar data-simplebar-auto-hide="false">
                                <ul class="inlined">
                                    <li>
                                        <a href="#">
                                            <span class="circle"></span>
                                            <span class="label">IN</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="circle"></span>
                                            <span class="label">CM</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="dimensionsSelector">
                                    <h4>HEIGHT</h4>
                                    <div class="selectorHolder heightSelector">
                                        <input type="text" name="height_from" value="" />
                                        <span>TO</span>
                                        <input type="text"  name="height_to" value="" />
                                        <a href="#">GO</a>
                                    </div>
                                    <h4>WIDTH</h4>
                                    <div class="selectorHolder widthSelector">
                                        <input  name="width_from" type="text" value=""  />
                                        <span>TO</span>
                                        <input  name="width_to" type="text" value=""  />
                                        <a href="#">GO</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btnsHolder">
                            <a href="#" class="triggerBtn">DONE</a>
                            <a href="#" class="triggerBtn closeFilterPopup">CLEAR</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sortingWrapper">
                        <h3>Learn more about our <a href="#" class="openGeneralPopup" data-usage="giftpopup">Corporate gifts</a></h3>
                        <select name='order_by'>
                            <option value="Sort by">Sort by</option>
                            <option value="Newest">Newest</option>
                            <option value="Oldest">Oldest</option>
                            <option value="A-to-Z">A-to-Z</option>
                            <option value="Z-to-A">Z-to-A</option>
                        </select>
                        <a href="#" class="openFilterPopup">Filter</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row LstProducts"></div>
                    <div class="row">
                        <div class="col-md-12" align="left">
                            <ul id="ProductsPagination" class="pagination-sm"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="generalPopup formsPopup" data-usage="giftpopup">
        <div class="innerGeneralPopup">
            <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
            <h1 class="popupTitle">Corporate gifts</h1>
            <div class="content">
                M. SQUARE GALLERY, dedicates a special area for prestigious and luxurious home accessories and gifts for
                all occasions. We also are specialized in the end of year corporate gifts where we hold a large
                collection of items at all budgets. 
                <br>
                Our delicious chocolate can be added to any selected gift and we
                always seek to satisfy and please our corporate clients with our beautiful and refined gifts
                presentation.
                <span>With M. SQUARE GALLERY its always about perfection.</span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/twbs-pagination/jquery.twbsPagination.min.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/shop.js" ></script>
<?php get_footer();