<?php
/*
 *
 *  Template name: Artist page
 * 
 */

{
    $id = isset($_GET['id']) ? $_GET['id'] : 0;
    
    $post_info = get_post($id);
     
    $thumb_id = get_post_thumbnail_id($id);
    $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
    
    $args = array(
        'post_type'=> 'services',
        'areas'    => 'painting',
        'order'    => 'ASC'
    );              

    $the_query = new WP_Query( $args );
    
    
    
   $query_images = get_post_gallery_images($id);
    
}

get_header(); ?>
<style>
    .singleArtistPage .columnsWrap .leftSection .innerLeftSection {
    padding: 130px 0 0 52px;
}
</style>
<div class="singleArtistPage">
    <div class="columnsWrap">
        <div class="leftSection">
            <div class="innerLeftSection">
                <a href="<?= esc_url(home_url('/artists')); ?>">
                <i class="fal fa-long-arrow-left"></i>
                <span class="mbsAnimate fadeRight">BACK TO ARTISTS</span></a>
                <div class="infoWrapper">
                    <img class="mbsAnimate fadeRight" data-delay="200" src="<?php echo $thumb_url[0]; ?>"
                        alt="<?php echo $post_info->post_title; ?> image" title="<?php echo $post_info->post_title; ?> image" />
                    <h1 class="mbsAnimate fadeRight" data-delay="300"><?php echo $post_info->post_title; ?></h1>
                    <div class="content mbsAnimate fadeRight" data-delay="400"><?php echo strip_shortcodes($post_info->post_content); ?></div>
                </div>
            </div>
        </div>
        <div class="rightSection">
            <div class="topWhiteBg"></div>
            <div class="innerRightSection"> 
                <?php
                 foreach ($query_images as $key => $value) {
                     ?>
                        <div class="eachArtWrap">
                            <img src="<?= $value; ?>"
                                alt="Msquare Gallery Artist Product" title="Msquare Gallery Artist Product" />
                                <h3></h3>
                        </div>
                     <?php
                 }
                ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer();