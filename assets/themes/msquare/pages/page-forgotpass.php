<?php
/*
 *
 *  Template name: Forgot pass
 * 
 */

{
    $siteurl = get_site_url();
}

get_header(); ?>
<input type="hidden" name="siteurl" value="<?= $siteurl ?>" />
<div class="forgotPassPage formsPage paddingTop">
    <div class="innerPage">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <div class="innerLeftSection ForgotForm">
                        <h1 class="titleFont">Forgot your password?</h1>
                        <h4>Please enter your username or email address. You will receive a link to create a new password via email.</h4>
                        <form name="frm_forgot_password" id="FRM_FORGOT_PASSWORD" method="post">
                            <input type="hidden" name="action" value="send_reset_password" />
                            <span class="ErrorMsg"></span>
                            <div class="fieldWrapper">
                                <label>Email</label>
                                <input type="text" name="user_email" value="" id="USER_EMAIL" />
                            </div>
                            <br/>
                            <div class="fieldWrapper submitWrapper">
                                <input type="submit" value="RESET PASSWORD" class="triggerBtn" name="btn_forgot_password" id="BTN_FORGOT_PASSWORD" />&nbsp;&nbsp;<img src="<?= get_template_directory_uri(); ?>/assets/images/msquare-loader.gif" style="width:63px;visibility: hidden" class="ImgLoader" />
                            </div>
                        </form>
                    </div>
                    <div class="innerLeftSection ForgotMessage" style="display:none">
                         <form name="frm_resend_email" id="FRM_RESEND_EMAIL" method="post">
                            <input type="hidden" name="action" value="resend_email" />
                            <input type="hidden" name="email_address" value="resend_email" />
                        <h3>Reset password link sent</h3><br/>
                        <span>
                            We’ve sent you an email to reset your password<br/><br/>
                            To create your new password, click the link in the email<br/>
                            and enter a new one - easy<br/><br/>
                            Didn’t receive the email? Check your junk email, any other<br/>
                            email addresses linked to your Msquare Account account, or click<br/>
                            below to resend email<br/><br/>
                        </span>
                        <div class="fieldWrapper submitWrapper">
                           
                            <button type="submit" class="triggerBtn" name="btn_resend_email" id="BTN_RESEND_EMAIL" >RESEND EMAIL</button>
                            <a href="<?= esc_url(home_url( '/login' )); ?>" class="triggerBtn">LOG IN</a>
                           
                        </div>
                         </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 rightSection">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img19.png"
                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/users.js" ></script>
<?php get_footer();