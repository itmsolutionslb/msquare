<?php
/*
 *
 *  Template name: My Bag
 * 
 */

{
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    $siteurl = get_site_url();
}

get_header(); 

?>
<input type="hidden" name="siteurl" value="<?php echo $siteurl ?>" />
<div class="withPageIdentifier myBagPage">
    <div class="pageIdentifier">
        <h1>My Bag</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row EmptyBagPage" style="display:none">
                <div class="col-md-12" align="center">
                    <span>Your bag is empty!</span><br/><br/>
                     <a href="<?= $siteurl ?>/shop" class="generalBtn">START SHOPPING</a>
                </div>
            </div>
            <div class="row MyBagPage">
                <?php if( count($items) > 0 ){ ?>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <?php foreach ($items as $key => $item_info) { 
                        
                        $ID = $item_info['data']->get_id();
                        $product =  wc_get_product( $ID ); 
                        $product_title = $product->get_title();
                        $product_description = $product->get_description();
                        $product_width   = $product->get_width();
                        $product_height  = $product->get_height();
                        $product_length  = $product->get_length();
                        $regular_price  = $product->get_regular_price();
                        $sales_price    = $product->get_sale_price();
                        
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $ID ), 'single-post-thumbnail' );
    $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";

                        $artist_name = $product->get_attribute( 'pa_artist-name' );
                       
                        $category_slug = woocommerceCategorySlug( $ID ); 
                        $is_variable = $product->is_type( 'variable' ) ? 1 : 0;
                        
                        ?>
                        <div class="horizontalPoductWrap">
                            <div class="imageHolder">
                                <img src="<?= $product_image_url; ?>"
                                    alt="Msquare Gallery logo" title="Msquare Gallery" />
                            </div>
                            <div class="descHolder">
                                <h3 class="productName"><?= $product_title; ?></h3>
                                <?php if($category_slug != "cash-gift"){ ?>
                                <h4 class="artistName"><?= $artist_name ?></h4>
                                <!--<h5 class="color moreInfo">Color: blue</h5>-->
                                <h5 class="dimensions moreInfo">Dimensions: W: <?= $product_width ?> x <?= $product_length ?>CM -  H: <?= $product_height ?> x <?= $product_length ?>CM </h5>
                                <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $item_info['data']->get_id() ?>" class="viewItem">View Item</a>
                                <?php } ?>
                            </div>
                            <div class="priceHolder">
                                <h3><?= $regular_price ?><?= get_woocommerce_currency_symbol() ?></h3>
                                <a href="#" class="removeBtn removeProduct" data-product_id="<?= $ID ?>" >remove</a>
                            </div>
                        </div> 
                    <?php } ?>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <div class="innerRightSection">
                        <h3 class="sectionTitle withBackSquare">OVERVIEW</h3>
                        <div class="separatedInnerContent subtotal">
                            <h4>Subtotal</h4>
                            <h4><?= $woocommerce->cart->get_cart_subtotal() ?></h4>
                        </div>
                        <div class="separatedInnerContent shipping">
                            <h4>Shipping</h4>
                            <h4>To be quoted</h4>
                        </div>
                        <div class="separatedInnerContent total bolded">
                            <h4>Total</h4>
                            <h4><?= $woocommerce->cart->get_cart_total() ?></h4>
                        </div>
                        <a href="<?php echo $siteurl ?>/my-checkout" class="generalBtn">PROCEED TO CHECKOUT</a>
                        <div class="additionalInfo">
                            <h3 class="sectionTitle">SHIPPING INFO</h3>
                            <div class="content">
                                Shipping: Standard To Anywhere In The World, Arrives In 8-13 Days.
                            </div>
                        </div>
                        <div class="additionalInfo">
                            <h3 class="sectionTitle">SECURE CHECKOUT</h3>
                            <div class="content">
                            Lorem Ipsum Is Simply Dummy Text Of The Printing And Typesetting Industry.
                            </div>
                        </div>
                    </div>
                </div>
                <?php }else{ ?>
                <div class="col-md-12" align="center">
                    <span>Your bag is empty!</span><br/><br/>
                     <a href="<?= $siteurl ?>/shop" class="generalBtn">START SHOPPING</a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/users.js" ></script>
<?php get_footer();