<?php
/*
 *
 *  Template name: Login
 * 
 */

{
    $login_image_profile = get_field("login_image_profile");
    $siteurl = get_site_url();
}

get_header(); ?>
<div class="loginPage formsPage paddingTop">
<input type="hidden" name="siteurl" value="<?php echo $siteurl ?>" />
<input type="hidden" name="link_url" value="<?= $_GET['link_url'] ?>" />
    <div class="innerPage">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 leftSection">     
                    <div class="innerLeftSection">
                        <h1 class="titleFont">Log in to your account</h1><br/>
                        <span class="ErrorMsg"></span>
                        <form name="form_login" id="FORM_LOGIN">
                            <input type="hidden" name="action" value="login" />
                            <div class="fieldWrapper">
                                <label>Email</label>
                                <input type="text"  name="msq_login" id="MSQ_LOGIN" value="" />
                            </div>
                            <div class="fieldWrapper">
                                <label>Password</label>
                                <input type="password"  name="msq_password" id="MSQ_PASSWORD" value="" />
                            </div>
                            <div class="rememberForgotWrap">
                                <div class="rememberWrapper">
                                    <span><input class="square" type="checkbox" name="msq_remember_me" id="MSQ_REMEMBER_ME" value="1" /></span>
                                    <label>Remember me</label>
                                </div>
                                <a href="<?= esc_url(home_url( '/forgot-password' )); ?>">Forgot password?</a>
                            </div>
                            <div class="fieldWrapper submitWrapper">
                                <button type="submit" class="triggerBtn" name="btn_login" id="BTN_LOGIN" >LOG IN</button>
                                <a href="<?= esc_url(home_url( '/register' )); ?>" class="triggerBtn">REGISTER</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 rightSection">
                    <img src="<?php echo $login_image_profile["url"]; ?>"
                        alt="Msquare Gallery Login" title="Msquare Gallery Login" />
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/users.js" ></script>
<?php get_footer();