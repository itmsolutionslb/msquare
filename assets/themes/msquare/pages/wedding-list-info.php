<?php
/*
 *
 *  Template name: Wedding list info
 * 
 */


{
    $customer_id = get_current_user_id();
    $siteurl = get_site_url();
    if($customer_id == 0)
    {
        $permalink = get_permalink();
        
        header("location:".$siteurl . "/login?link_url=" . urlencode($permalink));
        exit;
    }
    
    
    global $current_user,$woocommerce;
    wp_get_current_user();
    $display_name = $current_user->display_name;
    $name_array = explode(" ", $display_name);
    $full_name = $name_array[0];
    $count_wishlist = yith_wcwl_count_all_products();
    $siteurl = get_site_url();
    
    $user_id       = get_current_user_id();
    $couple_name   = get_user_meta($user_id , "couple_name");
    $couple_name = $couple_name[0];
 
        $args = array(
            "customer_id" => $user_id,
           "meta_query" => array(
               'key' => 'couple_name',
               'value' => $couple_name
           ) 
    );
    $received_gift = wc_get_orders($args); 
    $type = isset($_GET['type']) ? $_GET['type'] : "history";
}

get_header(); ?>
<div class="weddingListInfo myAccountPages withPageIdentifier">
    <input type="hidden" name="siteurl" value="<?php echo $siteurl ?>" />
    <input type="hidden" name="type" value="<?= $type ?>" />
    <div class="pageIdentifier">
        <h1>My Account</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <h2 class="userName">Hello, <?= $display_name ?></h2>
                    <div class="innerLeftSection">
                        <h3 class="pageName">WEDDING LIST <i class="far fa-plus"></i></h3>
                        <ul>
                            <li><a href="<?= esc_url(home_url('/my-orders')); ?>">MY ORDERS</a></li>
                            <li><a href="<?= esc_url(home_url('/account-details')); ?>">ACCOUNT DETAILS</a></li>
                            <li><a href="<?= esc_url(home_url('/addresses')); ?>">ADDRESSES</a></li>
                            <li><a href="<?= esc_url(home_url('/wishlist')); ?>">WISHLIST (<?= $count_wishlist ?>)</a></li>
                            <li class="active"><a href="#"><i class="fal fa-gift"></i>
                                    WEDDING LIST <i class="far fa-check"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <div class="innerRightSection">
                        <div class="sectionHeader">
                            <ul>
                                <li data-type="history" class="<?php echo ($type == "history") ? "active" : "" ?>"><a href="#">Gifts History</a></li>
                                <li style="<?php echo ( strlen($couple_name) == 0 ? "display:none" : "" ); ?>" data-type="received"  class="<?php echo ($type == "received_gift") ? "active" : "" ?>" ><a href="#">Received Gifts (<?= count($received_gift) ?>)</a></li>
                            </ul>
                            <a href="#" class="generalBtn"><i class="fas fa-print"></i> PRINT</a>
                        </div>
                        <div class="productsWrapper"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/wedding-list.js" ></script>
<script type="text/javascript">
jQuery(function(){
    weddinglist_module.DisplayListGifts();
})
</script>
<?php get_footer();