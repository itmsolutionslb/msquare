<?php
/*
 *
 *  Template name: About Us
 * 
 */

{
    $our_story_label    = get_field("our_story_label");
    $our_story_content  = get_field("our_story_content");
    
    
    $first_story_slide  = get_field("first_story_slide");
    $second_story_slide = get_field("second_story_slide");
    $third_story_slide = get_field("third_story_slide");
    $fourth_story_slide = get_field("fourth_story_slide");
    $fifth_story_slide = get_field("fifth_story_slide");
}

get_header(); ?>
<div class="aboutUsPage paddingTop">
    <div class="innerPage">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 leftSection">
                    <div class="innerLeftSection">
                        <div class="identifier">
                            <h1 class="titleFont mbsAnimate fadeRight"><?php echo $our_story_label; ?></h1>
                        </div>
                        <div class="content mbsAnimate fadeRight" data-delay="250">
                             <?php echo $our_story_content; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rightSection">
                    <div class="innerRightSection">
                        <div class="swiper-container aboutUsSlider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <img src="<?php echo $first_story_slide['url'] ?>"
                                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                                </div>
                                <div class="swiper-slide">
                                    <img src="<?php echo $second_story_slide['url'] ?>"
                                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                                </div>
                                <div class="swiper-slide">
                                    <img src="<?php echo $third_story_slide['url'] ?>"
                                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                                </div>
                                <div class="swiper-slide">
                                    <img src="<?php echo $fourth_story_slide['url'] ?>"
                                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                                </div>
                                <div class="swiper-slide">
                                    <img src="<?php echo $fifth_story_slide['url'] ?>"
                                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination aboutUsSliderPag"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();