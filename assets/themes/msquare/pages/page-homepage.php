<?php
/*
 *
 *  Template name: homepage
 * 
 */
  $site_url = get_option('siteurl');

  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );
 
 {
        $args = array(
         'post_type'      => 'product',
         'posts_per_page' => 4,
         'product_cat' => 'our-picks'
      );

    $lst_picks_products = new WP_Query( $args ); 
    
    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => 4,
        'product_cat' => 'promotion'
     );

    $lst_sales_products = new WP_Query( $args ); 
    
    $args = array(
        'post_type'      => 'product',
        'product_cat' => 'curated-weekly'
    );
    $lst_weekly_products = new WP_Query( $args );
    $total_weekly_products = count($lst_weekly_products->posts);
    $weekly_products = $lst_weekly_products->posts;
    $slide_blocks = $total_weekly_products % 4;
    $first_slide_cover  = get_field("first_slide_cover");
    $second_slide_cover  = get_field("second_slide_cover");
    $third_slide_cover  = get_field("third_slide_cover");
    $fourth_slide_cover  = get_field("fourth_slide_cover");
    
    
 }

 // get slider info
 {
    $first_slide_label  = get_field("first_slide_label");
    $first_slide_link   = get_field("first_slide_link");
    $first_slide_image  = get_field("first_slide_image");
    
    $second_slide_label  = get_field("second_slide_label");
    $second_slide_link   = get_field("second_slide_link");
    $second_slide_image  = get_field("second_slide_image");
    
    $third_slide_label  = get_field("third_slide_label");
    $third_slide_link   = get_field("third_slide_link");
    $third_slide_image  = get_field("third_slide_image");
    
    $fourth_slide_label  = get_field("fourth_slide_label");
    $fourth_slide_link   = get_field("fourth_slide_link");
    $fourth_slide_image  = get_field("fourth_slide_image");
    
    $fifth_slide_label  = get_field("fifth_slide_label");
    $fifth_slide_link   = get_field("fifth_slide_link");
    $fifth_slide_image  = get_field("fifth_slide_image");
    
    
 }
 
 {// weeding list section
    $wedding_list_label        = get_field("wedding_list_label");
    $wedding_list_brief        = get_field("wedding_list_brief");
    $wedding_list_image        = get_field("wedding_list_image");
 }
 
 { // whats on section
    $whatson_label        = get_field("section_title");
    $whatson_brief        = get_field("section_description");
    $whatson_image        = get_field("section_image");
 }
 
 
  { // coorporet gift section
    $corporate_title        = get_field("corporate_title");
    $corporate_brief        = get_field("corporate_brief");
    $corporate_image        = get_field("corporate_image");
 } 
 
 
$current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; 

$siteurl = get_site_url();


{
    $user_id       = get_current_user_id();
   
    $product_ids = get_user_meta($user_id , "wishlist_product_ids");
     if(is_array($product_ids) && count($product_ids) > 0)
        $product_ids = $product_ids[0];
     else
         $product_ids = array();
                                    
}


get_header(); ?>
<input type="hidden" name="siteurl" value="<?= $siteurl ?>" />
<!-- <?php //echo esc_url( add_query_arg( 'remove_from_wishlist', $item->get_product_id() ) ); ?>-->
<div class="homepage">
    <section class="topSlider">
        <div class="swiper-container topSwiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide"
                    style="background-image: url('<?php echo $first_slide_image["url"]; ?>')">
                    <div class="innerDescription">
                        <h1><?php echo $first_slide_label; ?></h1>
                        <a href="<?php echo $first_slide_link; ?>">LEARN MORE <i class="fal fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <?php if(strlen($second_slide_image["url"]) > 0){ ?>
                <div class="swiper-slide"
                    style="background-image: url('<?php echo $second_slide_image["url"]; ?>')">
                    <div class="innerDescription">
                        <?php if(strlen($second_slide_label) > 0){ ?>
                        <h1><?php echo $second_slide_label; ?></h1>
                        <?php } ?>
                        <?php if(strlen($second_slide_link) > 0 || $second_slide_link != "#"){ ?>
                        <a href="<?php echo $second_slide_link; ?>">LEARN MORE <i class="fal fa-long-arrow-right"></i></a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                
                <div class="swiper-slide"
                    style="background-image: url('<?php echo $third_slide_image["url"]; ?>')">
                    <div class="innerDescription">
                        <?php if(strlen($third_slide_label) > 0){ ?>
                        <h1><?php echo $third_slide_label; ?></h1>
                        <?php } ?>
                        <?php if(strlen($third_slide_link) > 0){  ?>
                        <a href="<?php echo $third_slide_link; ?>">LEARN MORE <i class="fal fa-long-arrow-right"></i></a>
                        <?php } ?>
                    </div>
                </div>
                
                
                <div class="swiper-slide"
                    style="background-image: url('<?php echo $fourth_slide_image["url"]; ?>">
                    <div class="innerDescription">
                         <?php if(strlen($fourth_slide_label) > 0){ ?>
                        <h1><?php echo $fourth_slide_label; ?></h1>
                        <?php } ?>
                        <?php if(strlen($fourth_slide_link) > 0){ ?>
                        <a href="<?php echo $fourth_slide_link; ?>">LEARN MORE <i class="fal fa-long-arrow-right"></i></a>
                         <?php } ?>
                    </div>
                </div>
               
                
                <div class="swiper-slide"
                    style="background-image: url('<?php echo $fifth_slide_image["url"]; ?>')">
                    <div class="innerDescription">
                        <?php if(strlen($fifth_slide_label) > 0){ ?>
                        <h1><?php echo $fifth_slide_label; ?></h1>
                         <?php } ?>
                        <?php if(strlen($fifth_slide_link) > 0){ ?>
                        <a href="<?php echo $fifth_slide_link; ?>">LEARN MORE <i class="fal fa-long-arrow-right"></i></a>
                        <?php } ?>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="topSwiperPag"></div>
        <div class="topWhitebg"></div>
    </section>
    <section class="curatedWeekly">
        <div class="container">
            <h2 class="sectionTitle titleFont mbsAnimate fadeUp">Curated Weekly</h2>
            <div class="sliderWrapper mbsAnimate fadeUp">
                <div class="swiper-container curatedWeeklySlider">
                    <div class="swiper-wrapper">
                        <?php 
                            for ($index = 0; $index < $slide_blocks ; $index++){
                                $image_url = "";
                                switch ($index) {
                                    case 0:
                                    {
                                        $image_url = $first_slide_cover['url'];
                                    }
                                    break;
                                    case 1:
                                    {
                                        $image_url = $second_slide_cover['url'];
                                    }
                                    break;
                                    case 2:
                                    {
                                        $image_url = $third_slide_cover['url'];
                                    }
                                    break;
                                    case 3:
                                    {
                                        $image_url = $fourth_slide_cover['url'];
                                    }
                                    break;
                                    default:
                                    {
                                        $image_url = $fourth_slide_cover['url'];
                                    }
                                    break;
                                }
                                    
                          ?>
                             <div class="swiper-slide">
                                    <div class="row">
                                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 leftSection">
                                            <img src="<?php echo $image_url; ?>"
                                                alt="Curated Weekly Msquare Gallery" title="Curated Weekly Msquare Gallery" />
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 rightSection">
                                            <?php 
                                                for ($j = $index * 4; $j < $index * 4 + 4; $j++) { 
                                                    
                                                    $post_info = $weekly_products[$j];
                                                    $product_id = $post_info->ID;
                                                    $product = wc_get_product( $product_id );
                                                    $regular_price  = $product->get_regular_price();
                                                    $sales_price    = $product->get_sale_price();
                                                    $product_price  = $product->get_price_html();
                                                    $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
                                                    $cat_id = (int)$term_list[0]; 
                                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_info->ID ), 'single-post-thumbnail' );
                                                    $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                                                    
                                                    $background_style = "";
                                                    $icon_class = "fal fa-heart";
                                                    if(in_array($product_id, $product_ids))
                                                    { 
                                                        $background_style  = "color:#FEC512";
                                                        $icon_class = "fas fa-heart";
                                                    } 
                                                    
                                             ?>
                                            <span>
                                                
                                            </span> 
                                            <span data-product_id="<?= $product_id ?>"  class="addToWishlist"><i style="<?= $background_style ?>" class="<?= $icon_class ?>"></i></span>
                                                <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>" class="productWrap">
                                                <div class="imageHolder">
                                                    <img src="<?= $product_image_url ?>"
                                                        alt="Msquare Gallery Product <?= $post_info->post_title ?>" title="Msquare Gallery Product <?= $post_info->post_title ?>" />
                                                </div>
                                                <div class="descHolder">
                                                    <h3><?= ucfirst( strtolower( $post_info->post_title) ) ?></h3>
                                                    <h4><?= $product_price ?></h4>
                                                    <?php if(strlen($sales_price) > 0 ){ ?>
                                                    <h4 class="sale-price"><?= $sales_price ?></h4>
                                                    <?php } ?>
                                                </div>
                                            </a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                             <?php } ?> 
                    </div>
                </div>
            </div>
            <div class="curatedWeeklySliderPag"></div>
        </div>
    </section>
    <section class="ourPicksSection productListSection">
        <div class="container">
            <h2 class="sectionTitle titleFont mbsAnimate fadeUp">Our Picks</h2>
            <div class="row">
                <?php 
                    if($lst_picks_products->post_count > 0)
                    {
                        $post_info = $lst_picks_products->posts;
                    if(count($post_info) <= 1)
                    {
                        $post_info = $post_info[0];
                        $product_id = $post_info->ID;
                        $product = wc_get_product( $product_id );
                        $regular_price  = $product->get_regular_price();
                        $sales_price    = $product->get_sale_price();
                        $product_price  = $product->get_price_html();
                        $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
                        $cat_id = (int)$term_list[0];

                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_info->ID ), 'single-post-thumbnail' );
                        $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                        
                                    
                        $background_style = "";
                        $icon_class = "fal fa-heart";
                        if(in_array($product_id, $product_ids))
                        { 
                            $background_style  = "color:#FEC512";
                            $icon_class = "fas fa-heart";
                        }
                        
                        
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 productWrap">
                            <span data-product_id="<?= $product_id ?>"  class="addToWishlist"><i  style="<?= $background_style ?>" class="<?= $icon_class ?>"></i></span>
                            <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>"
                                    class="<?= (strlen($sales_price) > 0 ) ? "sale" : "" ?>">
                                    <div class="imageHolder">
                                        <img src="<?= $product_image_url; ?>"
                                             alt="Msquare Gallery Product <?= $post_info->post_title ?>" title="Msquare Gallery Product <?= ucfirst($post_info->post_title) ?>" />
                                    </div>
                                    <div class="descHolder">
                                        <h3><?= ucfirst( strtolower( $post_info->post_title) ) ?></h3>
                                        <h4><?= $product_price ?></h4>
                                        <?php if(strlen($sales_price) > 0 ){ ?>
                                        <h4 class="sale-price"><?= $sales_price ?></h4>
                                        <?php } ?>
                                    </div>
                                </a>
                        </div>
                         
                        <?php 
                    }
                    else {
                        ?>
                        <?php foreach ($lst_picks_products->posts as $product_id => $post_info) { ?>
                            <?php  
                                $product_id = $post_info->ID;
                                $product = wc_get_product( $product_id );
                                $regular_price  = $product->get_regular_price();
                                $sales_price    = $product->get_sale_price();
                                $product_price  = $product->get_price_html();
                                $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
                                $cat_id = (int)$term_list[0];

                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_info->ID ), 'single-post-thumbnail' );
                                $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                                
                                // wishlist url
                                $wishlist_url = "";
                                $background_style = "";
                                $icon_class = "fal fa-heart";
                                if(in_array($product_id, $product_ids))
                                {
                                    $wishlist_url = esc_url( add_query_arg( 'remove_from_wishlist', $product_id ) );
                                     $background_style  = "color:#FEC512";
                                     $icon_class = "fas fa-heart";
                                }
                                else 
                                {
                                    $wishlist_url =  esc_url( add_query_arg( 'add_to_wishlist', $product_id, $siteurl ) );
                                }
                                
                            ?>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 productWrap">
                                    <span data-product_id="<?= $product_id ?>"  class="addToWishlist"><i  style="<?= $background_style ?>" class="<?= $icon_class ?>"></i></span>
                                    <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>"
                                       class="<?= (strlen($sales_price) > 0 ) ? "sale" : "" ?>">
                                       <div class="imageHolder">
                                           <img src="<?= $product_image_url; ?>"
                                               alt="Msquare Gallery Product <?= $post_info->post_title ?>" title="Msquare Gallery Product <?= ucfirst($post_info->post_title) ?>" />
                                       </div>
                                       <div class="descHolder">
                                           <h3><?= ucfirst( strtolower( $post_info->post_title ) ) ?></h3>
                                           <h4><?= $product_price ?></h4>
                                           <?php if(strlen($sales_price) > 0 ){ ?>
                                           <h4 class="sale-price"><?= $sales_price ?></h4>
                                           <?php } ?>
                                       </div>
                                   </a>
                                </div>
                            <?php } ?>
                        <?php 
                    }
                    }
                
                    
                
                ?>
                
               
            </div>
        </div>
    </section>
    <div class="container">
        <div class="seperator"></div>
    </div>
    <section class="ourPicksSection productListSection">
        <div class="container">
            <h2 class="sectionTitle titleFont mbsAnimate fadeUp">Sale</h2>
            <div class="row">
                <?php 
                 if($lst_sales_products->post_count > 0)
                 {
                     $post_info = $lst_sales_products->posts;
                    if(count($post_info) <= 1)
                    {
                        $post_info = $post_info[0];
                         $product_id = $post_info->ID;
                        $product = wc_get_product( $product_id );
                        $regular_price  = $product->get_regular_price();
                        $sales_price    = $product->get_sale_price();
                        $product_price  = $product->get_price_html();
                        $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
                        $cat_id = (int)$term_list[0];

                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_info->ID ), 'single-post-thumbnail' );
                        $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                        
                       $background_style = "";
                        $icon_class = "fal fa-heart";
                        if(in_array($product_id, $product_ids))
                        { 
                            $background_style  = "color:#FEC512";
                            $icon_class = "fas fa-heart";
                        }
                        
                        ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 productWrap">
                    <span data-product_id="<?= $product_id ?>"  class="addToWishlist"><i  style="<?= $background_style ?>" class="<?= $icon_class ?>"></i></span>
                    <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>"
                                class="<?= (strlen($sales_price) > 0 ) ? "sale" : "" ?>">
                                <div class="imageHolder">
                                    <img src="<?= $product_image_url; ?>"
                                        alt="Msquare Gallery Product <?= $post_info->post_title ?>" title="Msquare Gallery Product <?= $post_info->post_title ?>" />
                                </div>
                                <div class="descHolder">
                                    <h3><?= ucfirst( strtolower( strtolower($post_info->post_title) ) ) ?></h3>
                                    <h4><?= $product_price ?></h4>
                                    <?php if(strlen($sales_price) > 0 ){ ?>
                                    <h4 class="sale-price"><?= $sales_price ?></h4>
                                    <?php } ?>
                                </div>
                            </a>
                </div>
                         
                        <?php
                    }
                    else {
                         foreach ($lst_sales_products->posts as $product_id => $post_info) {  
                            $product_id = $post_info->ID;
                            $product = wc_get_product( $product_id );
                            $regular_price  = $product->get_regular_price();
                            $sales_price    = $product->get_sale_price();
                            $product_price  = $product->get_price_html();
                            $term_list = wp_get_post_terms($product_id,'product_cat',array('fields'=>'ids'));
                            $cat_id = (int)$term_list[0];

                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_info->ID ), 'single-post-thumbnail' );
                            $product_image_url = ( strlen($image[0]) > 0 ) ?  $image[0] : get_template_directory_uri() . "/assets/images/no-image.jpg";
                            
                            $background_style = "";
                            $icon_class = "fal fa-heart";
                            if(in_array($product_id, $product_ids))
                            { 
                                $background_style  = "color:#FEC512";
                                $icon_class = "fas fa-heart";
                            }
                            
                        ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 productWrap">
                    <span data-product_id="<?= $product_id ?>"  class="addToWishlist"><i  style="<?= $background_style ?>" class="<?= $icon_class ?>"></i></span>
                      <a href="<?= esc_url(home_url('/single-shop')); ?>?ID=<?= $product_id ?>"
                                class="<?= (strlen($sales_price) > 0 ) ? "sale" : "" ?>">
                                <div class="imageHolder">
                                    <img src="<?= $product_image_url; ?>"
                                        alt="Msquare Gallery Product <?= $post_info->post_title ?>" title="Msquare Gallery Product <?= $post_info->post_title ?>" />
                                </div>
                                <div class="descHolder">
                                    <h3><?= ucfirst( strtolower($post_info->post_title) ) ?></h3>
                                    <h4><?= $product_price ?></h4>
                                    <?php if(strlen($sales_price) > 0 ){ ?>
                                    <h4 class="sale-price"><?= $sales_price ?></h4>
                                    <?php } ?>
                                </div>
                            </a>
                </div>
                           
                        <?php }
                    }
                
                 } 
                
                ?>
            </div>
        </div>
    </section>
    <section class="categoriesSection">
        <h2 class="sectionTitle titleFont mbsAnimate fadeUp">Shop By Category</h2>
        <div class="sliderWrapper">
            <div class="swiper-container categoriesSwiper">
                <div class="swiper-wrapper">
                    <?php 
                        foreach ($all_categories as $cat) { 
                            $category_slug = $cat->slug;
                            if($category_slug == 'uncategorized' || $category_slug == 'corporate-gifts' || $category_slug == 'cash-gift' || $category_slug == 'our-picks' || $category_slug == 'promotion' || $category_slug == 'curated-weekly')
                                continue;
                            $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                            $image = wp_get_attachment_url( $thumbnail_id );
                            $siteurl = get_option("siteurl");
                        ?>
                        <a href="<?php echo $siteurl; ?>shop/?cat_name=<?php echo $category_slug; ?>" class="swiper-slide">
                           <div class="imageHolder">
                               <img src="<?php echo $image; ?>"
                                   alt="Msquare Gallery logo" title="Msquare Gallery" />
                           </div>
                           <h3><?php echo $cat->name; ?></h3>
                       </a>
                  <?php } ?>
                </div>
            </div>
            <div class="btnPrev categoriesSwiperNav"><i class="fal fa-angle-left"></i></div>
            <div class="btnNext categoriesSwiperNav"><i class="fal fa-angle-right"></i></div>
        </div>
    </section>
    <section class="informativeSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 contentHolder">
                    <div class="innerContentHolder mbsAnimate fadeRight">
                        <h2 class="titleFont"><?php echo $wedding_list_label; ?></h2>
                        <div class="content">
                            <?php echo $wedding_list_brief; ?>
                        </div>
                        <a href="<?php echo $site_url; ?>/wedding-list/">LEARN MORE <i class="fal fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 imageHolder">
                    <img src="<?php echo $wedding_list_image['url']; ?>"
                        alt="Msquare Gallery logo" title="Msquare Gallery" class="mbsAnimate fadeLeft" />
                </div>
            </div>
        </div>
    </section>
    <section class="informativeSection inverted">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 contentHolder">
                    <div class="innerContentHolder mbsAnimate fadeLeft">
                        <h2 class="titleFont"><?php echo $corporate_title; ?></h2>
                        <div class="content">
                            <?php echo $corporate_brief; ?>
                        </div>
                        <a href="<?= $site_url ?>/shop/?cat_name=corporate-gift">VIEW MORE <i class="fal fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 imageHolder">
                    <img src=" <?php echo $corporate_image['url']; ?>"
                        alt="Msquare Gallery logo" title="Msquare Gallery" class="mbsAnimate fadeRight" />
                </div>
            </div>
        </div>
    </section>
    <section class="informativeSection centered">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 contentHolder">
                    <div class="innerContentHolder mbsAnimate fadeRight">
                        <h2 class="titleFont"><?php echo $whatson_label ?></h2>
                        <div class="content">
                            <?php echo $whatson_brief; ?>
                        </div>
                        <a href="<?php echo $site_url; ?>/whats-on/">LEARN MORE</a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 imageHolder">
                    <img src="<?php echo $whatson_image['url']; ?>"
                        alt="Msquare Gallery logo" title="Msquare Gallery" class="mbsAnimate fadeLeft" />
                </div>
            </div>
        </div>
    </section>
    <section class="generalBenefitsSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 eachColumn mbsAnimate fadeUp">
                    <div class="iconHolder">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img18.png"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <h3>Secure Payment</h3>
                    <h4>visa, american express, mastercard</h4>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 eachColumn mbsAnimate fadeUp">
                    <div class="iconHolder">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img16.png"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <h3>Delivery</h3>
                    <h4>home delivery on your schedule</h4>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 eachColumn mbsAnimate fadeUp">
                    <div class="iconHolder">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img17.png"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <h3>Support Center</h3>
                    <h4><a href="<?= esc_url(home_url('/contact-us')); ?>">chat with us</a></h4>
                </div>
            </div>
        </div>
    </section>
</div>
<?Php get_footer();