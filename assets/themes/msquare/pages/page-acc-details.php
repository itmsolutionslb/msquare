<?php
/*
 *
 *  Template name: Account details
 * 
 */

{
    
    $customer_id = get_current_user_id();
    $siteurl = get_site_url();
    if($customer_id == 0)
    {
        $permalink = get_permalink();
        
        header("location:".$siteurl . "/login?link_url=" . urlencode($permalink));
        exit;
    }
    
    
    global $current_user,$woocommerce;
    wp_get_current_user(); 
    $display_name = $current_user->display_name;
    $name_array = explode(" ", $display_name);
    $full_name = $name_array[0];
    $count_wishlist = yith_wcwl_count_all_products(); 
    
}

get_header(); ?>
<div class="accountDetailsPage myAccountPages withPageIdentifier">
    <div class="pageIdentifier">
        <h1>My Account</h1>
    </div>
    <div class="contentSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <h2 class="userName">Hello, Maya</h2>
                    <div class="innerLeftSection">
                        <h3 class="pageName">ACCOUNT DETAILS <i class="far fa-plus"></i></h3>
                        <ul>
                            <li><a href="<?= esc_url(home_url('/my-orders')); ?>">MY ORDERS</a></li>
                            <li class="active"><a href="#">ACCOUNT DETAILS <i class="far fa-check"></i></a></li>
                            <li><a href="<?= esc_url(home_url('/addresses')); ?>">ADDRESSES</a></li>
                            <li><a href="<?= esc_url(home_url('/wishlist')); ?>">WISHLIST (<?= $count_wishlist ?>)</a></li>
                            <li><a href="<?= esc_url(home_url('/wedding-list-info')); ?>"><i class="fal fa-gift"></i>
                                    WEDDING
                                    LIST</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <div class="innerRightSection">
                        <h2 class="sectionTitle">Account Details</h2>
                        <div class="formHolder">
                            <form>
                                <div class="fieldWrapper">
                                    <label>Full Name *</label>
                                    <input type="text" name="msq_full_name" value="<?php echo $current_user->display_name ?>" />
                                </div>
                                <div class="fieldWrapper">
                                    <label>Email *</label>
                                    <input type="text" name="msq_email" value="<?php echo $current_user->user_email ?>" />
                                </div>
                                <div class="fieldWrapper">
                                    <label>Password *</label>
                                    <input type="password" name="msq_password" value="" />
                                </div>
                                <div class="fieldWrapper">
                                    <label>Confirm Password *</label>
                                    <input type="password" name="msq_confirm_password" value="" />
                                </div>
                                <div class="fieldWrapper submitWrapper">
                                    <input type="submit" value="SAVE CHANGES" name="btn_save_change" id="BTN_SAVE_CHANGE" class="generalBtn" />
                                </div>
                            </form>
                        </div>
                        <div class="userAccountDetails">
                          <!--  <span class="circle active"></span>
                            <div class="detailsHolder">
                                <h4>Rayane haddad</h4>
                                <h4>Lebanon, Beirut 1107</h4>
                                <h4>Lebanon</h4>
                                <h4>+961 277 3748</h4>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();