<?php
/*
 *
 *  Template name: Contact us
 * 
 */

{
    $contact_image      = get_field("contact_image");
    $contact_address    = get_field('contact_address');
    $contact_phone      = get_field('contact_phone');
    $contact_email      = get_field('contact_email');
    $msquare_facebook   = get_field('msquare_facebook');
    $msquare_instagram   = get_field('msquare_instagram');
    $msquare_youtube    = get_field('msquare_youtube');
}

get_header(); ?>
<div class="aboutUsPage contactUs paddingTop">
    <div class="innerPage">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 leftSection">
                    <div class="innerLeftSection">
                        <h1 class="mbsAnimate fadeRight">Get in touch</h1>
                        <ul class="getInTouchList">
                            <li class="mbsAnimate fadeRight" data-delay="200">
                                <span class="icon"><i class="fal fa-map-marker-alt"></i></span>
                                <h4><?php echo $contact_address; ?></h4>
                            </li>
                            <li class="phoneNumber mbsAnimate fadeRight" data-delay="300">
                                <span class="icon"><i class="fal fa-phone"></i></span>
                                <h4><a href="tel:<?php echo $contact_phone; ?>"><?php echo $contact_phone; ?></a> <!--<span class="seperator">|</span> <a
                                        href="tel:009611342000"> 01 342
                                        000</a>--></h4>
                            </li>
                            <li class="mbsAnimate fadeRight" data-delay="400">
                                <span class="icon"><i class="fal fa-map-marker-alt"></i></span>
                                <h4><a href="mailto:<?php echo $contact_email; ?>"><?php echo $contact_email; ?></a></h4>
                            </li>
                            <li class="mbsAnimate fadeRight" data-delay="500">
                                <span class="icon"><i class="fal fa-link"></i></span>
                                <h4>Follow us on</h4>
                            </li>
                        </ul>
                        <ul class="socialMediaList mbsAnimate fadeRight" data-delay="600">
                            <li><a href="<?php echo $msquare_facebook; ?>"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?php echo $msquare_instagram; ?>"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="<?php echo $msquare_youtube; ?>"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                        <div class="buttonsHolder mbsAnimate fadeRight" data-delay="700">
                            <a href="#" class="sendMessageTrigger">SEND A MESSAGE</a>
                            <a href="https://www.google.com/maps/dir//M+Square+Gallery,+Beirut/@33.8933543,35.4857929,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x151f17288badd57b:0xe48958deffb6fd59!2m2!1d35.4879816!2d33.8933543" target="_blank"><i class="fal fa-map-marker-alt"></i> GET DIRECTIONS</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 rightSection">
                    <img src="<?= $contact_image['url']; ?>"
                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                </div>
            </div>
        </div>
    </div>
    <div class="generalPopup formsPopup">
        <div class="innerGeneralPopup" data-simplebar data-simplebar-auto-hide="false">
            <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
            <h1 class="popupTitle">Send a message</h1>
            <form>
                <div class="fieldWrapper">
                    <label>Full Name*</label>
                    <input type="text" />
                </div>
                <div class="fieldWrapper">
                    <label>Email*</label>
                    <input type="text" />
                </div>
                <div class="fieldWrapper">
                    <label>Phone</label>
                    <input type="text" />
                </div>
                <div class="fieldWrapper">
                    <label>Your Message</label>
                    <textarea rows="5"></textarea>
                </div>
                <div class="fieldWrapper submitWrapper">
                <input type="submit" value="SEND" />
                </div>
            </form>
        </div>
    </div>
</div>
<?php get_footer();