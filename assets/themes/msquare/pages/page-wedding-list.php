<?php
/*
 *
 *  Template name: Wedding list
 * 
 */

{
    $list_main_title    = get_field("list_main_title");
    $list_main_caption  = get_field("list_main_caption");
    $list_main_image    = get_field("list_main_image");
    
    $benefit_title          = get_field("benefit_title");
    $benefit_description    = get_field("benefit_description");
    $benefit_image          = get_field("benefit_image");
    
    $send_gift_label            = get_field("send_gift_label");
    $send_gift_description      = get_field("send_gift_description");
    $send_gift_image            = get_field("send_gift_image");
     
    
    global $current_user;
    wp_get_current_user();
    
    $user_id = $current_user->ID;
    
    $has_couple = get_user_meta($user_id,"has_couple");
    
    $show_button = true;
    
    $couple_name = get_user_meta($user_id,"couple_name");
    
    
    if($couple_name != null)
    {
      if(strlen($couple_name[0]) > 0)
        $show_button = false;  
    }

    
    
     $siteurl = get_site_url();
    
}

get_header(); ?>
<div class="weddingListPage">
    <input type="hidden" name="siteurl" value="<?php echo $siteurl ?>" />
    <section class="firstSection"
        style="background-image:url('<?= $list_main_image['url']; ?>')">
        <div class="innerFirstSection">
            <?php if($show_button == true){ ?>
            <h1 class="mbsAnimate fadeRight"><?php echo $list_main_title; ?></h1>
            <div class="content mbsAnimate fadeRight" data-delay="250">
                <?php echo $list_main_caption; ?>
            </div>
            <a href="#" class="openGeneralPopup mbsAnimate fadeRight" data-delay="400" data-usage="formpopup">COUPLE REGISTRATION</a>
            <?php }else{ ?>
                <h1 class="mbsAnimate fadeRight"></h1>
                <div class="content mbsAnimate fadeRight" data-delay="250">
                    Have a look at your wedding gifts
                </div>
                <a href="<?= $siteurl ?>/wedding-list-info/?type=received_gift" class="mbsAnimate fadeRight" data-delay="400" data-usage="formpopup">VIEW WEDDING GIFTS</a>
            <?php } ?>
            <div class="shadow"></div>
        </div>
        <div class="topWhitebg"></div>
    </section>
    <section class="secondSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 leftSection">
                    <div class="innerLeftSection">
                        <h2 class="sectionTitle mbsAnimate fadeRight"><?php echo $benefit_title; ?></h2>
                        <?php echo $benefit_description; ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 rightSection">
                    <img class="mbsAnimate fadeLeft" src="<?php echo $benefit_image['url']; ?>"
                        alt="Msquare Gallery logo" title="Msquare Gallery" />
                </div>
            </div>
        </div>
    </section>
    <section class="informativeSection centered">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contentHolder">
                    <div class="innerContentHolder">
                        <h2 class="titleFont mbsAnimate fadeRight"><?php echo $send_gift_label; ?></h2>
                        <div class="content mbsAnimate fadeRight" data-delay="250">
                            <?php echo $send_gift_description; ?>
                        </div>
                        <a href="#" class="openGeneralPopup mbsAnimate fadeRight" data-delay="400" data-usage="cashpopup">CASH GIFT</a>
                        <span class="mbsAnimate fadeRight" data-delay="500">or</span>
                        <a href="<?= esc_url(home_url('/shop')); ?>" class="mbsAnimate fadeRight" data-delay="600">GIFT FROM SHOP</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 imageHolder">
                    <img class="mbsAnimate fadeLeft" src="<?php echo $send_gift_image['url'] ?>"
                        alt="Msquare Gallery <?php echo $send_gift_label; ?>" title="Msquare Gallery <?php echo $send_gift_label; ?>" />
                </div>
            </div>
        </div>
    </section>
    <section class="generalBenefitsSection">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 eachColumn mbsAnimate fadeUp">
                    <div class="iconHolder">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img18.png"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <h3>Secure Payment</h3>
                    <h4>visa, american express, mastercard</h4>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 eachColumn mbsAnimate fadeUp">
                    <div class="iconHolder">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img16.png"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <h3>Delivery</h3>
                    <h4>home delivery on your schedule</h4>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 eachColumn mbsAnimate fadeUp">
                    <div class="iconHolder">
                        <img src="<?= get_template_directory_uri(); ?>/assets/images/todelete/img17.png"
                            alt="Msquare Gallery logo" title="Msquare Gallery" />
                    </div>
                    <h3>Support Center</h3>
                    <h4><a href="#">chat with us</a></h4>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="generalPopup formsPopup" data-usage="formpopup">
    <div class="innerGeneralPopup" data-simplebar data-simplebar-auto-hide="false">
        <div class="WeddingListForm">
            <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
            <h1 class="popupTitle">Join the wedding list</h1>
            <form name="frm_create_weddinglist" id="FRM_CREATE_WEDDINGLIST">
                <input type="hidden" name="action" value="register_wedding_list" />
                <div class="fieldWrapper">
                    <label>Your Name*</label>
                    <input type="text" name="wl_couple_name" value="" />
                </div>
                <?php if( $current_user->ID == 0 ){ ?>
                <div class="fieldWrapper">
                    <label>Email*</label>
                    <input type="text" name="wl_email" required="required" value="<?= $current_user->user_email ?>" />
                </div>
                <div class="fieldWrapper">
                    <label>Password*</label>
                    <input type="password" name="wl_password" required="required" value="" />
                </div>
                <?php } ?>
                <div class="fieldWrapper">
                    <label>Phone</label>
                    <input type="text" name="wl_phone" value="" />
                </div>
                <div class="fieldWrapper submitWrapper">
                    <input type="submit" name="btn_submit_weddinglist" id="BTN_SUBMIT_WEDDING_LIST"  value="SEND" />
                </div>
            </form>
        </div>
        <div class="WeddingListConfirmationMsg" style="display:none">
            <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
            <div class="row">
                <div class="col-md-12" align="center">
                    <img src="<?= get_template_directory_uri(); ?>/assets/images/wedding-list-icon.png" /><br/>
                    <h2>Thank you for submitting!</h2><br/>
                    <span>We will contact you shortly within the next 3 hours!</span>
                </div>   
            </div>
        </div>
    </div>
</div>
<div class="generalPopup formsPopup" data-usage="cashpopup">
    <div class="innerGeneralPopup CashPopup">
        <a href="#" class="closeBtn"><i class="fal fa-times"></i></a>
        <h1 class="popupTitle">Cash Gift</h1>
        <h3 class="popupSubTitle">We will contact you shortly within the next 3 hours!Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
        <form name="frm_cash" id="FRM_CASH">
            <input type="hidden" name="action" value="cash_amount" />
            <div class="fieldWrapper">
                <label>Enter Amount</label>
                <div class="input-group">
                    <input type="text" name="cg_amount" placeholder="00.00" value="" />
                    <img class="CashIcon" src="<?= get_template_directory_uri(); ?>/assets/images/cash-icon.png" />
                </div>
                
            </div>
            <div class="fieldWrapper submitWrapper">
                <input name="btn_add_cash_gift" id="BTN_ADD_CASH_GIFT" type="submit" value="PROCEED TO CHECKOUT" />
            </div>
        </form>
    </div>
</div>
</div>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/javascript/users.js" ></script>
<?php get_footer();