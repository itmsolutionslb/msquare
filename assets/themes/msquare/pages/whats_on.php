<?php
/*
 *
 *  Template name: Whats on
 * 
 */
{
          $defaults = array(
        'numberposts'      => -1,
        'category'         => 27,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'suppress_filters' => true,
    );
   $lst_posts = get_posts( $defaults );
    
   
}
get_header(); ?>
<div class="artistsPage withPageIdentifier whatsOnPage">
    <div class="pageIdentifier">
        <h1>What's On</h1>
    </div>
    <div class="artistsSection">
        <div class="artistsWrapper">
            <div class="container">
                <div class="row">
                    <?php foreach ( $lst_posts as $key => $post_info ) { 
                           $post_id     = $post_info->ID;
                           $post_title  = $post_info->post_title;
                           $facebook_url    = get_field("event_facebook_link",$post_id);
                           $event_date      = get_field("event_date",$post_id);
                           $event_date = strtotime($event_date);
                           $event_date = date("d M Y",$event_date);
                           $post_image = get_the_post_thumbnail($post_id);
                        ?>
                         <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 eachArtistWrap mbsAnimate fadeUp">
                            <a href="<?= esc_url(home_url('/single-whats-on')); ?>?post_id=<?php echo $post_id; ?>">
                                <div class="imageHolder">
                                   <?php echo $post_image; ?>
                                </div>
                                <h3><?php echo $post_title; ?></h3>
                                <h4><?php echo $event_date; ?></h4>
                            </a>
                            <a href="<?php echo $facebook_url; ?>" class="facebookTrigger">VIEW ON FACEBOOK</a>
                        </div>
                   <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();