<?php
/*
 *
 *  Template name: Artists
 * 
 */

{// 
    $taxonomy     = 'portfolio_category';
    $orderby      = 'name';  
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no  
    $title        = '';  
    $empty        = 0;

    $args = array(
           'taxonomy'     => $taxonomy,
           'orderby'      => $orderby,
           'show_count'   => $show_count,
           'pad_counts'   => $pad_counts,
           'hierarchical' => $hierarchical,
           'title_li'     => $title,
           'hide_empty'   => $empty
    );
   $all_categories = get_categories( $args );
 
   $category = isset($_GET['category_slug']) ? $_GET['category_slug'] : "";
   
    $args = array(
       'post_type'      => 'portfolio', 
       'posts_per_page' => -1
    );
    if($category != "" )
    {

      $args['tax_query'] = array(
        array(
          'taxonomy' => 'portfolio_category',
        'field' => 'slug',
        'terms' => $category, // Where term_id of Term 1 is "1".
        'include_children' => false
        )
      );
    }
    $lst_portfolios = new WP_Query( $args ); 
 
    $lst_portfolios = $lst_portfolios->posts;
 
      /**$defaults = array(
        'numberposts'      => -1,
        'category'         => "furniture",
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'portfolio',
        'suppress_filters' => true,
    );
    $portfolio = new WP_Query();
    
   $lst_portfolios = get_posts( $defaults );*/

  $siteurl = get_option("siteurl");
   
}
get_header(); ?>
<div class="artistsPage withPageIdentifier">
    <div class="pageIdentifier">
        <h1>Meet our artists</h1>
    </div>
    <div class="artistsSection">
        <div class="categoriesWrapper">
            <ul>
                <li class="<?php echo  ( $category == '' ? "active" : "" ) ?>"><a href="<?php echo $siteurl ?>/artists/">All</a></li>
                <?php foreach ($all_categories as $key => $cat_info) { ?>
                <li class="<?php echo ($category == $cat_info->term_id ? "active" : "" ); ?>" ><a href="<?php echo $siteurl ?>/artists/?category_slug=<?php echo $cat_info->slug; ?>"><?php echo $cat_info->name ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="artistsWrapper">
            <div class="container">
                <div class="row">
                    <?php foreach ($lst_portfolios as $key => $port_info) {
                        $post_id = $port_info->ID;  
                        $post_image = get_the_post_thumbnail($post_id);
                          $item_categories = get_the_terms( $post_id, 'portfolio_category' );
                    ?>
                    <a href="<?= esc_url(home_url('/single-artist')); ?>?id=<?php echo $post_id; ?>" class="col-lg-4 col-md-6 col-sm-6 col-xs-12 eachArtistWrap mbsAnimate fadeUp">
                        <div class="imageHolder">
                           <?php echo $post_image; ?>
                        </div>
                        <h3><?php echo $port_info->post_title; ?></h3>
                        <?php if(!isset($_GET['category_slug'])){ ?>
                        <h4>
                            <?php
                            $categories = "";
                            foreach ($item_categories as $key => $category_item) {
                                $categories .= ucfirst($category_item->name) . ",";
                            }
                            //$categories[strlen($categories) - 1] = "";
                            $categories = substr($categories, 0 ,strlen($categories) - 1 );
                            echo $categories;
                            ?>
                        </h4>
                        <?php } ?>
                    </a> 
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();