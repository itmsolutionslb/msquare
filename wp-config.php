<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define( 'DB_NAME', 'msquare_website_db' );
define( 'DB_NAME', 'msquare_second_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':0n 0<2:]1}HXZk9<5u(fQyjxDS`^8sl^1G0V!d_uyt5l kW4rw NJUKk:CX]}}!' );
define( 'SECURE_AUTH_KEY',  ']|n$R!n*e)w>U8xTP-a/:Eu<-t!THM>y{wG$Sxccu@%Ugf#8RjW:W_C#wpp#BPvy' );
define( 'LOGGED_IN_KEY',    '2tD+% u?Z@1r!-8$GIdr7(fFy;zlPoIVl+=}mZaT%l.]Q[sgq(DNW.>LNzGScQDc' );
define( 'NONCE_KEY',        '4^$Hnr!kExR+*zi]mcjx77qlu-&6&)n<IA@r{r?[:Ty4qn3qYgKqb~(_+^e c%?{' );
define( 'AUTH_SALT',        '=W^YAEEIIh>2.u7u?uS;)L{.t!Yd,f_|Y:o e$XW?I8!] Cnd5$sBcL*l,<dG$9a' );
define( 'SECURE_AUTH_SALT', '69>A2esFr!G!{QqR%<z]0*w_gTIdHi*e!7u:ni*m>y+.8|RMz~Jb=_KLJhO7!Gx1' );
define( 'LOGGED_IN_SALT',   'p AIn,r4rurCVo{k18jBPv6ZW|37UV[l5M<4!j.,d$+%h@Gu?t:%q5jvp*0#2P_]' );
define( 'NONCE_SALT',       '%z1s-_U]Rm+IE9ZZ-dVSI,M(wV3v{R[hleRp`OH[kSfB?U}-UBTaW{Ss$E}!h-k5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );
@ini_set( 'display_errors', 1 );

define('WP_CONTENT_FOLDERNAME', 'assets');
define('WP_CONTENT_DIR', ABSPATH . WP_CONTENT_FOLDERNAME);
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/itm_websites/msquare_website/');
define('WP_CONTENT_URL', WP_SITEURL . WP_CONTENT_FOLDERNAME);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
